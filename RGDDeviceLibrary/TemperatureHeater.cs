﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace RGD_Library
{
    /// <summary>
    /// Holds all the temperature and heater information
    /// </summary>
    public class TemperatureHeater : PlottableChannel
    {
        public double Temperature_Current { get; private set; }
        public double Temperature_Previous { get; private set; }
        public double Temperature_Raw { get; private set; }
        public short Status { get; private set; }

        public double calSlope;
        public double calIntercept;

        //These are for special uses, not the same as the Temp Ideal and Temp Tolerance in the main system
        public float OptimalValue;
        public float OptimalRange;


        public MainController ParentController;

        public TemperatureHeater(MainController parentControl)
        {
            initPC(ChannelClass.RecentBufferSize);
            rectValue = new RectangleF(0, 0, 75, 100);
            rectDisplay = new RectangleF(0, 0, 75, 100);

            ChannelName = "Temp C";

            ParentController = parentControl;
            Temperature_Current = -1;
            Temperature_Previous = -1;
        }

        internal void Update(float Temperature, double Volts, string HeaterStatus)
        {
            Temperature_Raw = Volts;
            Temperature_Previous = Temperature_Current;
            Temperature_Current = Temperature;

            DoEnqueue(Temperature);

            double delta = Temperature_Previous - Temperature_Current;
            switch (delta)
            {
                case double n when n > 0:
                    Status = -1;
                    break;
                case double n when n == 0:
                    Status = 0;
                    break;
                case double n when n < 0:
                    Status = 1;
                    break;
                default:
                    Status = 0;
                    break;
            }
        }

        public void Calibrate(double cal_Low_Raw, double cal_High_Raw)
        {
            calSlope = (ParentController.Store.TemperatureCal_High - ParentController.Store.TemperatureCal_Low) / (cal_High_Raw - cal_Low_Raw); //Rise/Run
            calIntercept = ParentController.Store.TemperatureCal_High - (calSlope * cal_High_Raw);
        }
    }

}
