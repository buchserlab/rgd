﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace RGD_Library
{
    /// <summary>
    /// These are the byte packets that are sent to the Device (SOFTWARE > FIRMWARE)
    /// </summary>
    public class Packet_Instruct
    {
        public byte[] Bytes { get; }
        public byte nBytes => (byte)Bytes.Length;
        public byte StartCode => Bytes[0];
        public const byte BytesTotal = 16;
        public string TextForm => Encoding.UTF8.GetString(Bytes);

        public const byte StartCode_Heat = 27;
        public const byte StartCode_Timing = 45;
        public const byte StartCode_EEPROM = 73;
        public const byte StartCode_EEPROM_Ext1 = 79;
        public const byte StartCode_EEPROM_Ext2 = 83;
        public const byte StartCode_PreQuery = 91;
        public const byte StartCode_StatusBuzzer = 107;
        public const byte StartCode_StatusLights = 123;

        public Packet_Instruct()
        {

        }

        public Packet_Instruct(string TextFormInstruct)
        {
            Bytes = Encoding.UTF8.GetBytes(TextForm);
        }

        public Packet_Instruct(byte[] ByteArr)
        {
            if (ByteArr.Length != BytesTotal)
            {
                Debugger.Break();
            }
            Bytes = ByteArr;
        }

        //Send Temp -
        //On/Off (1 byte) IdealTemp (2 bytes > float), tolerance (1 byte > float)
        public static Packet_Instruct SendTemp(bool HeaterOn, float IdealTempC, float Tolerance)
        {
            byte StartCode = StartCode_Heat;               //0
            byte On = HeaterOn ? (byte)1 : (byte)0;        //1
            byte[] TI = BitConverter.GetBytes(IdealTempC); //2,3,4,4
            byte[] TO = BitConverter.GetBytes(Tolerance);  //6,7,8,9
            byte[] bArr = new byte[BytesTotal] { StartCode, On, TI[0], TI[1], TI[2], TI[3], TO[0], TO[1], TO[2], TO[3], 0, 0, 0, 0, 0, 0 };
            return new Packet_Instruct(bArr);
        }

        public static Packet_Instruct SendTemp(bool HeaterOn, StoreClass sV)
        {
            return SendTemp(HeaterOn, sV.Temperature_Ideal, sV.Temperature_Tolerance);
        }

        public static Packet_Instruct SendTemp_HeaterOn_60()
        {
            return SendTemp(true, 60, 0.5f);
        }

        public static Packet_Instruct SendTemp_HeaterOff()
        {
            return SendTemp(false, 0, 0);
        }

        //Send Timing of Lights and Reads - - - - - - - - 
        public static Packet_Instruct SendTiming(Int32 CycleTime, Int16 LightTime, Int16 Delay, Int16 Sensor, Int16 Send, Int16 Delta, bool AllowLights)
        {
            byte StartCode = StartCode_Timing;             //0
            byte[] CT = BitConverter.GetBytes(CycleTime);  //1,2,3,4
            byte[] LT = BitConverter.GetBytes(LightTime);  //5,6
            byte[] DE = BitConverter.GetBytes(Delay);      //7,8
            byte[] SE = BitConverter.GetBytes(Sensor);     //9,10
            byte[] SD = BitConverter.GetBytes(Send);       //11,12
            byte[] DT = BitConverter.GetBytes(Delta);      //13,14
            byte AL = (byte)(AllowLights ? 1 : 0);         //15
            byte[] bArr = new byte[BytesTotal] { StartCode, CT[0], CT[1], CT[2], CT[3], LT[0], LT[1], DE[0], DE[1], SE[0], SE[1], SD[0], SD[1], DT[0], DT[1], AL };
            return new Packet_Instruct(bArr);
        }

        public static Packet_Instruct SendTiming(StoreClass SV)
        {
            return SendTiming(SV.Timing_Cycle, SV.Timing_Light, SV.Timing_Delay, SV.Timing_Sensor, SV.Timing_Send, SV.Timing_Delta, SV.AllowLights);
        }

        //Set EEPROM -
        //DeviceID (4 bytes > long), CalSlope (2 bytes > float), CalIntercept (2 bytes > float), 
        public static Packet_Instruct SendEEPROM(Int32 deviceID, float CalSlope, float CalIntercept)
        {
            byte StartCode = StartCode_EEPROM;               //0
            byte[] dID = BitConverter.GetBytes(deviceID);    //1,2,3,4
            byte[] CS = BitConverter.GetBytes(CalSlope);     //5,6,7,8
            byte[] CI = BitConverter.GetBytes(CalIntercept); //9,10,11,12
            byte[] bArr = new byte[BytesTotal] { StartCode, dID[0], dID[1], dID[2], dID[3], CS[0], CS[1], CS[2], CS[3], CI[0], CI[1], CI[2], CI[3], 0, 0, 0 };
            return new Packet_Instruct(bArr);
        }

        public static Packet_Instruct SendEEPROM_Ext1(byte PinTube1, byte PinTube2, byte PinTube3, byte PinLEDT1, byte PinLEDT3, byte PinTherm, byte PinTherm2, byte PinHeater)
        {
            byte StartCode = StartCode_EEPROM_Ext1;
            //                                   //0  (Green)T1  T2  T3  (Red)T1 T2 T3  (blueLED) T1 T2 T3 (greenLED) T1 T2 T3 (Heat) therm1 therm2 heater
            byte[] bArr = new byte[BytesTotal] { StartCode, PinTube1, PinTube2, PinTube3, 0, 0, 0, PinLEDT1, 0, PinLEDT3, 0, 0, 0, PinTherm, PinTherm2, PinHeater };
            return new Packet_Instruct(bArr);
        }

        public static Packet_Instruct SendEEPROM_Ext2(byte PinBuzzer, byte pinR, byte pinG, byte pinB, float temp2_Optimal, float temp2_Range)
        {
            byte StartCode = StartCode_EEPROM_Ext2;
            byte[] O = BitConverter.GetBytes(temp2_Optimal);  //1,2,3,4
            byte[] R = BitConverter.GetBytes(temp2_Range);  //1,2,3,4
            //                                 //0          1          2     3     4     5     6     7     8     9     10    11    12    13 14 15   
            byte[] bArr = new byte[BytesTotal] { StartCode, PinBuzzer, pinR, pinG, pinB, O[0], O[1], O[2], O[3], R[0], R[1], R[2], R[3], 0, 0, 0 };
            return new Packet_Instruct(bArr);
        }

        public static Packet_Instruct SendPreQuery()
        {
            byte StartCode = StartCode_PreQuery;
            byte[] bArr = new byte[BytesTotal] { StartCode, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            return new Packet_Instruct(bArr);
        }

        public static Packet_Instruct SendStatusBuzzer_Connected()
        {
            float tone_msec = 60; float silence = 22;
            List<Tuple<float, float, float>> Frequency_Time_Silent = new List<Tuple<float, float, float>>(2);
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(523f, tone_msec, silence));    //C 523
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(523f, tone_msec, silence));    //C 523
            return SendStatusBuzzer(Frequency_Time_Silent);
        }

        public static Packet_Instruct SendStatusBuzzer_StartingRun()
        {
            float tone_msec = 47; float silence = 3;
            List<Tuple<float, float, float>> Frequency_Time_Silent = new List<Tuple<float, float, float>>(2);
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(329.6f, tone_msec, silence));    //E 329.6
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(369.9f, tone_msec, silence));    //F# 369.9
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(440f, tone_msec, silence));      //A 440 
            return SendStatusBuzzer(Frequency_Time_Silent);
        }

        public static Packet_Instruct SendStatusBuzzer_Halfway()
        {
            float tone_msec = 40; float silence = 20;
            List<Tuple<float, float, float>> Frequency_Time_Silent = new List<Tuple<float, float, float>>(2);
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(329.6f, tone_msec, silence));    //E 329.6
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(329.6f, tone_msec, silence));    //E 329.6
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(329.6f, tone_msec, silence));    //E 329.6 
            return SendStatusBuzzer(Frequency_Time_Silent);
        }

        public static Packet_Instruct SendStatusBuzzer_RunEnd()
        {
            float tone_msec = 175; float silence = 10;
            List<Tuple<float, float, float>> Frequency_Time_Silent = new List<Tuple<float, float, float>>(2);
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(369.9f, tone_msec, silence));         //F# 369.9
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(440f, tone_msec, silence));           //A 440 
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(369.9f, tone_msec, silence));         //F# 369.9
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(329.6f, tone_msec * 2, silence * 2)); //E 329.6
            //Frequency_Time_Silent.Add(new Tuple<float, float, float>(440f, tone_msec, silence ));    //A 440
            return SendStatusBuzzer(Frequency_Time_Silent);
        }


        public static Packet_Instruct SendStatusBuzzer_Simple1()
        {
            float tone_msec = 120; float silence = 5;
            List<Tuple<float, float, float>> Frequency_Time_Silent = new List<Tuple<float, float, float>>(2);
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(329.6f, tone_msec, silence));    //E 329.6
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(369.9f, tone_msec, silence));    //F# 369.9
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(440f, tone_msec, silence));  //A 440 
            return SendStatusBuzzer(Frequency_Time_Silent);
        }

        public static Packet_Instruct SendStatusBuzzer_Simple3()
        {
            float tone_msec = 120; float silence = 5;
            List<Tuple<float, float, float>> Frequency_Time_Silent = new List<Tuple<float, float, float>>(2);
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(369.9f, tone_msec, silence));    //F# 369.9
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(329.6f, tone_msec, silence));    //E 329.6
            Frequency_Time_Silent.Add(new Tuple<float, float, float>(369.9f, tone_msec * 2, silence));    //F# 369.9
            return SendStatusBuzzer(Frequency_Time_Silent);
        }

        /// <summary>
        /// The list is a set of numbers, a sequence. Each element is Frequency in Hz, Time in msec for the tone, and Time in msec of silence before starting the next
        /// </summary>
        /// <param name="Frequency_Time_Silent"></param>
        /// <returns></returns>
        public static Packet_Instruct SendStatusBuzzer(List<Tuple<float, float, float>> Frequency_Time_Silent)
        {
            byte StartCode = StartCode_StatusBuzzer;
            float freq_div = 4;
            float freq_offset = 200;
            float int_div = 20;
            while (Frequency_Time_Silent.Count < 5) Frequency_Time_Silent.Add(new Tuple<float, float, float>(0, 0, 0));
            byte[] bArr = new byte[BytesTotal] { StartCode,
                (byte)((Frequency_Time_Silent[0].Item1-freq_offset)/freq_div), (byte)(Frequency_Time_Silent[0].Item2 / int_div), (byte)(Frequency_Time_Silent[0].Item3 / int_div),
                (byte)((Frequency_Time_Silent[1].Item1-freq_offset)/freq_div), (byte)(Frequency_Time_Silent[1].Item2 / int_div), (byte)(Frequency_Time_Silent[1].Item3 / int_div),
                (byte)((Frequency_Time_Silent[2].Item1-freq_offset)/freq_div), (byte)(Frequency_Time_Silent[2].Item2 / int_div), (byte)(Frequency_Time_Silent[2].Item3 / int_div),
                (byte)((Frequency_Time_Silent[3].Item1-freq_offset)/freq_div), (byte)(Frequency_Time_Silent[3].Item2 / int_div), (byte)(Frequency_Time_Silent[3].Item3 / int_div),
                (byte)((Frequency_Time_Silent[4].Item1-freq_offset)/freq_div), (byte)(Frequency_Time_Silent[4].Item2 / int_div), (byte)(Frequency_Time_Silent[4].Item3 / int_div) };
            return new Packet_Instruct(bArr);
        }

        public static Packet_Instruct SendStatusLights(bool KeepOn)
        {
            return SendStatusLights(0, 0, 0);
        }

        public static Packet_Instruct SendStatusLights(byte R, byte G, byte B)
        {
            return SendStatusLights(R, G, B, 0, 0.5, 3);
        }

        public static Packet_Instruct SendStatusLights(byte R, byte G, byte B, float MSEC_Mod_Cycle, double FractionOn, byte TotalCycles)
        {
            byte StartCode = StartCode_StatusLights;
            byte M_on = (byte)(MSEC_Mod_Cycle / 20); //which modulus switches on in milliseconds - Max 5 seconds, min 20 msec
            byte Frac = (byte)(256 * FractionOn); //what fraction of the same modulus do the lights stay on for (so 0.5 means on for half and off for half . . 0.9 means on 90% duty cycle)
            byte S_tot = (byte)(TotalCycles); //0 means indefinite, so for example if M_on is 1000 (1 second), and TotalCycles is 10, then the blink will happen for 10 seconds
            byte[] bArr = new byte[BytesTotal] { StartCode, R, G, B, M_on, Frac, S_tot, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            return new Packet_Instruct(bArr);
        }

    }

}
