﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RGD_Library
{
    // Important: After any non-trivial change within this file, you MUST check to see if the results are the same
    public static class CurveFit
    {
    }

    internal class Fit_Optimization_Params
    {
        public string PName;
        public float Min;
        public float Max;
        public float Interval;
        public int Steps;
        public int Iterations;

        public Fit_Optimization_Params(string PropertyName, float min, float max, int steps, int iterations)
        {
            PName = PropertyName;
            Min = min; Max = max;
            Steps = steps;
            Interval = (Max - Min) / Steps;
            Iterations = iterations;
        }
    }
    public class CurveModel
    {
        public readonly float[] RawData;
        private readonly float[] _data;
        private bool _isFitted = false;
        public bool IsFitted { get => _isFitted; }

        private float _plateau;
        public float Plateau { get { CheckForFit(); return _plateau; } private set { _plateau = value; } }
        private float _baseline;
        public float Baseline { get { CheckForFit(); return _baseline; } private set { _baseline = value; } }
        private float _x50;
        public float X50 { get { CheckForFit(); return _x50; } private set { _x50 = value; } }
        private float _riseTime;
        public float RiseTime { get { CheckForFit(); return _riseTime; } private set { _riseTime = value; } }
        private double _r2;
        public double R2 { get { CheckForFit(); return _r2; } }
        private float _runLength;
        public float RunLength { get { CheckForFit(); return _runLength; } }
        private float _yDelta { get => _plateau - _baseline; }
        public float yDelta { get => Plateau - Baseline; }
        public float X50_Minutes { get => (X50 * MinutesPerDataPoint) + MinutesExcluded; }
        public float RiseTime_Minutes { get => (RiseTime * MinutesPerDataPoint) + MinutesExcluded; }
        public float RunLength_Minutes { get => (RunLength * MinutesPerDataPoint) + MinutesExcluded; }
        public float MinutesPerDataPoint { get; set; }  //Usually 30/720 = 0.04167 minutes per Datapoint
        public float MinutesExcluded { get; set; }

        public float JustOptimized;

        public readonly int NSteps = 20;
        public readonly int NIterations = 2;
        public FitResult Interpretation
        {
            get
            {
                // First decide whether there was amplificaion
                bool Amplification = X50_Minutes > 0 && X50_Minutes < 35 && RiseTime_Minutes > 2 && RiseTime_Minutes < 15 && R2 > 0.2 && R2 < 1 && yDelta > 2 && yDelta < 1024;
                if (!Amplification) return FitResult.NoAmplification;

                if (X50_Minutes > 25)
                {
                    return FitResult.BackgroundAmplification;
                }
                else if (X50_Minutes > 22)
                {
                    return FitResult.Ambiguous;
                }
                else
                {
                    return FitResult.ProductAmplification;
                }
            }
        }
        public CurveModel(ChannelClass channel)
        {
            RawData = channel.Recent_Exclude.ToArray();  // Make sure you use the data that excludes some of the beginning, but have to account for that somewhere
            _baseline = channel.Baseline;
            _plateau = channel.Plateau;
            _riseTime = RawData.Length / 4.3f;

            List<float> DataL = new List<float>(RawData);

            //This is trying to get the X50 to be on the far right if it is a flat curve
            float newEnd = Math.Max(1, _plateau + (_plateau - _baseline));
            DataL.Add(newEnd); DataL.Add(newEnd);

            _data = DataL.ToArray();
        }

        public void Fit()
        {
            OptimizeParameter(nameof(_x50), _riseTime / 2, (float)(_data.Length - (_riseTime / 2)));
            OptimizeParameter(nameof(_baseline), _baseline - (_yDelta / 2), _plateau - 0.01f);
            OptimizeParameter(nameof(_plateau), _baseline + 0.01f, _plateau + (_yDelta / 2));
            OptimizeParameter(nameof(_riseTime), _data.Length / 100, _data.Length * 1);
            _isFitted = true;
        }
        public void CheckForFit()
        {
            if (!IsFitted) throw new ModelNotFittedException();
        }

        public float[] GenerateCurve()
        {
            float[] t = new float[_data.Length];
            //Lets assume rise-time and x50 is in n units
            int Rise_Start = (int)(_x50 - (_riseTime / 2));
            int Rise_End = (int)(_x50 + (_riseTime / 2));

            float ySpan = _plateau - _baseline;

            for (int i = 0; i < _data.Length; i++)
            {
                if (i < Rise_Start)
                {
                    t[i] = _baseline;
                }
                else if (i < Rise_End)
                {
                    t[i] = (ySpan * (i - Rise_Start) / (_riseTime)) + _baseline;
                }
                else
                {
                    t[i] = _plateau;
                }
            }
            return t;
        }

        public static double CalcR2(float[] y_actual, float[] y_fit)
        {
            //r2 = 1 - ss total / ss res
            double avg_y = y_actual.Average();
            double ss_total = y_actual.Sum(y => Math.Pow(y - avg_y, 2));
            double ss_residual = 0;
            for (int i = 0; i < y_actual.Length; i++) ss_residual += Math.Pow(y_actual[i] - y_fit[i], 2);
            double r2 = (1 - (ss_residual / ss_total));
            return r2;
        }

        private void OptimizeParam(string FPVarName, float Min, float Max, float Interval)
        {
            float[] tFit;
            double r2;
            double r2_Max = -1; float val_atMax = 1;
            Type type = typeof(CurveModel);
            FieldInfo field = type.GetField(FPVarName, BindingFlags.NonPublic | BindingFlags.Instance);
            for (float i = Min; i < Max; i += Interval)
            {
                field.SetValue(this, i);
                tFit = GenerateCurve();
                r2 = CalcR2(_data, tFit);
                //System.Diagnostics.Debug.Print(i + " " + r2);
                if (r2 > r2_Max) { r2_Max = r2; val_atMax = i; }
            }

            type.GetField(FPVarName, BindingFlags.NonPublic | BindingFlags.Instance).SetValue(this, val_atMax);
            JustOptimized = val_atMax;
            _r2 = r2_Max;
        }

        private void OptimizeParams_Fine(string FPVarName, float PrevInterval)
        {
            Type type = typeof(CurveModel);
            FieldInfo field = type.GetField(FPVarName, BindingFlags.NonPublic | BindingFlags.Instance);
            float Val = (float)field.GetValue(this);
            float Min = Math.Max(0, Val - PrevInterval * 1.5f);
            float Max = Val + PrevInterval * 1.5f;
            float NewInterval = (Max - Min) / NSteps;

            OptimizeParam(FPVarName, Min, Max, NewInterval);
        }



        private void OptimizeParameter(string PName, float Min, float Max)
        {
            //System.Diagnostics.Debug.Print(PName + " 0 " + " " + FP.JustOptimized + " " + FP.R2);
            float Interval = (Max - Min) / NSteps;
            OptimizeParam(PName, Min, Max, Interval);
            //System.Diagnostics.Debug.Print(PName + " 1 " + " " + FP.JustOptimized + " " + FP.R2);
            for (int i = 1; i < NIterations; i++)
            {
                OptimizeParams_Fine(PName, Interval);
                //System.Diagnostics.Debug.Print(PName + " " + (i + 1) + " " + FP.JustOptimized + " " + FP.R2);
            }
        }
    }

    public class ModelNotFittedException : Exception
    {
        public override string Message => "Model must be fit before accessing parameters";
    }
}
