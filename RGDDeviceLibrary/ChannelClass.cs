﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;

namespace RGD_Library
{
    /// <summary>
    /// Represents one Channel of one Tube in the Device
    /// </summary>
    public class ChannelClass : PlottableChannel
    {
        public int ChannelID { get; internal set; }
        public static int RecentBufferSize { get; set; }
        public TubeClass ParentTube { get; private set; }
        public bool SaveRawData { get; set; }
        //public Queue<float> ListRecent { get; private set; }
        public int tubeID => ParentTube.TubeID;
        private StreamWriter SW => ParentTube.ParentTubes.ParentControl.SaveStream;

        private Random _Rnd;

        public CurveModel CurveRegression { get; private set; } = null;

        public ChannelClass(TubeClass tParent, int ChannelID)
        {
            init(tParent, RecentBufferSize, true, ChannelID);
        }

        private void init(TubeClass tParent, int RecentBufferSize, bool SaveRaw, int ChannelID)
        {
            SaveRawData = SaveRaw;
            ParentTube = tParent;
            this.ChannelID = ChannelID;
            if (tParent != null) ChannelName = "Tube" + tParent.TubeID; // + " Ch" + ChannelID;

            initPC(RecentBufferSize, 0, 2);

            ResetScaling();

            _Rnd = new Random(DateTime.Now.Millisecond + 1);
        }

        public string Add(float NewData)
        {
            DoEnqueue(NewData);

            return SaveToDisk(NewData);
        }

        internal string SaveToDisk(float NewData, bool ExportHeaders = false)
        {
            if (SaveRawData)
            {
                //Every so often, save what we have
                if (_Rnd.Next(128) == 2) { SW.Flush(); }

                if (ExportHeaders)
                    return ParentTube.ExportPrefix(ExportHeaders) + "ChID" + "\t" + "ChData";
                else
                    return ParentTube.ExportPrefix(ExportHeaders) + ChannelID + "\t" + NewData;
            }
            // There might be a problem if SaveRawData is false
            return "";
        }

        internal void FlushBuffer()
        {
            SW.Flush();
        }

        public void AdjustScaling(float upper_value, bool AdjustBaseline = true)
        {
            if (float.IsNaN(upper_value)) return;
            float lower = Baseline;
            float div = 10.24f;
            if (lower > upper_value)
            {
                //Not good, not sure what to do
                Debugger.Break();
            }
            rectValue = new RectangleF(rectValue.X, lower, rectValue.Width, upper_value);
            rectDisplay = new RectangleF(rectDisplay.X, lower / div, rectDisplay.Width, (upper_value / 10.24f));
        }

        public void ResetScaling()
        {
            rectValue = new RectangleF(0, 0, 100, 1024);
            rectDisplay = new RectangleF(0, 0, 100, 100);
            //rectValue = new RectangleF(rectValue.X, rectValue.Y, rectValue.Width, rectValue.Height);
            //rectDisplay = new RectangleF(rectDisplay.X, rectValue.Y, rectValue.Width, rectValue.Height);
        }

        internal static ChannelClass PlaceHolderChannel()
        {
            return MainController.CreatePlaceHolder();
        }

        public void GenerateCurveFit()
        {
            CurveRegression = new CurveModel(this);

            //This adjusts the output to make it more viewable.  If you do it earlier, it gets overwritten
            CurveRegression.MinutesPerDataPoint = SampleTime_Minutes();
            CurveRegression.MinutesExcluded = Exclude_Samples * SampleTime_Minutes(); //This is related to whatever is excluded in the very beginning
            CurveRegression.Fit();
        }
    }

}
