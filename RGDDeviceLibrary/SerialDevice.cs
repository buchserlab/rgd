﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
using System.Threading.Tasks;

namespace RGD_Library
{
    public class SerialDevice
    {
        public MainController ParentControl;
        internal SerialPort SP { get; private set; }
        public PinsClass Pins { get; private set; }
        public long deviceID { get; private set; }
        public long Firmware_Version;

        private string _PortName;
        public string PortName { get { return _PortName; } set { _PortName = value; Reset(); } }
        public string RunID { get; private set; }
        public void ResetRunID(bool IsRun)
        {
            RunID = StartTime.ToString("yyyyMMdd_HHHmmss_") + (IsRun ? "R" : "T");
        }

        private int _BaudRate;
        public int BaudRate { get { return _BaudRate; } set { _BaudRate = value; Reset(); } }

        public string SubFolder => DateTime.Now.ToString("yyyy.MM.dd ") + deviceID;
        public string FileHeader => RunID;

        public Dictionary<string, object> exportInfo()
        {
            return new Dictionary<string, object>()
            {
                {"RunID", RunID },
                {"deviceID", deviceID}
            };
        }

        public DateTime StartTime { get; private set; }

        public void ResetStartTime()
        {
            StartTime = DateTime.Now;
        }

        public DateTime LastReceived_SP { get; private set; }

        public string Query()
        {
            try
            {
                SP.Open();
            }
            catch (Exception E)
            {
                return E.Message;
            }
            long deviceID_t = -1;
            PinsClass tPins = new PinsClass();
            string line;
            for (int i = 0; i < 10; i++)
            {
                SP.DiscardInBuffer();
                Instruct(Packet_Instruct.SendPreQuery());
                Thread.Sleep(300);
                for (int j = 0; j < 10; j++)
                {
                    var task = Task.Run(() => SP.ReadLine());
                    if (task.Wait(TimeSpan.FromSeconds(2)))
                        line = task.Result;
                    else
                        return "";
                    if (line == null) break;
                    string[] messages = line.Split(' ');
                    if (messages.Length >= 12)
                    {
                        Packet_Data.Receive_Initial(messages, out deviceID_t, out ParentControl.tempHeater.calSlope, out ParentControl.tempHeater.calIntercept, out ParentControl.tempHeater2.OptimalValue, out ParentControl.tempHeater2.OptimalRange, out Firmware_Version, out tPins);
                        break;
                    }
                }
                if (deviceID_t > 0) break;
            }
            deviceID = deviceID_t; Pins = tPins;
            SP.Close();
            return "";
        }

        public void Instruct(string SendString)
        {
            if (!SP.IsOpen) return;
            SP.WriteLine(SendString);
        }

        public void Instruct(Packet_Instruct Instructions)
        {
            if (!SP.IsOpen) return;
            SP.Write(Instructions.Bytes, 0, Instructions.nBytes);
        }

        public SerialDevice(MainController controller, string comPort)
        {
            this.ParentControl = controller;
            init(9600, comPort);
        }

        public SerialDevice(MainController controller, int BaudRate, string comPort)
        {
            this.ParentControl = controller;
            init(BaudRate, comPort);
        }

        private void init(int BaudRate, string comPort)
        {
            LastReceived_SP = StartTime = DateTime.Now;

            deviceID = -1;
            RunID = "";

            _PortName = comPort; //  Depends on various factors
            _BaudRate = BaudRate;   //  9600 is plenty

            Pins = new PinsClass(this);

            if (PortName == "") return;
            SP = new SerialPort(PortName, BaudRate);
        }

        public double Minutes
        {
            get
            {
                TimeSpan TS = DateTime.Now - StartTime;
                return TS.TotalMinutes;
            }
        }

        /// <summary>
        /// Start ArduinoSerial, and recording of the channel data
        /// </summary>
        /// <returns>Any Errors or blank string if everything is OK</returns>
        public string Start()
        {
            try
            {
                SP.Open();
            }
            catch
            {
                return "Error Opening Serial Port.";
            }

            ResetStartTime();
            SP.DiscardInBuffer(); //Clear out buffer
            SP.DataReceived += _SP_DataReceived;

            return "";
        }

        public void Stop()
        {
            if (SP == null) return;
            SP.DataReceived -= _SP_DataReceived;
            Thread.Sleep(250);
            if (SP.IsOpen)
            {
                SP.DiscardInBuffer();
                SP.Close();
            }
        }

        public void Reset()
        {
            if (SP.IsOpen) SP.Close();
            SP.PortName = PortName;
            SP.BaudRate = BaudRate;
        }

        private void _SP_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                ParentControl.ReceiveData(SP.ReadLine());
                LastReceived_SP = DateTime.Now;
            }
            catch //(Exception E)
            {

            }
        }

        ~SerialDevice()
        {
            this.Stop();
        }
    }

    public class PinsClass
    {
        public SerialDevice ParentDevice { get; private set; }
        public byte SENgT1 { get; set; }
        public byte SENgT2 { get; set; }
        public byte SENgT3 { get; set; }
        public byte LEDgT1 { get; set; }
        public byte LEDgT2 { get; set; }
        public byte LEDgT3 { get; set; }
        public byte Thermo1 { get; set; }
        public byte Thermo2 { get; set; }
        public byte Heater { get; set; }
        public byte Buzzer { get; set; }
        public byte LEDrgbR { get; set; }
        public byte LEDrgbG { get; set; }
        public byte LEDrgbB { get; set; }
        public PinsClass()
        {

        }
        public PinsClass(SerialDevice Parent)
        {
            ParentDevice = Parent;
        }
    }
}
