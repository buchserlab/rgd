﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;

namespace RGD_Library
{
    //----------------------------------------------------------------------------------------------------------------------------------------

    //Note, just because we are having this metadata file, I do NOT want to remove the metadata that is recorded in the raw run, at least for now
    public class Metadata_Table
    {
        private StoreClass _Store;

        public string FileName_Table => "RGD_" + DateTime.Today.ToString("yyyyMMdd") + ".xml";
        public string FullFileName => Path.Combine(_Store.BaseFolder, FileName_Table);

        public Metadata_Table(StoreClass store)
        {
            this._Store = store;
        }

        public void Add(ResultsClass results)
        {
            //Make a new Metadata Entry with these results
            //Metadata_Entry ME = new Metadata_Entry(results);

            bool IsNewFile = !File.Exists(FullFileName);
            string res = "error reading res";
            try
            {
                //Have the results ready to be written so there is no downtime
                res = results.ExportXML().ToString();
            }
            catch { }
            res = (IsNewFile ? "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<root>\r\n" : "") + res + Environment.NewLine;

            //Now open the file that is today's "table" of all results, and add this entry to it
            if (!IsNewFile)
            {
                for (int i = 0; i < 200; i++) //Total time that it will wait is 5 seconds
                {
                    if (!IsFileLocked()) break;
                    Thread.Sleep(25);
                }
            }
            File.AppendAllText(FullFileName, res);
        }

        // Copied from https://stackoverflow.com/questions/876473/is-there-a-way-to-check-if-a-file-is-in-use
        protected virtual bool IsFileLocked()
        {
            try
            {
                using (FileStream stream = File.Open(FullFileName, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }

            //file is not locked
            return false;
        }
    }

    public class Metadata_Entry
    {
        public ResultsClass Result;

        public Metadata_Entry(ResultsClass result)
        {
            Result = result;
        }

        internal string ExportEntry()
        {
            throw new NotImplementedException();
        }
    }
}
