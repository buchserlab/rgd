﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace RGD_Library
{
    /// <summary>
    /// These are the string packets that are received from the Device (FIRMWARE > SOFTWARE)
    /// </summary>
    public static class Packet_Data
    {
        public static long Error_Count = 0;

        public static string Receive(string toParse, TemperatureHeater tempH = null, TemperatureHeater tempH2 = null, TubesCollection tubes = null)
        {
            try
            {
                string[] arr = toParse.Split(' ');

                if (tempH != null)
                {
                    tempH.Update(float.Parse(arr[3]), float.Parse(arr[4]), arr[6]);
                    tempH2.Update(float.Parse(arr[5]), 0, "0");
                }
                if (tubes != null)
                {
                    string data1 = tubes[1].Channels[0].Add(float.Parse(arr[0]));
                    string data2 = tubes[2].Channels[0].Add(float.Parse(arr[1]));
                    string data3 = tubes[3].Channels[0].Add(float.Parse(arr[2]));
                    return $"{data1}\r\n{data2}\r\n{data3}";
                }
            }
            catch
            {
                //Occasionally things get gummed up . . just record and error count?
                Error_Count++;
            }
            return "";
        }

        /// <summary>
        /// The initial message is just sent when the serial is first connected to give info about the device
        /// </summary>
        internal static void Receive_Initial(string[] messages, out long deviceID, out double calSlope, out double calIntercept, out float OptimalValue, out float OptimalRange, out long Firmware_Version, out PinsClass Pins)
        {
            deviceID = -1; calSlope = 0; calIntercept = 0; Firmware_Version = -1; OptimalValue = 0; OptimalRange = 0; //Defaults
            Pins = new PinsClass();
            try
            {
                deviceID = long.Parse(messages[0]);
                calSlope = double.Parse(messages[1]) / 100; //Arduino cuts off some of the decimals with the string format, so this adds them back in
                calIntercept = double.Parse(messages[2]);
                Firmware_Version = long.Parse(messages[3]);

                Pins.SENgT1 = byte.Parse(messages[4]);
                Pins.SENgT2 = byte.Parse(messages[5]);
                Pins.SENgT3 = byte.Parse(messages[6]);

                Pins.LEDgT1 = byte.Parse(messages[7]);
                Pins.LEDgT2 = byte.Parse(messages[8]);
                Pins.LEDgT3 = byte.Parse(messages[9]);

                Pins.Thermo1 = byte.Parse(messages[10]);
                Pins.Heater = byte.Parse(messages[11]);

                Pins.Thermo2 = byte.Parse(messages[12]);
                OptimalValue = float.Parse(messages[13]);
                OptimalRange = float.Parse(messages[14]);

                Pins.Buzzer = byte.Parse(messages[15]);
                Pins.LEDrgbR = byte.Parse(messages[16]);
                Pins.LEDrgbG = byte.Parse(messages[17]);
                Pins.LEDrgbB = byte.Parse(messages[18]);
            }
            catch
            {
                //If some of it got through, just go with it . . 
            }
        }

        // The adds in this will not! write anything out to the disk
        internal static long Receive_Simulated(Random rnd, TemperatureHeater tempH, TemperatureHeater tempH2, TubesCollection tubes)
        {
            tubes[1].Channels[0].Add((float)rnd.NextDouble());
            tubes[3].Channels[0].Add((float)rnd.NextDouble());
            tempH.Update((float)rnd.NextDouble(), (short)rnd.Next(0, 10230), "0");
            tempH2.Update((float)rnd.NextDouble(), 0, "0");
            return -1;
        }
    }
}
