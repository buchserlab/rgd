﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace RGD_Library
{
    public class RunSchema
    {
        public SampleType Tube1Type;
        public SampleType Tube3Type;
        public SchemaFlags Flags = SchemaFlags.Regular;

        public RunSchema()
        {
            Tube1Type = SampleType.Sample;
            Tube3Type = SampleType.NegControl;
        }
        public RunSchema(SampleType tube1type, SampleType tube3type)
        {
            Tube1Type = tube1type;
            Tube3Type = tube3type;
        }
        public RunSchema(SampleType tube1type, SampleType tube3type, SchemaFlags flags)
        {
            Tube1Type = tube1type;
            Tube3Type = tube3type;
            Flags = flags;
        }
        public bool BothSamples { get => Tube1Type == SampleType.Sample && Tube3Type == SampleType.Sample; }
        public bool BothControls {
            get
            {
                return (Tube1Type == SampleType.PosControl || Tube1Type == SampleType.NegControl) &&
                (Tube3Type == SampleType.PosControl || Tube3Type == SampleType.NegControl);
            }
         }
        public static string SampleasChar(SampleType tubeType)
        {
            switch (tubeType)
            {
                case SampleType.PosControl:
                    return "+";
                case SampleType.NegControl:
                    return "\u2022";
                case SampleType.Sample:
                    return "s";
                default:
                    throw new Exception("Sample Type not found");
            }
        }

        public static string SampleAsString(SampleType tubeType, string id = "")
        {
            switch (tubeType)
            {
                case SampleType.PosControl:
                    return "Pos. Control";
                case SampleType.NegControl:
                    return "Neg. Control";
                case SampleType.Sample:
                    return $"Sample: {id}";
                default:
                    throw new Exception("Sample Type not found");
            }
        }
    }

    public class RunSchemaList : IEnumerable<RunSchema>
    {
        private List<RunSchema> _List;
        public RunSchema this[int index] => _List[index];

        public int Count => _List.Count;

        public static string DefaultFolder
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "RGD");
            }
        }
        public static string DefaultPath
        {
            get
            {
                return Path.Combine(DefaultFolder, "RGD_Schema.xml");
            }
        }

        public static string OldPath
        {
            get
            {
                return Path.Combine(Path.GetTempPath(), "RGD_Schema.xml");
            }
        }

        public RunSchemaList()
        {
            _List = new List<RunSchema>();
        }

        public RunSchemaList(List<RunSchema> list)
        {
            _List = list;
        }

        public RunSchemaList FilterBySchemaFlag(SchemaFlags schemaFlag)
        {
            return new RunSchemaList(this.Where(rs => (rs.Flags & schemaFlag) != SchemaFlags.None).ToList());
        }

        public bool ContainsSchemaFlags(SchemaFlags schemaFlag)
        {
            return this.Any(rs => (rs.Flags & schemaFlag) != SchemaFlags.None);
        }

        public void Add(RunSchema schema)
        {
            _List.Add(schema);
        }

        public IEnumerator<RunSchema> GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        public bool Save()
        {
            Directory.CreateDirectory(DefaultFolder);
            return Save(DefaultPath);
        }
        public bool Save(string Path)
        {
            try
            {
                XmlSerializer XMLSer = new XmlSerializer(typeof(RunSchemaList));
                StreamWriter SW = new StreamWriter(Path);
                XMLSer.Serialize(SW, this);
                SW.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool Load(out RunSchemaList rsl)
        {
            if (!File.Exists(DefaultPath) && File.Exists(OldPath)) return Load(OldPath, out rsl); // This is for backwards compatibility
            return Load(DefaultPath, out rsl);
        }

        public static bool Load(string FolderPath, out RunSchemaList rsl)
        {
            try
            {
                if (!File.Exists(FolderPath))
                {
                    rsl = new RunSchemaList(new List<RunSchema>() { 
                        new RunSchema(SampleType.PosControl, SampleType.NegControl, SchemaFlags.StartUpSchema),
                        new RunSchema(SampleType.NegControl, SampleType.PosControl, SchemaFlags.StartUpSchema),
                        new RunSchema(SampleType.PosControl, SampleType.Sample),
                        new RunSchema(SampleType.Sample, SampleType.NegControl),
                        new RunSchema(SampleType.NegControl, SampleType.Sample),
                        new RunSchema(SampleType.Sample, SampleType.PosControl),
                        new RunSchema(SampleType.NegControl, SampleType.PosControl, SchemaFlags.EndofDaySchema)
                    });
                    rsl.Save();
                    return true;
                }
                XmlSerializer XMLSer = new XmlSerializer(typeof(RunSchemaList));
                StreamReader SR = new StreamReader(FolderPath);
                rsl = (RunSchemaList)XMLSer.Deserialize(SR);
                SR.Close();
                return true;
            }
            catch
            {
                //Probably didn't exist, so just make a new one
                rsl = new RunSchemaList();
                return false;
            }
        }
    }
}
