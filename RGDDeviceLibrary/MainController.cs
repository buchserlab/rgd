﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RGD_Library
{
    public class MainController
    {
        public TubesCollection Tubes;
        public SerialDevice Device;
        public long GetDeviceID => Device.deviceID;
        public StoreClass Store;
        public RunSchemaList Schema;
        public RGDFlags Flags;

        public TemperatureHeater tempHeater { get; private set; }
        public TemperatureHeater tempHeater2 { get; private set; }

        public string SubFolder => DateTime.Now.ToString("yyyy.MM.dd ") + Device.deviceID;
        public string FullFileName => Path.Combine(Store.BaseFolder, SubFolder, Device.RunID + ".txt");
        public StreamWriter SaveStream { get; internal set; }

        public float Temp_InTube_RunAvg => tempHeater.Avg;
        public float Temp_Holder_RunAvg => tempHeater2.Avg;

        public MainController()
        {
            Flags = new RGDFlags();
            InitController();
        }

        /// <summary>
        /// Initialization of the MainController
        /// </summary>
        /// <param name="flags">A RGDFlags object that is used to track the main controller</param>
        public MainController(RGDFlags flags)
        {
            Flags = flags;
            InitController();
        }

        /// <summary>
        /// Initiates the controller
        /// </summary>
        private void InitController()
        {
            Flags.GoodPath = StoreClass.Load(out Store);
            LoadSchema();
            Schema.Save();
            if (Store.SchemaIndex >= Schema.Count || Store.SchemaIndex < 0)
            {
                Store.SchemaIndex = 0;
            }
            SaveStream = null;
            ChannelClass.RecentBufferSize = Store.RecentBufferSize;
            Tubes = TubesCollection.SetupChannels_Prototype01(this);
            tempHeater = new TemperatureHeater(this);
            tempHeater2 = new TemperatureHeater(this);
            tempHeater2.ChannelName = "Temp V";
        }
        /// <summary>
        /// Establish connection to a device
        /// </summary>
        /// <param name="comPort">The port that the device is connected to</param>
        public void ConnectToDevice(string comPort)
        {
            Device = new SerialDevice(this, comPort);
            Tubes = TubesCollection.SetupChannels_Prototype01(this);
            tempHeater = new TemperatureHeater(this);
            tempHeater2 = new TemperatureHeater(this);
            tempHeater2.ChannelName = "Temp V";
        }
        /// <summary>
        /// Start taking in information from the connected device
        /// </summary>
        /// <returns></returns>
        public string StartDevice()
        {
            if (Store.SaveChannelData) SaveChannelData_StartNew(false);
            return Device.Start();
        }
        /// <summary>
        /// Stops the device and stops taking in information
        /// </summary>
        public void StopDevice()
        {
            Device.Stop();
            if (SaveStream != null) SaveStream.Close();
            Tubes.Process_Stopped();
            ChannelsReset();
        }

        private void SaveChannelData_StartNew(bool isRun = false)
        {
            //Check that existing ones are closed out
            if (SaveStream != null) SaveStream.Close();

            //Run ID defines the file name, so set it first
            Device.ResetStartTime();
            Device.ResetRunID(isRun);

            //Check that the folder is real
            Directory.CreateDirectory(Path.Combine(Store.BaseFolder, SubFolder));

            //Open stream and Write out the headers
            SaveStream = new StreamWriter(FullFileName);
            WriteOutData(Tubes[0].Channels[0].SaveToDisk(-1, true));
        }

        internal void WriteOutData(string data)
        {
            Directory.Exists(Store.BaseFolder);

            SaveStream.WriteLine(data);
        }

        internal string ExportPrefix(bool exportHeaders = false)
        {
            if (exportHeaders)
                return "Minutes" + "\t" + "Time" + "\t" + "deviceID" + "\t" + "RunID" + "\t" + "User" + "\t" + "Note2" + "\t" + "Temp" + "\t" + "TempV" + "\t";
            else
                return Device.Minutes + "\t" + DateTime.Now.ToString() + "\t" + Device.deviceID.ToString() + "\t" + Device.RunID + "\t" + Store.User + "\t" + Store.Subject + "\t" + tempHeater.Temperature_Current + "\t" + tempHeater2.Temperature_Current + "\t";
        }

        public void Channels_AdjustScaling(bool ResetScaling = false)
        {
            //So first find the baseline and subtract to bring the values down to start at 1
            float global_upper = -1; float max = -1;
            ChannelClass Chx;
            for (int i = 0; i < Tubes.Count; i++)
            {
                if (Tubes[i].Active)
                {
                    for (int ch = 0; ch < Tubes[i].Count; ch++)
                    {
                        Chx = Tubes[i].Channels[ch];
                        if (ResetScaling) Chx.ResetScaling();
                        else
                        {
                            if (Chx.Plateau > global_upper) global_upper = Chx.Plateau;
                            if (Chx.Max > max) max = Chx.Max;
                        }
                    }
                }
            }
            if (ResetScaling) return;
            //Since we now know the upper value from both channels, we can adjust the scaling for all together
            global_upper *= 1.15f; //Want to have a little room on the top (15%)
            for (int i = 0; i < Tubes.Count; i++)
            {
                if (Tubes[i].Active)
                {
                    for (int ch = 0; ch < Tubes[i].Count; ch++)
                    {
                        Chx = Tubes[i].Channels[ch];
                        Chx.AdjustScaling(global_upper);
                    }
                }
            }
        }

        public string QueryDevice()
        {
            return Device.Query();
        }

        public void LoadSchema()
        {
            RunSchemaList.Load(out Schema);
        }

        public string ExportTitle => (Device == null ? "" : (" Device#" + Device.deviceID.ToString("000")));

        public List<PlottableChannel> GetChannels()
        {
            List<PlottableChannel> Chs = new List<PlottableChannel>();
            for (int i = 1; i < Tubes.Count; i++)
            {
                if (Tubes[i].Active)
                {
                    Chs.AddRange(Tubes[i].Channels);
                }
            }
            return Chs;
        }

        /// <summary>
        /// Clears the recent list inside all of the tubes and channels
        /// </summary>
        public void ChannelsReset()
        {
            for (int i = 0; i < Tubes.Count; i++)
            {
                if (Tubes[i].Active)
                {
                    for (int ch = 0; ch < Tubes[i].Count; ch++)
                    {
                        Tubes[i].Channels[ch].Reset();
                    }
                }
            }
            Device.ResetStartTime();
        }
        /// <summary>
        /// Saves the recent queue from all the tubes for the channel specified in the float[]
        /// </summary>
        /// <param name="Channel"></param>
        /// <returns></returns>
        public List<float[]> ChannelsRecentData(int Channel)
        {
            List<float[]> tOut = new List<float[]>(Tubes.Count);
            for (int i = 0; i < Tubes.Count; i++)
            {
                if (Tubes[i].Active)
                {
                    tOut.Add(Tubes[i].Channels[Channel].Recent.ToArray());
                }
                else
                {
                    tOut.Add(null);
                }
            }
            return tOut;
        }

        public void Channels_SetupForLongRun(bool BothControls)
        {
            //Fix the channel settings and clear out for long run
            RunType rt = BothControls ? RunType.ShortenedLongRun : RunType.LongRun;
            Channels_SetBufferSize(rt);
            //Start saving this new file
            SaveChannelData_StartNew(true);
        }

        public void Channels_SetupForBasic()
        {
            Channels_SetBufferSize(RunType.BasicRun);
        }

        public void Channels_SetBufferSize(RunType runtype)
        {
            for (int i = 0; i < Tubes.Count; i++)
            {
                if (Tubes[i].Active)
                {
                    for (int ch = 0; ch < Tubes[i].Count; ch++)
                    {
                        Tubes[i].Channels[ch].SetBuffer(runtype, Store);
                    }
                }
            }
        }
        /// <summary>
        /// Resets the averaging for the temperature channels
        /// </summary>
        public void TempReset()
        {
            tempHeater.Reset();
            tempHeater2.Reset();
        }

        public bool IsHotEnough()
        {
            return tempHeater.Temperature_Current < (Store.Temperature_Ideal * 0.7);
        }

        /// <summary>
        /// Saves (Flushes) save stream buffer so that things are on the disk (but it stays open and can keep writing)
        /// </summary>
        public void ChannelsFlushBuffer()
        {
            ChannelClass Chx;
            for (int i = 0; i < Tubes.Count; i++)
            {
                if (Tubes[i].Active)
                {
                    for (int ch = 0; ch < Tubes[i].Count; ch++)
                    {
                        Chx = Tubes[i].Channels[ch];
                        Chx.FlushBuffer();
                    }
                }
            }
        }

        internal void ReceiveData(string message)
        {
            string datatowrite = Packet_Data.Receive(message, tempHeater, tempHeater2, Tubes);
            WriteOutData(datatowrite);
        }

        internal static ChannelClass CreatePlaceHolder()
        {
            var dev = new MainController();
            return dev.Tubes[1].Channels[0];
        }


        public string GetDeviceInfo(string port)
        {
            SerialDevice tDev = new SerialDevice(this, 9600, port);
            string t = tDev.Query();
            if (tDev.deviceID == -1) // If no device ID found
            {
                return "-1";
            }
            return tDev.deviceID.ToString("000 ") + "(" + port + ")";
        }

        public List<ResultsClass> GetResultsFromDevice()
        {
            List<ResultsClass> Sets = new List<ResultsClass>(3);

            //Store the data
            ResultsClass RC;
            foreach (TubeClass tube in Tubes)
            {
                if (tube.Active)
                {
                    foreach (ChannelClass channel in tube.Channels)
                    {
                        //Copy the channel data into the metadata store
                        RC = new ResultsClass(channel); Sets.Add(RC);
                    }
                }
            }
            return Sets;
        }

        public void LoadRawData(string RawFolder)
        {
            //RawFolder = @"R:\five\exp\FIV335\2020.06.11 17";
            //RawFolder = @"C:\Temp\Record\2020.06.22 4";
            ResultsSet_Class RS = LoadFromRawFolder(RawFolder);
            StoreClass STemp = new StoreClass(); STemp.BaseFolder = RawFolder;
            Metadata_Table MT = new Metadata_Table(STemp);
            foreach (ResultsClass results in RS)
            {
                MT.Add(results);
            }
        }
        public ChannelClass GenerateChannelFromData(float[] data)
        {
            var channel = new ChannelClass(null, -1);
            channel.SetBuffer(RunType.LongRun, Store);
            foreach (float datum in data) channel.DoEnqueue(datum);
            return channel;
        }

        public List<ResultsClass> LoadFromRaw(string fullNameRawfile, OutputVersion version)
        {
            Dictionary<string, ResultsClass> tRes = new Dictionary<string, ResultsClass>();
            List<string> Headers;
            int cTube; int cRunID = 0; int cVal; int cMetadata = 0; int cDeviceID; int cTemp; int cUser; int cSubject; int cMinutes; int cTempV;
            string TubeID;
            string[] Line;
            string t;
            using (StreamReader SR = new StreamReader(fullNameRawfile))
            {
                t = SR.ReadLine();
                Headers = t.Split('\t').ToList();
                cMinutes = Headers.IndexOf("Minutes"); cDeviceID = Headers.IndexOf("deviceID");
                cTube = Headers.IndexOf("TubeID"); cUser = Headers.IndexOf("User");
                cSubject = Headers.IndexOf("Note2"); if (cSubject < 0) cSubject = Headers.IndexOf("Subject");
                cVal = Headers.IndexOf("ChData"); cTemp = Headers.IndexOf("Temp");
                //Only in version 2+
                cMetadata = Headers.IndexOf("Metadata"); cRunID = Headers.IndexOf("RunID"); cTempV = Headers.IndexOf("TempV");

                while (!SR.EndOfStream)
                {
                    t = SR.ReadLine(); Line = t.Split('\t');
                    if (Line.Length < Headers.Count) continue;
                    TubeID = Line[cTube];
                    if (!tRes.ContainsKey(TubeID))
                    {
                        if (version == OutputVersion.V1)
                            tRes.Add(TubeID, new ResultsClass(Line[cDeviceID], TubeID, Line[cUser], Line[cSubject], fullNameRawfile));
                        else if (version == OutputVersion.V2)
                            tRes.Add(TubeID, new ResultsClass(Line[cDeviceID], TubeID, Line[cUser], Line[cSubject], Line[cRunID], Line[cMetadata]));
                    }
                    tRes[TubeID].AppendDataFromRaw(Line[cMinutes], Line[cVal], Line[cTemp], version >= OutputVersion.V3 ? Line[cTempV] : "0");
                }
                SR.Close();
            }
            foreach (ResultsClass results in tRes.Values)
            {
                if (version == OutputVersion.V1) results.Raw_RestrictToLastMinutes(30f);
                results.Raw_FinishLoad();
                results.Raw_Finished(GenerateChannelFromData(results.Raw_Data));
            }

            return tRes.Values.ToList();
        }
        public ResultsSet_Class LoadFromRawFolder(string FolderPath)
        {
            ResultsSet_Class RSC = new ResultsSet_Class();
            DirectoryInfo DI = new DirectoryInfo(FolderPath);
            foreach (FileInfo FI in DI.GetFiles("*.txt", SearchOption.AllDirectories))
            {
                if (FI.Name.EndsWith("_T.txt")) RSC.AddRange(LoadFromRaw(FI.FullName, OutputVersion.V2));
                else if (!FI.Name.EndsWith("_R.txt")) RSC.AddRange(LoadFromRaw(FI.FullName, OutputVersion.V1));
            }
            return RSC;
        }
    }
}
