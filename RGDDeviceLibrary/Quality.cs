﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;


namespace RGD_Library.Quality
{
    public class RuntimeCheck
    {
        public static string InitialCheck(MainController Control)
        {
            //Need to exclude the first ~2 minutes of the run (fraction = 0.0667 or 1/15)
            //This is already setup when the run starts . . all data now excludes that (once we are beyond that point)

            //3.Check the noise of the sensor channels so far
            //4.Check for all zeros

            //The lighting we can't check all times, but we assume that during this time it is pretty stable
            StringBuilder sB = new StringBuilder();
            float avg; float stdev;
            foreach (TubeClass tube in Control.Tubes)
            {
                if (tube.Active)
                {
                    foreach (ChannelClass channel in tube.Channels)
                    {
                        avg = channel.Avg; stdev = channel.StdDev;
                        sB.Append(channel.ChannelName + " avg " + avg.ToString("0.0") + " stdev " + stdev.ToString("0.0") + "\r\n");
                    }
                }
            }

            return sB.ToString();
        }

        public static string FrequentCheck(MainController Control)
        {
            //1.Check that TempV is within Ratio range of the Temperature(11.5 - 12.5)
            //2.Check that the temperature curve is relatively stable (StDev of Temperature) and Avg
            float ratio = Control.Temp_Holder_RunAvg / Control.Temp_InTube_RunAvg;
            float tempSD = Control.tempHeater.StdDev;
            float temp2SD = Control.tempHeater2.StdDev;
            float avgTemp = Control.Temp_InTube_RunAvg;
            return "tratio " + ratio.ToString("0.0") + " tempSD " + tempSD.ToString("0.0") + " temp2SD " +temp2SD.ToString("0.0") + " avgTemp " + avgTemp.ToString("0.0");
        }
    }

    public class QualityCriteria
    {

    }
}
