﻿using System;
namespace RGD_Library
{
    public enum SampleType
    {
        PosControl,
        NegControl,
        Sample,
        NotGiven
    }

    [Flags]
    public enum SchemaFlags
    {
        None = 0,
        Regular = 1,
        SamplesAreDifferent = 2,
        StartUpSchema = 4,
        EndofDaySchema = 8
    }
    public enum RunSchemaState
    {
        StartUp,
        Regular,
        ShutDown
    }
    public enum FitResult
    {
        ProductAmplification,
        BackgroundAmplification,
        NoAmplification,
        Ambiguous
    }

    public enum RunType
    {
        BasicRun,
        LongRun,
        ShortenedLongRun
    }

    public enum OutputVersion
    {
        V1,
        V2,
        V3
    }

    [Flags]
    public enum ResultsErrors
    {
        None = 0,
        NegControlPositive = 1,
        PosControlNegative = 2
    }

    public enum RGDControlState
    {
        NotQueried,
        Queried,
        Standby,
        RunLong
    }
}