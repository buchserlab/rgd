﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace RGD_Library
{
    public class ResultsSet_Class : IEnumerable<ResultsClass>
    {
        private List<ResultsClass> _List;
        public int Count => _List.Count;

        public List<ResultsClass> Result_Latest { get; private set; }

        public void Add(ResultsClass Result)
        {
            _List.Add(Result);
            Result_Latest = new List<ResultsClass>(1) { Result };
        }

        public ResultsSet_Class()
        {
            _List = new List<ResultsClass>();
        }

        public IEnumerator<ResultsClass> GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _List.GetEnumerator();
        }

        public void AddRange(List<ResultsClass> results)
        {
            _List.AddRange(results);
            Result_Latest = results;
        }

        public string ReportLatest()
        {
            StringBuilder sB = new StringBuilder();
            for (int i = 0; i < Result_Latest.Count; i++)
            {
                sB.Append(Result_Latest[i].Report() + "\r\n");
            }
            return sB.ToString();
        }

        public ResultsErrors LatestResultsErrors()
        {
            var errors = ResultsErrors.None;
            foreach (var result in Result_Latest)
            {
                errors = errors | result.CheckResults();
            }
            return errors;
        }
    }

    public class ResultsClass
    {
        public Dictionary<string, object> InformationDict;
        public float[] Raw_Data { get; internal set; }
        public float Baseline => (float)InformationDict[nameof(Baseline)];
        public float Plateau => (float)InformationDict[nameof(Plateau)];
        public int TubeID => (int)InformationDict[nameof(TubeID)];
        public int ChannelID => (int)InformationDict[nameof(ChannelID)];
        public string TubeMetadata => (string)InformationDict[nameof(TubeMetadata)];
        public double R2 => (double)InformationDict[nameof(FPs.R2)];
        public float X50_Minutes => (float)InformationDict[nameof(FPs.X50_Minutes)];
        public string FinalNote { get { return (string)InformationDict[nameof(FinalNote)]; } set { InformationDict[nameof(FinalNote)] = value; }  }

        public string AdditionalInfo
        {
            get
            {
                if (!InformationDict.ContainsKey(nameof(AdditionalInfo))) InformationDict.Add(nameof(AdditionalInfo), "");
                return (string)InformationDict[nameof(AdditionalInfo)];
            }
            set
            {
                InformationDict[nameof(AdditionalInfo)] = value;
            }
        }
        public FitResult Interpretation => (FitResult)InformationDict[nameof(Interpretation)];
        public SampleType TubeSampleType => (SampleType)InformationDict[nameof(TubeSampleType)];
        public float yDelta => (float)InformationDict[nameof(yDelta)];
        public float RiseTime_Minutes => (float)InformationDict[nameof(RiseTime_Minutes)];


        public CurveModel FPs;

        public ResultsClass()
        {

        }

        /// <summary>
        /// This is for pulling in raw data . . should be called only by specific piece
        /// </summary>
        public ResultsClass(string sDeviceID, string sTubeID, string sUser, string sSubject, string sRunID = "", string Metadata = "")
        {
            InformationDict = new Dictionary<string, object>();

            ChannelClass Chx = ChannelClass.PlaceHolderChannel();
            TubeClass Tube = Chx.ParentTube;
            MainController Con = Tube.ParentTubes.ParentControl;

            InformationDict.Add(nameof(Con.Device.RunID), sRunID);
            InformationDict.Add(nameof(Con.Device.deviceID), long.Parse(sDeviceID));
            InformationDict.Add(nameof(Con.Store.User), sUser);
            InformationDict.Add(nameof(Con.Store.Subject), sSubject);
            InformationDict.Add(nameof(Tube.TubeMetadata), Metadata);
            InformationDict.Add(nameof(Tube.TubeID), int.Parse(sTubeID));
            InformationDict.Add(nameof(Chx.ChannelID), 0);
        }

        public ResultsClass(ChannelClass Chx)
        {
            InformationDict = new Dictionary<string, object>();

            TubeClass Tube = Chx.ParentTube;
            MainController Con = Tube.ParentTubes.ParentControl;
            InformationDict.Add(nameof(Con.Device.RunID), Con.Device.RunID);
            InformationDict.Add(nameof(Con.Device.deviceID), Con.Device.deviceID);
            InformationDict.Add(nameof(Con.Store.User), Con.Store.User);
            InformationDict.Add(nameof(Con.Store.Subject), Con.Store.Subject);
            InformationDict.Add(nameof(Con.Temp_InTube_RunAvg), Con.Temp_InTube_RunAvg);
            InformationDict.Add(nameof(Con.Temp_Holder_RunAvg), Con.Temp_Holder_RunAvg);
            InformationDict.Add(nameof(Tube.TubeMetadata), Tube.TubeMetadata);
            InformationDict.Add(nameof(Tube.TubeID), Tube.TubeID);
            InformationDict.Add(nameof(Tube.TubeSampleType), Tube.TubeSampleType);
            InformationDict.Add(nameof(Chx.ChannelID), Chx.ChannelID);
            InformationDict.Add(nameof(Chx.Baseline), Chx.Baseline);
            InformationDict.Add(nameof(Chx.Plateau), Chx.Plateau);
            InformationDict.Add(nameof(FinalNote), "");

            Chx.GenerateCurveFit();
            PropertyInfo[] Props = typeof(CurveModel).GetProperties();
            foreach (PropertyInfo PI in Props) InformationDict[PI.Name] = PI.GetValue(Chx.CurveRegression);
        }

        private void aTest()
        {
            bool testing = true;
            if (testing)
            {
                ResultsClass tSet;
                tSet = new ResultsClass() { Raw_Data = new float[721] { 9f, 10.5f, 0f, 1f, 0.5f, 10f, 11f, 9f, 0f, 9.5f, 1.5f, 0f, 11f, 1f, 9f, 11f, 11f, 2f, 9.5f, 9.5f, 10f, 2f, 2f, 1f, 0.5f, 1.5f, 9f, 9.5f, 0f, 1f, 10.5f, 2f, 11f, 1f, 11f, 10f, 11f, 9.5f, 9.5f, 9f, 9f, 1.5f, 10.5f, 10.5f, 0.5f, 10f, 9.5f, 0.5f, 2f, 0.5f, 11f, 9f, 9.5f, 1f, 1f, 10.5f, 1f, 10f, 0f, 1.5f, 1f, 9f, 1f, 0f, 9f, 10.5f, 0f, 11f, 9.5f, 11f, 11f, 1.5f, 0f, 11f, 0f, 10f, 0f, 0.5f, 11f, 9.5f, 10f, 1.5f, 0.5f, 1.5f, 1.5f, 11f, 1.5f, 1.5f, 1f, 2f, 1f, 1.5f, 9.5f, 10f, 10f, 11f, 9f, 0.5f, 10f, 11f, 11f, 0f, 0f, 0.5f, 1f, 11f, 1.5f, 9f, 0.5f, 11f, 2f, 10f, 0f, 0.5f, 10.5f, 10.5f, 1f, 10f, 10f, 1.5f, 2f, 11f, 0f, 1f, 1f, 10.5f, 1.5f, 1.5f, 11f, 0f, 0.5f, 9f, 11f, 9f, 11f, 10f, 9f, 10f, 9f, 9.5f, 0f, 11f, 11f, 10.5f, 1.5f, 2f, 9f, 10.5f, 1f, 1f, 10f, 2f, 9.5f, 9f, 10.5f, 2f, 10.5f, 10f, 2f, 9f, 10f, 1.5f, 1.5f, 11f, 9.5f, 2f, 1f, 0f, 1f, 2f, 0f, 1.5f, 11f, 0.5f, 11f, 0.5f, 2f, 9.5f, 9.5f, 11f, 10f, 2f, 2f, 10.5f, 9f, 10f, 9f, 2f, 1f, 0.5f, 11f, 9.5f, 9.5f, 0f, 10f, 0f, 9.5f, 0.5f, 11f, 10.5f, 11f, 0f, 9f, 11f, 10.5f, 2f, 1f, 0f, 10f, 1f, 11f, 10f, 11f, 0.5f, 9f, 9.5f, 1f, 9f, 1.5f, 9.5f, 10.5f, 1.5f, 0.5f, 2f, 10f, 10.5f, 9f, 0f, 2f, 10.5f, 10f, 1f, 10.5f, 11f, 0f, 0f, 10.5f, 10f, 1f, 10.5f, 10f, 9f, 0f, 11f, 9.5f, 1.5f, 2f, 9.5f, 2f, 9.5f, 1.5f, 10.5f, 10f, 0f, 0f, 9f, 1.5f, 9f, 11f, 0.5f, 10f, 0f, 9f, 10f, 0f, 1.5f, 10f, 9.5f, 1.5f, 10.5f, 2f, 10f, 1f, 9f, 9.5f, 10f, 10.5f, 2f, 1.5f, 0.5f, 10.5f, 1f, 2f, 0f, 10f, 0.5f, 2f, 11f, 0f, 0f, 1f, 9.5f, 1.5f, 1.5f, 11f, 10f, 1f, 2f, 9.5f, 9f, 0f, 10f, 9f, 10f, 11f, 1f, 9.5f, 0f, 1.5f, 1.5f, 11f, 1.5f, 9f, 10.5f, 10.5f, 10f, 1f, 9.5f, 10f, 0.5f, 1.5f, 2f, 11f, 2f, 11f, 1.5f, 2f, 10f, 1f, 0.5f, 1.5f, 10f, 2f, 0f, 0.5f, 0.5f, 1.5f, 0.5f, 0.5f, 11f, 9f, 11f, 1f, 1.5f, 9f, 0.5f, 1.5f, 9.5f, 9.5f, 0.5f, 10f, 10f, 10.5f, 10.5f, 10f, 9.5f, 9.5f, 0.5f, 9f, 0f, 1f, 1.5f, 2f, 1.5f, 0.5f, 2f, 1.5f, 0.5f, 2f, 0.5f, 9.5f, 10.5f, 9.5f, 1.5f, 0f, 2f, 9.5f, 2f, 10f, 0.5f, 10.5f, 10.5f, 1f, 0f, 10f, 11f, 1.5f, 9f, 9.5f, 2f, 1f, 10.5f, 1.5f, 1f, 2f, 2f, 2f, 2f, 9.5f, 2f, 1.5f, 0f, 10.5f, 9.5f, 10.5f, 1.5f, 11f, 9f, 1.5f, 0.5f, 2f, 10f, 0.5f, 0f, 0f, 9.5f, 0f, 1f, 1f, 0.5f, 9.5f, 0.5f, 1.5f, 2f, 11f, 2f, 9.5f, 10.5f, 2f, 1.5f, 0f, 9f, 10.5f, 0.5f, 2f, 10f, 10f, 2f, 1.5f, 9.5f, 10.5f, 10.5f, 0.5f, 9f, 2f, 9.5f, 0f, 0.5f, 9f, 0f, 9.5f, 0.5f, 2f, 2f, 10.5f, 1.5f, 9.5f, 0f, 9f, 2f, 9.5f, 11f, 1.5f, 9.5f, 1.5f, 1f, 2f, 0f, 10f, 1.5f, 1.5f, 11f, 0.5f, 11f, 10f, 0.5f, 0f, 11f, 9f, 1.5f, 9.5f, 10f, 0.5f, 10.5f, 0f, 10.5f, 0.5f, 0f, 2f, 9.5f, 11f, 0.5f, 10.5f, 1f, 0.5f, 2f, 10.5f, 1f, 10.5f, 2f, 9.5f, 0.5f, 1.5f, 9f, 1f, 10f, 1.5f, 9.5f, 9f, 0f, 11f, 0.5f, 11f, 9.5f, 11f, 9f, 11f, 10f, 10.5f, 0f, 1f, 11f, 2f, 1f, 1f, 9.5f, 9f, 9f, 10f, 0.5f, 1f, 10f, 9f, 11f, 10.5f, 0f, 2f, 11f, 11f, 2f, 10.5f, 10f, 1.5f, 9.5f, 1f, 0.5f, 9.5f, 9.5f, 1f, 2f, 1.5f, 9f, 1.5f, 0f, 11f, 0.5f, 10.5f, 10f, 9.5f, 0.5f, 1f, 2f, 10f, 9f, 0.5f, 0.5f, 0.5f, 10f, 1f, 0f, 11f, 11f, 1f, 9.5f, 11f, 1.5f, 10f, 10f, 1f, 11f, 1.5f, 2f, 1f, 10.5f, 9.5f, 2f, 1.5f, 9.5f, 0f, 10.5f, 10f, 9f, 10f, 0.5f, 1f, 11f, 10.5f, 0.5f, 11f, 9.5f, 1.5f, 1.5f, 1f, 0f, 1.5f, 0f, 0.5f, 9.5f, 2f, 1.5f, 2f, 10.5f, 0.5f, 9f, 9f, 10f, 1.5f, 2f, 9.5f, 0.5f, 9f, 11f, 0.5f, 11f, 9f, 1.5f, 10f, 1f, 0.5f, 9.5f, 9.5f, 1f, 10f, 11f, 2f, 9.5f, 9f, 9f, 9f, 1.5f, 2f, 2f, 2f, 9.5f, 10.5f, 10.5f, 9f, 0f, 11f, 10f, 0.5f, 11f, 0f, 10f, 1.5f, 10f, 1.5f, 0f, 1f, 2f, 2f, 9f, 0f, 11f, 0.5f, 1.5f, 0.5f, 0.5f, 10f, 0f, 1f, 11f, 9.5f, 0f, 0f, 9f, 11f, 10f, 9.5f, 9f, 1f, 0f, 10.5f, 9f, 2f, 10f, 1.5f, 2f, 10.5f, 1.7f, 11.4f, 1.1f, 9.8f, 2f, 2.7f, 10.4f, 11.6f, 12.3f, 3f, 3.7f, 12.4f, 13.1f, 3.3f, 4.5f, 12.7f, 4.9f, 12.6f, 13.8f, 15f, 5.2f, 5.4f, 6.6f, 15.8f, 5f, 6.2f, 16.4f, 14.6f, 6.3f, 7f, 6.7f, 15.4f } };
                //tSet.Baseline = 5f; tSet.Plateau = 7.9f;

                tSet = new ResultsClass() { Raw_Data = new float[721] { 15f, 16.5f, 15f, 15f, 15f, 3f, 3f, 16.5f, 14f, 15.5f, 15f, 15f, 15f, 5.5f, 5f, 5.5f, 3f, 14.5f, 5.5f, 15.5f, 3f, 15.5f, 3f, 15.5f, 16.5f, 15.5f, 16f, 14f, 15f, 16.5f, 3f, 14f, 14f, 14.5f, 14f, 14f, 3f, 5.5f, 5f, 14.5f, 16.5f, 4f, 5f, 4f, 14f, 15f, 14.5f, 14.5f, 16f, 15f, 14.5f, 4f, 16f, 4f, 16.5f, 15.5f, 14.5f, 4.5f, 5.5f, 4f, 3.5f, 14f, 4f, 15.5f, 4f, 4.5f, 5.5f, 4.5f, 14f, 4.5f, 5.5f, 5.5f, 16.5f, 16.5f, 16.5f, 16.5f, 5.5f, 15.5f, 4.5f, 14f, 16.5f, 15.5f, 14.5f, 5.5f, 14.5f, 16f, 5.5f, 14.5f, 3f, 14f, 3f, 4f, 5.5f, 14f, 15.5f, 15.5f, 16f, 3f, 16f, 15f, 3f, 15f, 5f, 3f, 16.5f, 14f, 16.5f, 14f, 4.5f, 3.5f, 3f, 16.5f, 16.5f, 5.5f, 16.5f, 14.5f, 15f, 14f, 3.5f, 14.5f, 3f, 5.5f, 16.5f, 3.5f, 5f, 4f, 5f, 15f, 14.5f, 3.5f, 5f, 15.5f, 15f, 4f, 4f, 4f, 5.5f, 15.5f, 5f, 15f, 14.5f, 3f, 5.5f, 3f, 5.5f, 3f, 4f, 3.5f, 4f, 14.5f, 15.5f, 16.5f, 16f, 4.5f, 16f, 15.5f, 15.5f, 3.5f, 15.5f, 4.5f, 5.5f, 4f, 3.5f, 5f, 15f, 16.5f, 14f, 15f, 14.5f, 15.5f, 14f, 3f, 15f, 3.5f, 14f, 14f, 15.5f, 14.5f, 14f, 16.5f, 3f, 3f, 4.5f, 3f, 5f, 3f, 14f, 4f, 3f, 15f, 3f, 4f, 16f, 16.5f, 14f, 16.5f, 15.5f, 4.5f, 4.5f, 14f, 5.5f, 15f, 3f, 15.5f, 14.5f, 5f, 16.5f, 4.5f, 5.5f, 14.5f, 3f, 16.5f, 15f, 4f, 5f, 4.5f, 5f, 14f, 3.5f, 15.5f, 4f, 14.5f, 16.5f, 4.5f, 15f, 3f, 15.5f, 15.5f, 16f, 3f, 15f, 16.5f, 5.5f, 14f, 4f, 3.5f, 16.5f, 5.5f, 5.5f, 14f, 14.5f, 5.5f, 4f, 16f, 15f, 16.5f, 16.5f, 3.5f, 3.5f, 3.5f, 14.5f, 5f, 15.5f, 14f, 4.5f, 3.5f, 14f, 14f, 3.5f, 3.5f, 4f, 5f, 4.5f, 15.5f, 16.5f, 5.5f, 15f, 16.5f, 14.5f, 14.5f, 4f, 3f, 15.5f, 15f, 3.5f, 14.5f, 4.5f, 3.5f, 3f, 14.5f, 4.5f, 15.5f, 4f, 3f, 5f, 14f, 3.1f, 3.7f, 4.3f, 5.9f, 6f, 5.7f, 5.4f, 5.1f, 6.8f, 6.5f, 7.2f, 17.4f, 5.1f, 17.3f, 19f, 16.7f, 18.9f, 19.6f, 7.3f, 18f, 6.7f, 19.4f, 20.1f, 20.3f, 10f, 19.2f, 21.4f, 9.6f, 19.8f, 11f, 19.7f, 20.4f, 21.6f, 21.3f, 21f, 23.2f, 22.9f, 11.1f, 12.8f, 22.5f, 22.2f, 11.4f, 24.6f, 23.8f, 13.5f, 24.7f, 12.9f, 12.1f, 24.8f, 24f, 14.7f, 15.4f, 24.1f, 26.3f, 25f, 14.2f, 26.9f, 27.1f, 15.8f, 27f, 17.2f, 26.9f, 26.6f, 27.8f, 18f, 17.2f, 28.9f, 29.6f, 16.3f, 28f, 18.1f, 28.2f, 28.8f, 29.9f, 18f, 18.1f, 19.2f, 28.8f, 28.4f, 19f, 18.6f, 18.2f, 30.3f, 19.4f, 19.5f, 18.6f, 19.2f, 29.8f, 20.4f, 19f, 31.1f, 31.7f, 20.8f, 21.4f, 20.5f, 30.1f, 31.2f, 20.8f, 20.9f, 21f, 21.1f, 22.2f, 32.8f, 31.4f, 31.5f, 22.6f, 22.2f, 33.3f, 32.4f, 33f, 22.1f, 32.7f, 21.8f, 22.4f, 32.5f, 21.1f, 23.2f, 33.8f, 22.9f, 34.5f, 35.1f, 21.7f, 23.3f, 32.9f, 34.5f, 24.6f, 33.2f, 34.8f, 35.4f, 33.5f, 23.6f, 34.7f, 33.8f, 36.4f, 36.5f, 25.1f, 35.7f, 23.8f, 25.9f, 35.5f, 35.6f, 34.7f, 36.3f, 24.4f, 37f, 37.1000000000001f, 25.2f, 36.8000000000001f, 26.9000000000001f, 36.5000000000001f, 38.1000000000001f, 36.7000000000001f, 25.2000000000001f, 25.2000000000001f, 35.7000000000001f, 35.7000000000001f, 25.2000000000001f, 25.7000000000001f, 35.7000000000001f, 37.7000000000001f, 35.7000000000001f, 36.2000000000001f, 37.7000000000001f, 36.7000000000001f, 37.7000000000001f, 36.2000000000001f, 35.7000000000001f, 38.2000000000001f, 26.2000000000001f, 35.7000000000001f, 36.2000000000001f, 35.7000000000001f, 26.7000000000001f, 25.7000000000001f, 27.2000000000001f, 36.2000000000001f, 24.7000000000001f, 24.7000000000001f, 37.2000000000001f, 24.7000000000001f, 36.7000000000001f, 37.2000000000001f, 37.2000000000001f, 24.7000000000001f, 24.7000000000001f, 27.2000000000001f, 25.7000000000001f, 26.7000000000001f, 24.7000000000001f, 36.7000000000001f, 37.7000000000001f, 25.2000000000001f, 36.2000000000001f, 27.2000000000001f, 36.2000000000001f, 26.7000000000001f, 38.2000000000001f, 36.7000000000001f, 27.2000000000001f, 37.2000000000001f, 26.2000000000001f, 35.7000000000001f, 25.2000000000001f, 35.7000000000001f, 37.2000000000001f, 25.7000000000001f, 38.2000000000001f, 37.2000000000001f, 36.7000000000001f, 36.2000000000001f, 36.2000000000001f, 24.7000000000001f, 26.2000000000001f, 36.2000000000001f, 36.2000000000001f, 25.2000000000001f, 36.7000000000001f, 38.2000000000001f, 36.7000000000001f, 25.2000000000001f, 36.7000000000001f, 38.2000000000001f, 25.2000000000001f, 38.2000000000001f, 24.7000000000001f, 37.2000000000001f, 37.2000000000001f, 26.2000000000001f, 36.2000000000001f, 35.7000000000001f, 37.2000000000001f, 36.7000000000001f, 37.2000000000001f, 35.7000000000001f, 35.7000000000001f, 38.2000000000001f, 26.7000000000001f, 25.7000000000001f, 26.7000000000001f, 25.7000000000001f, 27.2000000000001f, 37.7000000000001f, 35.7000000000001f, 25.2000000000001f, 37.2000000000001f, 35.7000000000001f, 38.2000000000001f, 26.7000000000001f, 35.7000000000001f, 36.7000000000001f, 25.2000000000001f, 36.2000000000001f, 37.7000000000001f, 25.7000000000001f, 25.2000000000001f, 36.7000000000001f, 35.7000000000001f, 26.2000000000001f, 26.2000000000001f, 36.7000000000001f, 36.2000000000001f, 37.2000000000001f, 38.2000000000001f, 26.2000000000001f, 36.2000000000001f, 35.7000000000001f, 36.7000000000001f, 38.2000000000001f, 26.7000000000001f, 25.2000000000001f, 27.2000000000001f, 36.7000000000001f, 37.7000000000001f, 24.7000000000001f, 37.2000000000001f, 38.2000000000001f, 36.7000000000001f, 24.7000000000001f, 26.2000000000001f, 24.7000000000001f, 26.7000000000001f, 35.7000000000001f, 37.7000000000001f, 36.2000000000001f, 27.2000000000001f, 25.2000000000001f, 35.7000000000001f, 38.2000000000001f, 25.7000000000001f, 25.2000000000001f, 24.7000000000001f, 37.7000000000001f, 38.2000000000001f, 35.7000000000001f, 38.2000000000001f, 26.2000000000001f, 38.2000000000001f, 25.7000000000001f, 35.7000000000001f, 37.7000000000001f, 27.2000000000001f, 37.7000000000001f, 26.7000000000001f, 26.7000000000001f, 36.7000000000001f, 27.2000000000001f, 37.7000000000001f, 25.7000000000001f, 38.2000000000001f, 27.2000000000001f, 36.7000000000001f, 37.2000000000001f, 27.2000000000001f, 25.7000000000001f, 25.2000000000001f, 36.7000000000001f, 24.7000000000001f, 35.7000000000001f, 35.7000000000001f, 24.7000000000001f, 37.7000000000001f, 25.7000000000001f, 35.7000000000001f, 38.2000000000001f, 37.7000000000001f, 37.7000000000001f, 36.7000000000001f, 36.7000000000001f, 37.2000000000001f, 24.7000000000001f, 25.7000000000001f, 37.7000000000001f, 27.2000000000001f, 35.7000000000001f, 38.2000000000001f, 25.7000000000001f, 26.2000000000001f, 35.7000000000001f, 37.2000000000001f, 38.2000000000001f, 25.7000000000001f, 36.7000000000001f, 36.2000000000001f, 25.7000000000001f, 26.2000000000001f, 27.2000000000001f, 25.2000000000001f, 36.2000000000001f, 26.7000000000001f, 27.2000000000001f, 26.2000000000001f, 25.7000000000001f, 27.2000000000001f, 27.2000000000001f, 26.7000000000001f, 37.2000000000001f, 27.2000000000001f, 24.7000000000001f, 35.7000000000001f, 38.2000000000001f, 36.2000000000001f, 27.2000000000001f, 37.2000000000001f, 38.2000000000001f, 25.2000000000001f, 26.2000000000001f, 25.7000000000001f, 25.2000000000001f, 35.7000000000001f, 36.7000000000001f, 37.2000000000001f, 37.2000000000001f, 27.2000000000001f, 25.2000000000001f, 26.7000000000001f, 27.2000000000001f, 36.7000000000001f, 36.7000000000001f, 37.7000000000001f, 25.7000000000001f, 37.7000000000001f, 37.7000000000001f, 25.7000000000001f, 38.2000000000001f, 26.7000000000001f, 26.7000000000001f, 26.7000000000001f, 26.7000000000001f, 37.7000000000001f, 24.7000000000001f, 26.2000000000001f, 24.7000000000001f, 36.2000000000001f, 37.2000000000001f, 25.2000000000001f, 36.7000000000001f, 27.2000000000001f, 25.2000000000001f, 26.2000000000001f, 35.7000000000001f, 25.7000000000001f, 25.7000000000001f, 37.2000000000001f, 24.7000000000001f, 26.2000000000001f, 25.2000000000001f, 37.7000000000001f, 37.7000000000001f, 24.7000000000001f, 26.2000000000001f, 35.7000000000001f, 24.7000000000001f, 25.7000000000001f, 35.7000000000001f, 26.2000000000001f, 25.2000000000001f, 36.7000000000001f, 26.7000000000001f, 36.2000000000001f, 25.2000000000001f, 24.7000000000001f, 25.2000000000001f, 24.7000000000001f, 36.2000000000001f, 24.7000000000001f, 26.2000000000001f, 26.7000000000001f, 27.2000000000001f, 36.7000000000001f, 26.2000000000001f, 37.2000000000001f, 25.7000000000001f, 35.7000000000001f, 36.7000000000001f, 24.7f } };
                //tSet.Baseline = 11.2f; tSet.Plateau = 30.0f;
            }
        }

        // Exports the information in tab-delimited format
        public string ExportInformation()
        {
            return String.Join("\t", InformationDict.OrderBy(i => i.Key).Select(v => v.Value.ToString()));
        }

        // Exports the information in XML format
        public XElement ExportXML()
        {
            return new XElement("Run", InformationDict.Select(kv => new XElement(kv.Key, kv.Value)));
        }

        internal string Report()
        {
            string delim = "  ";
            return String.Join(delim, 
                new string[] 
                { 
                    TubeMetadata, Interpretation.ToString(), "X50:" + X50_Minutes.ToString("0.0"), "RiseTime:" + RiseTime_Minutes.ToString("0.0"),
                    "yDelta:" + yDelta.ToString("0.0"), "r2:" + R2.ToString("0.00")
                } 
            );
        }

        public ResultsErrors CheckResults()
        {
            if (TubeSampleType == SampleType.PosControl && (Interpretation == FitResult.NoAmplification || Interpretation == FitResult.BackgroundAmplification)) 
            {
                return ResultsErrors.PosControlNegative;
            } 
            else if (TubeSampleType == SampleType.NegControl && Interpretation == FitResult.ProductAmplification)
            {
                return ResultsErrors.NegControlPositive;
            }
            return ResultsErrors.None;
        }

        /// <summary>
        /// Older versions started recording the file BEFORE the Run button is pressed, so we have to take just the last x# of minutes specified, and delete the rest
        /// </summary>
        /// <param name="MinutesToRestrict"></param>
        public void Raw_RestrictToLastMinutes(float MinutesToRestrict)
        {
            float MaxMinutes = Raw_Minutes.Max();
            if (MaxMinutes <= MinutesToRestrict) return;
            float EarliesTime = MaxMinutes - MinutesToRestrict;
            while (Raw_Minutes[0] < EarliesTime)
            {
                Raw_Minutes.RemoveAt(0);
                Raw_ChValue.Remove(0);
                Raw_Temp.RemoveAt(0);
                Raw_TempV.RemoveAt(0);
            }
        }

        //These are only used to reconstruct loaded raw data
        private List<float> Raw_Temp;
        private List<float> Raw_TempV;
        private List<float> Raw_ChValue;
        private List<float> Raw_Minutes;

        public void AppendDataFromRaw(string sMinutes, string ChValue, string Temp, string TempV)
        {
            if (Raw_Temp == null)
            {
                Raw_Temp = new List<float>(); Raw_ChValue = new List<float>(); Raw_TempV = new List<float>(); Raw_Minutes = new List<float>();
            }
            float t;
            t = 0; float Minutes = float.Parse(sMinutes); Raw_Minutes.Add(Minutes);
            t = 0; float.TryParse(ChValue, out t); Raw_ChValue.Add(t);
            t = 0; float.TryParse(Temp, out t); Raw_Temp.Add(t);
            t = 0; float.TryParse(TempV, out t); Raw_TempV.Add(t);
        }

        public static float GetAvgInterval(float[] Arr)
        {
            double IntervalSum = 0;
            for (int i = 1; i < Arr.Length; i++)
            {
                IntervalSum += (Arr[i] - Arr[i - 1]);
            }
            return (float)IntervalSum / (Arr.Length - 1);
        }

        public void Raw_FinishLoad()
        {
            Raw_Data = Raw_ChValue.ToArray();
        }

        public void Raw_Finished(ChannelClass channel)
        {
            Raw_Data = Raw_ChValue.ToArray();

            InformationDict.Add("Temp_InTube_RunAvg", Raw_Temp.Average());
            //InformationDict.Add(nameof(Dev.Temp_Holder_RunAvg), Dev.Temp_Holder_RunAvg);
            InformationDict.Add("Baseline", Raw_Data.Min());
            InformationDict.Add("Plateau", Raw_Data.Max());

            //Fit the curves (and save to InformationDict)

            //Actually fit the dataset
            var curveModel = new CurveModel(channel);

            //This adjusts the output to make it more viewable.  If you do it earlier, it gets overwritten

            curveModel.MinutesPerDataPoint = GetAvgInterval(Raw_Minutes.ToArray());
            curveModel.MinutesExcluded = 0;
            curveModel.Fit();

            PropertyInfo[] Props = typeof(CurveModel).GetProperties();
            foreach (PropertyInfo PI in Props) InformationDict[PI.Name] = PI.GetValue(curveModel);
        }

        public List<string> Keys => InformationDict.Keys.ToList();
    }
}
