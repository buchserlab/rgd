﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace RGD_Library
{
    /// <summary>
    /// Represents all the Tubes data over time
    /// </summary>
    public class TubesCollection : IEnumerable<TubeClass>
    {
        public MainController ParentControl { get; private set; }
        public List<TubeClass> Tubes { get; private set; }
        public TubeClass this[int index] { get => Tubes[index]; }
        public int Count { get => Tubes.Count; }

        public TubesCollection(int TubesCount, int ChannelsPerTube, MainController control)
        {
            ParentControl = control;
            Tubes = new List<TubeClass>(TubesCount + 1);
            Tubes.Add(new TubeClass(this, ChannelsPerTube, 0, false));
            for (int i = 0; i < TubesCount; i++)
            {
                Tubes.Add(new TubeClass(this, ChannelsPerTube, i + 1, true));
            }
        }

        internal void Process_Stopped()
        {

        }

        internal IEnumerable<ChannelClass> AllChannels()
        {
            List<ChannelClass> tChs = new List<ChannelClass>();
            for (int t = 1; t < Count; t++)
            {
                if (Tubes[t].Active)
                {
                    for (int ch = 0; ch < Tubes[t].Count; ch++)
                    {
                        tChs.Add(Tubes[t].Channels[ch]);
                    }
                }
            }
            return tChs;
        }

        internal static TubesCollection SetupChannels_Prototype01(MainController controller)
        {
            //This is based on the current device prototype architecture
            TubesCollection TC = new TubesCollection(3, 1, controller);
            TC.Tubes[2].Active = false;
            return TC;
        }

        public IEnumerator<TubeClass> GetEnumerator()
        {
            return Tubes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Tubes.GetEnumerator();
        }
    }

    /// <summary>
    /// Represents one Tube in the Device
    /// </summary>
    public class TubeClass
    {
        public List<ChannelClass> Channels { get; private set; }
        public ChannelClass this[int index] { get => Channels[index]; }
        public int Count { get => Channels.Count; }
        public bool Active { get; set; }
        public SampleType TubeSampleType = SampleType.NotGiven;
        public override string ToString()
        {
            return TubeID.ToString();
        }
        public TubesCollection ParentTubes { get; private set; }
        public string TubeMetadata
        {
            get
            {
                switch (TubeID)
                {
                    case 1:
                        return ParentTubes.ParentControl.Store.Metadata_Tube1;
                    case 2:
                        return ParentTubes.ParentControl.Store.Metadata_Tube2;
                    case 3:
                        return ParentTubes.ParentControl.Store.Metadata_Tube3;
                    default:
                        return "";
                }
            }
        }
        public int TubeID { get; internal set; }

        public TubeClass(TubesCollection CollectionParent, int TubeID)
        {
            this.TubeID = TubeID;
            ParentTubes = CollectionParent;
            Channels = new List<ChannelClass>();
            Active = true;
        }

        public TubeClass(TubesCollection CollectionParent, int channelsPerTube, int TubeID, bool IsActive)
        {
            this.TubeID = TubeID;
            ParentTubes = CollectionParent;
            Channels = new List<ChannelClass>(channelsPerTube);
            for (int i = 0; i < channelsPerTube; i++)
            {
                Channels.Add(new ChannelClass(this, i));
            }
            Active = IsActive;
        }

        /// <summary>
        /// This is what the Tube exports in terms of per-line headers
        /// </summary>
        internal string ExportPrefix(bool exportHeaders = false)
        {
            return ParentTubes.ParentControl.ExportPrefix(exportHeaders) + (exportHeaders ? "TubeID\tMetadata\t" : TubeID + "\t" + TubeMetadata + "\t");
        }
    }
}
