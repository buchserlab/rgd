﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace RGD_Library
{
    public class PlottableChannel
    {
        public void initPC(int recentBufferSize, int exclude_Samples = 0, int initialZoom = 1)
        {
            Recent = new Queue<float>(recentBufferSize);
            Times = new Queue<DateTime>(recentBufferSize);

            BufferSize = recentBufferSize;
            InitialZoom = initialZoom;
            Exclude_Samples = exclude_Samples;
        }

        private object locker = new object();
        public Queue<float> Recent { get; internal set; }

        public Queue<DateTime> Times { get; internal set; }

        /// <summary>
        /// Number of samples (front) to exclude from various analyses. So if you set to 2, then it will exclude the first 2 samples before the calculation.  Set to 0 to "turn off".
        /// </summary>
        public int Exclude_Samples { get; set; }

        /// <summary>
        /// Returns an array that skips over the first fex elements, as defined by Exclude_Samples
        /// </summary>
        public IEnumerable<float> Recent_Exclude
        {
            get
            {
                if (Exclude_Samples < 1) return Recent;
                if (Recent.Count <= Exclude_Samples) return Recent;
                return Recent.Skip(Exclude_Samples);
            }
        }

        public int BufferSize { get; set; }
        public string ChannelName { get; set; }

        /// <summary>
        /// These are the maximum and minimum values DISPLAYED on the axes
        /// </summary>
        public RectangleF rectDisplay { get; set; }

        /// <summary>
        /// Max and Min values that are plotted from the underlying data
        /// </summary>
        public RectangleF rectValue { get; set; }

        /// <summary>
        /// Allows for the x-axis to be "zoomed in", 1 will turn it off
        /// </summary>
        public double InitialZoom { get; set; }

        /// <summary>
        /// Uses the times that data has been enqueued to calculate the time in MSEC per sample
        /// </summary>
        public float SampleInterval_MSEC()
        {
            if (Times.Count < 2) return 1;
            DateTime[] TimeArr = Times.ToArray();
            double IntervalSum = 0;
            for (int i = 1; i < TimeArr.Length; i++)
            {
                IntervalSum += (TimeArr[i] - TimeArr[i - 1]).TotalMilliseconds;
            }
            return (float)IntervalSum / (Times.Count - 1);
        }

        /// <summary>
        /// Checks the average intervals and returns the time per sample in minutes
        /// </summary>
        /// <returns></returns>
        public float SampleTime_Minutes()
        {
            return SampleInterval_MSEC() / (60 * 1000);
        }

        /// <summary>
        /// Call this to Enqueue a Sample
        /// </summary>
        /// <param name="newValue"></param>
        public void DoEnqueue(float newValue)
        {
            lock (locker)
            {
                LatestValue = newValue;

                Recent.Enqueue(newValue);
                Times.Enqueue(DateTime.Now);

                if (Recent.Count > BufferSize) Recent.Dequeue();
                if (Times.Count > BufferSize) Times.Dequeue();
            }
        }

        /// <summary>
        /// Calculates the Average of the values in the Queue, but will exclude the first samples based on Exclude_Samples value
        /// </summary>
        public float Avg => Recent_Exclude.Count() == 0 ? 1 : Recent_Exclude.Average();

        public float Med
        {
            get
            {
                IEnumerable<float> sortedNumbers = Recent_Exclude.OrderBy(n => n);
                float median = 1;
                int count = sortedNumbers.Count();
                int halfIndex = count / 2;
                int halfIndex_1 = halfIndex - 1;
                if ((count % 2) == 0)
                {
                    median = ((sortedNumbers.ElementAt(halfIndex)
                        + sortedNumbers.ElementAt(halfIndex_1)) / 2);
                }
                else
                {
                    median = sortedNumbers.ElementAt(halfIndex);
                }
                return median;
            }
        }
        public float Min => Recent_Exclude.Count() == 0 ? 0.1f : Recent_Exclude.Min();
        public float Max => Recent_Exclude.Count() == 0 ? 1 : Recent_Exclude.Max();
        public float Avg_Upper
        {
            get
            {
                try
                {
                    if (Recent_Exclude.Count() == 0) return 1;
                    float thresh = Max - ((Max - Avg) / 3); //Anything that is at least halfway between Avg and Max
                    float avg = Recent_Exclude.Where(x => x > thresh).Average();
                    return avg;
                }
                catch
                {
                    return float.NaN;
                }
            }
        }

        public float Avg_Lower
        {
            get
            {
                try
                {
                    if (Recent_Exclude.Count() == 0) return 1;
                    float thresh = Min + ((Avg - Min) / 3); //Anything that is at most halfway between Min and Avg
                    float avg = Recent_Exclude.Where(x => x < thresh).Average();
                    return avg;
                }
                catch
                {
                    return float.NaN;
                }
            }
        }

        /// <summary>
        /// This is the estimate of the "baseline" . .starting point
        /// </summary>
        public float Baseline
        {
            get
            {
                return float.IsNaN(Avg_Lower) ? 0 : Avg_Lower; //For now
            }
        }

        /// <summary>
        /// This is the Avg_Upper //- Baseline for now
        /// </summary>
        public float Plateau
        {
            get
            {
                if (float.IsNaN(Avg_Upper)) return 1;
                return Avg_Upper; // - Baseline;
            }
        }

        public float StdDev
        {
            get
            {
                IEnumerable<float> values = Recent_Exclude;
                float ret = 0;
                int count = values.Count();
                if (count > 1)
                {
                    //Compute the Average
                    double avg = values.Average();

                    //Perform the Sum of (value-avg)^2
                    double sum = values.Sum(d => (d - avg) * (d - avg));

                    //Put it all together
                    ret = (float)Math.Sqrt(sum / count);
                }
                return ret;
            }
        }
        public float LatestValue { get; internal set; }

        public void SetBuffer(RunType runtype, StoreClass store)
        {
            ChannelBufferParams cbp = null;
            switch (runtype)
            {
                case RunType.BasicRun:
                    cbp = store.BasicRunParams;
                    break;
                case RunType.LongRun:
                    cbp = store.LongRunParams;
                    break;
                case RunType.ShortenedLongRun:
                    cbp = store.ShortenedLongRunParams;
                    break;
            }

            if (cbp.DoReset) Reset();
            BufferSize = Math.Max(24, cbp.SecondsToDisplay * 1000 / cbp.MSEC_Timing) + 1;
            Exclude_Samples = (int)(cbp.SecondsToExclude * 1000 / cbp.MSEC_Timing);
            rectDisplay = new System.Drawing.RectangleF(rectDisplay.X, rectDisplay.Y,cbp.UseMinutes_ForDisplay ? cbp.SecondsToDisplay / 60 : cbp.SecondsToDisplay, rectDisplay.Height);
            InitialZoom = cbp.Zoom;
        }

        public void Reset()
        {
            Recent.Clear();
            Times.Clear();
        }
    }

}
