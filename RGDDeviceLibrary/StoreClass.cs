﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace RGD_Library
{
    /// <summary>
    /// Use to store the parameters in the UI and also to pass parameters around between units
    /// </summary>
    public class StoreClass
    {
        public string BaseFolder { get; set; }
        public bool SaveChannelData { get; set; }
        public bool SimulationMode { get; set; }
        public string COMPort { get; set; }
        public string User { get; set; }
        public string Subject { get; set; }
        public string Metadata_Tube1 { get; set; }
        public string Metadata_Tube2 { get; set; }
        public string Metadata_Tube3 { get; set; }
        public float RunTime_Minutes { get; set; }
        public float RunTime_Minutes_Shortened { get; set; }
        public int RecentBufferSize { get; set; }
        public bool DisplayLogarithmic { get; set; }
        public bool SendNotification { get; set; }
        public float TemperatureCal_High { get; set; }
        public float TemperatureCal_Low { get; set; }
        public float Temperature_Ideal { get; set; }
        public float Temperature_Tolerance { get; set; }
        public Int32 Timing_Cycle { get; set; }
        public Int16 Timing_Light { get; set; }
        public Int16 Timing_Delay { get; set; }
        public Int16 Timing_Sensor { get; set; }
        public Int16 Timing_Send { get; set; }
        public Int16 Timing_Delta { get; set; }
        public bool AllowLights { get; set; }
        public bool AllowSound { get; set; }
        public int SplitterDistance { get; set; }
        public int Form_Height { get; set; }
        public int SchemaIndex { get; set; }
        public ChannelBufferParams BasicRunParams { get; set; }
        public ChannelBufferParams LongRunParams { get; set; }
        public ChannelBufferParams ShortenedLongRunParams { get; set; }

        public StoreClass()
        {
            Init("C:\\Temp\\Record\\", true, "COM5", "", "");
        }

        public StoreClass(string SaveFldr, bool SaveChData, string COMPort, string SubjectID, string ScanNote)
        {
            Init(SaveFldr, SaveChData, COMPort, SubjectID, ScanNote);
        }

        private void Init(string SaveFldr, bool SaveChData, string COMPort, string SubjectID, string ScanNote)
        {
            this.BaseFolder = SaveFldr;
            this.SaveChannelData = SaveChData;
            this.COMPort = COMPort;
            this.User = SubjectID;
            this.Subject = ScanNote;
            RecentBufferSize = 100;
            SplitterDistance = 350;
            TemperatureCal_High = 62;
            TemperatureCal_Low = 35;
            Temperature_Ideal = 62;
            Temperature_Tolerance = 0.8f;
            AllowLights = false;
            AllowSound = true;
            SendNotification = true;
            Form_Height = 640;
            Timing_Send = 250;
            RunTime_Minutes = 30f;
            RunTime_Minutes_Shortened = 25f;
            SchemaIndex = 0;
            Metadata_Tube1 = "";
            Metadata_Tube2 = "-";
            Metadata_Tube3 = "";
            InitializeDependents();
        }
        /// <summary>
        /// Initializes settings that are dependent on others
        /// This is needed as a separate function so that they can be reinitialized when the settings that they are dependent on change
        /// </summary>
        public void InitializeDependents()
        {
            BasicRunParams = new ChannelBufferParams
            {
                SecondsToDisplay = 20,
                UseMinutes_ForDisplay = false,
                MSEC_Timing = Timing_Send,
                DoReset = true,
                Zoom = 2,
                SecondsToExclude = 0
            };
            float SecondsRun = (60 * RunTime_Minutes);
            LongRunParams = new ChannelBufferParams
            {
                SecondsToDisplay = (int)SecondsRun,
                UseMinutes_ForDisplay = true,
                MSEC_Timing = Timing_Send,
                DoReset = true,
                Zoom = 1,
                SecondsToExclude = SecondsRun / 15
            };
            SecondsRun = (60 * RunTime_Minutes_Shortened);
            ShortenedLongRunParams = new ChannelBufferParams
            {
                SecondsToDisplay = (int)SecondsRun,
                UseMinutes_ForDisplay = true,
                MSEC_Timing = Timing_Send,
                DoReset = true,
                Zoom = 1,
                SecondsToExclude = SecondsRun / 15
            };
        }
        public bool Save()
        {
            Directory.CreateDirectory(DefaultFolder);
            return Save(StoreClass.DefaultPath);
        }

        public bool Save(string Path)
        {
            try
            {
                XmlSerializer XMLSer = new XmlSerializer(typeof(StoreClass));
                StreamWriter SW = new StreamWriter(Path);
                XMLSer.Serialize(SW, this);
                SW.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static string DefaultFolder
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "RGD");
            }
        }
        public static string DefaultPath
        {
            get
            {
                return Path.Combine(DefaultFolder, "RGD_Params.xml");
            }
        }
        public static string OldPath
        {
            get
            {
                return Path.Combine(Path.GetTempPath(), "RGD_Params.xml");
            }
        }
        public static bool Load(out StoreClass SV)
        {
            if (!File.Exists(DefaultPath) && File.Exists(OldPath)) return Load(OldPath, out SV); // This is for backwards compatibility
            return Load(DefaultPath, out SV);
        }

        public static bool Load(string FolderPath, out StoreClass SV)
        {
            try
            {
                XmlSerializer XMLSer = new XmlSerializer(typeof(StoreClass));
                StreamReader SR = new StreamReader(FolderPath);
                SV = (StoreClass)XMLSer.Deserialize(SR);
                SR.Close();
            }
            catch
            {
                //Probably didn't exist, so just make a new one
                SV = new StoreClass();
            }
            bool GoodPath = TestStoreLocation(SV);
            if (!GoodPath)
            {
                SV.BaseFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "RGD");
                Directory.CreateDirectory(SV.BaseFolder);
            }
            return GoodPath;
        }
        public static bool TestStoreLocation(StoreClass SV)
        {
            try
            {
                string folder = SV.BaseFolder;
                string testdoc = Path.Combine(folder, "_test");
                File.WriteAllText(testdoc, "    ");
                File.Delete(testdoc);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
    public class ChannelBufferParams
    {
        public int SecondsToDisplay;
        public int MSEC_Timing;
        public bool UseMinutes_ForDisplay;
        public bool DoReset;
        public double Zoom;
        public float SecondsToExclude;
    }
}
