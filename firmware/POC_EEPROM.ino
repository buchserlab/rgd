#define CURRENT_EEPROM_VERSON 5
#define EEPROM_USED 73

void Load_Settings() {
  if (EEPROM.read(1) == EEPROM_USED) {
    byte 
      EP_Version = EEPROM.read(2);
    deviceID     = E_Read_Long(10);
    t.calSlope  = E_Read_Float(20);
    t.calOffset = E_Read_Float(30);

    //Older versions don't have these values set, so just ignore the EEPROM and get it directly from the defaults
    if (EEPROM.read(40) != 255) {
      tubes[1].Ch[0].Sensor_Pin = EEPROM.read(40);
      tubes[2].Ch[0].Sensor_Pin = EEPROM.read(41);
      tubes[3].Ch[0].Sensor_Pin = EEPROM.read(42);
      //tubes[1].Ch[1].Sensor_Pin = EEPROM.read(43);
      //tubes[2].Ch[1].Sensor_Pin = EEPROM.read(44);
      //tubes[3].Ch[1].Sensor_Pin = EEPROM.read(45);
      
      tubes[1].Ch[0].LED_Pin = EEPROM.read(50);
      tubes[2].Ch[0].LED_Pin = EEPROM.read(51);
      tubes[3].Ch[0].LED_Pin = EEPROM.read(52);
      //tubes[1].Ch[1].LED_Pin = EEPROM.read(53);
      //tubes[2].Ch[1].LED_Pin = EEPROM.read(54);
      //tubes[3].Ch[1].LED_Pin = EEPROM.read(55);
     
      t.pinThermo  = EEPROM.read(60);
      t.pinHeater  = EEPROM.read(61);
      t.pinThermo2 = EEPROM.read(63);
      
      stat.pinBuzzer = EEPROM.read(70);
      stat.pinR      = EEPROM.read(71);
      stat.pinG      = EEPROM.read(72);
      stat.pinB      = EEPROM.read(73);

      t.temp2Raw_OptimalValue = E_Read_Float(64);
      t.temp2Raw_OptimalRange = E_Read_Float(74);
    }
  } else {
    Write_Default_Settings();
  }
}

void Write_Default_Settings() {
  deviceID = 999;
  t.calSlope = 0.116;
  t.calOffset= -32.21;
  Write_Settings();
}

void Write_Settings() {
  EEPROM.write(1, EEPROM_USED); //Whether it was set
  EEPROM.write(2, CURRENT_EEPROM_VERSON); //Whether it was set
  E_Write_Long (10, deviceID);
  E_Write_Float(20, t.calSlope);
  E_Write_Float(30, t.calOffset);
}

void Write_Settings_Ext1() {
  EEPROM.write(40, tubes[1].Ch[0].Sensor_Pin);
  EEPROM.write(41, tubes[2].Ch[0].Sensor_Pin);
  EEPROM.write(42, tubes[3].Ch[0].Sensor_Pin);
  
  EEPROM.write(50, tubes[1].Ch[0].LED_Pin);
  EEPROM.write(51, tubes[2].Ch[0].LED_Pin);
  EEPROM.write(52, tubes[3].Ch[0].LED_Pin);

  EEPROM.write(60, t.pinThermo);
  EEPROM.write(61, t.pinHeater);
  EEPROM.write(63, t.pinThermo2);

  EEPROM.write(70, stat.pinBuzzer);
  EEPROM.write(71, stat.pinR);
  EEPROM.write(72, stat.pinG);
  EEPROM.write(73, stat.pinB);
  
  E_Write_Float(64, t.temp2Raw_OptimalValue);
  E_Write_Float(74, t.temp2Raw_OptimalRange);
}

void Check_EEPROM_Positions() {
  for (byte i = 0; i < 40; i++) {
    Serial.print(i); Serial.print(delim);
    Serial.println(EEPROM.read(i));
  }
}

//-------------------------------------------------------------------------------

void Instruct_EEPROM(byte instructs[]) {
  //0 is start code
  deviceID    = B_Read_Long(instructs, 1);
  t.calSlope  = B_Read_Float(instructs, 5);
  t.calOffset = B_Read_Float(instructs, 9);
  Write_Settings();
}

void Instruct_EEPROM_Ext1(byte instructs[]) {
  //0 is start code
  //0  (Green)T1  T2  T3  (Red)T1 T2 T3  (blueLED) T1 T2 T3 (greenLED) T1 T2 T3 (Heat) therm therm2 heater 
  tubes[1].Ch[0].Sensor_Pin = instructs[1];
  tubes[2].Ch[0].Sensor_Pin = instructs[2];
  tubes[3].Ch[0].Sensor_Pin = instructs[3];
  
  tubes[1].Ch[0].LED_Pin = instructs[7];
  tubes[2].Ch[0].LED_Pin = instructs[8];
  tubes[3].Ch[0].LED_Pin = instructs[9];

  t.pinThermo = instructs[13];
  t.pinThermo2 = instructs[14];
  t.pinHeater = instructs[15];
  
  Write_Settings_Ext1(); //Write Settings Special

  Setup_Correct_PinMode(); //Make sure the pins are correctly enabled
}

void Instruct_EEPROM_Ext2(byte instructs[]) {
  //0 is start code
  stat.pinBuzzer = instructs[1];
  stat.pinR = instructs[2];
  stat.pinG = instructs[3];
  stat.pinB = instructs[4];

  t.temp2Raw_OptimalValue = B_Read_Float(instructs, 5);
  t.temp2Raw_OptimalRange = B_Read_Float(instructs, 9);

  Write_Settings_Ext1(); //Write Settings Special

  Setup_Correct_PinMode(); //Make sure the pins are correctly enabled
}
