//- - - CONVERT FLOAT - - - - - - - - - - - - - - - - - - - - - - - - - - 

union u_convert {
  byte b[4];
  float fval;
};

float E_Read_Float(short StartIdx) {
  u_convert u;
  for (byte i = 0; i < 4; i++) u.b[i] = EEPROM.read(StartIdx + i);
  return u.fval;
}

void E_Write_Float(short StartIdx, float writeVal) {
  u_convert u;
  u.fval = writeVal;
  for (byte i = 0; i < 4; i++) EEPROM.write(StartIdx + i, u.b[i]);
}

//- - - CONVERT LONG

union u_convertLong {
  byte b[4];
  long fval;
};

long E_Read_Long(short StartIdx) {
  u_convertLong u;
  for (byte i = 0; i < 4; i++) u.b[i] = EEPROM.read(StartIdx + i);
  return u.fval;
}

void E_Write_Long(short StartIdx, long writeVal) {
  u_convertLong u;
  u.fval = writeVal;
  for (byte i = 0; i < 4; i++) EEPROM.write(StartIdx + i, u.b[i]);
}

// - - - For the Instruction Part

float B_Read_Float(byte Arr[], byte Start) {
  u_convert u;
  for (byte i = 0; i < 4; i++) u.b[i] = Arr[Start + i];
  return u.fval;
}

long B_Read_Long(byte Arr[], byte Start) {
  u_convertLong u;
  for (byte i = 0; i < 4; i++) u.b[i] = Arr[Start + i];
  return u.fval;
}

union u_convertInt {
  byte b[2];
  long fval;
};

int B_Read_Int(byte Arr[], byte Start) {
  u_convertInt u;
  for (byte i = 0; i < 2; i++) u.b[i] = Arr[Start + i];
  return u.fval;
}
