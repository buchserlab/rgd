// - - - - - - - - Temperature/Heater Code - - - - - - - - - - - - - - - - -

void GetTemp() {
  t.tempRaw_Count++;
  t.tempRaw_Sum += analogRead(t.pinThermo);
  t.tempRaw_Avg = t.tempRaw_Sum / t.tempRaw_Count;
  t.thermoSensorCal = t.calSlope * t.tempRaw_Avg + t.calOffset;

  t.temp2Raw_Sum += analogRead(t.pinThermo2);
  t.temp2Raw_Avg = t.temp2Raw_Sum / t.tempRaw_Count;
}

void Temp_ResetAvg() {
  t.tempRaw_Count = 0;
  t.tempRaw_Sum = 0;
  t.temp2Raw_Sum = 0;
}

void AdjustTemp() {
  float tempC = t.thermoSensorCal;
  if (tempC <= (t.temp_Ideal - (t.temp_Tolerance / 2))) {
    HeatingElement(true);
  }
  if (tempC >= (t.temp_Ideal + (t.temp_Tolerance / 2))) {
    HeatingElement(false);
  }
}

void HeatingElement(bool ON) {
  if (ON) {
    digitalWrite(t.pinHeater, HIGH);
    hStatus = 1;
  } else {
    digitalWrite(t.pinHeater, LOW);
    hStatus = 0;
  }
}

void Check_Temperature() {
  GetTemp();
  if (allowHeater) {
    AdjustTemp();
    //Check any reasons to turn the heater off
    if ((millis()-t.Millis_HeaterOn) > t.Millis_Before_HeaterOff) {  //After 12 hours (as specified in the Millis_Before_HeaterOff
      SwitchHeater(false);
    }
    //Check if the in-holder tempeture has gotten above any thresholds - - - - - - - - - - 
    // 7/29/2020 Turned this off temporarily
    //if (t.temp2Raw_Avg > (t.temp2Raw_OptimalValue+(t.temp2Raw_OptimalRange*3))) {
    //  SwitchHeater(false);
    //}
  }
}

void SwitchHeater(bool On) {
  t.Millis_HeaterOn = millis();
  allowHeater = On;
  if (!On) HeatingElement(false);
}

void Instruct_Heat(byte instructs[]) {
  //Turn on or off the Heater
  if (instructs[1] == 1) {
    t.temp_Ideal = B_Read_Float(instructs, 2);
    t.temp_Tolerance = B_Read_Float(instructs, 6);
    SwitchHeater(true);
  } else {
    //Ignore the other instructions
    SwitchHeater(false);
  }
}
