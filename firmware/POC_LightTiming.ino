// - - - - - - - - Optical Sensor Code - - - - - - - - - - - - - - - - -

void Check_Optical() {
  if (t_i % cy.light < cy.t_delta) {
    //Turn off the Last LED in the beginning of each light cycle
    for (byte ch = 0; ch < chCount; ch++) {
      SwitchLED(lastOn, ch, !LED_OnStatus);
    }
  }
  for (byte tube = 1; tube <= tubeCount; tube++) {
    if (tubes[tube].active) {
      for (byte ch = 0; ch < chCount; ch++) {
        if (((t_i % cy.cycle) >= (tube * cy.light + 0)) && ((t_i % cy.cycle) <  (tube * cy.light + cy.t_delta))) {
          //If the Overall cycle is within its first hit of the cy.light, then turn the light on
          if (allowLights)
            SwitchLED(tube, ch, LED_OnStatus);
          lastOn = tube;
          Reset_Optical(tube, ch);
        }
        if (((t_i % cy.cycle) >= (tube * cy.light + cy.sdelay)) && ((t_i % cy.cycle) <  ((tube + 1) * cy.light ))) {
          //Make the reading(s) at all the times past the cy.delay (average these)
          if (t_i % cy.sensor < cy.t_delta) {
            Read_Optical(tube, ch);
          }
        }
      }
    }
  }
}

void Read_Optical(byte tubeIdx, byte chIdx) {
  tubes[tubeIdx].Ch[chIdx].countVal ++;
  tubes[tubeIdx].Ch[chIdx].sumVal += analogRead(tubes[tubeIdx].Ch[chIdx].Sensor_Pin);
  tubes[tubeIdx].Ch[chIdx].avgVal = tubes[tubeIdx].Ch[chIdx].sumVal / tubes[tubeIdx].Ch[chIdx].countVal;
}

void Reset_Optical(byte tubeIdx, byte chIdx) {
  tubes[tubeIdx].Ch[chIdx].sumVal = 0;
  tubes[tubeIdx].Ch[chIdx].countVal = 0;
}

void SwitchLED(byte tubeIdx, byte chIdx, byte Status) {
  if (tubeIdx < 0) return;
  digitalWrite(tubes[tubeIdx].Ch[chIdx].LED_Pin, Status);
}


void Instruct_Timing(byte instructs[]) {
  //0 is start code
  cy.cycle    = B_Read_Long(instructs, 1);
  cy.light    = B_Read_Int(instructs, 5);
  cy.sdelay   = B_Read_Int(instructs, 7);
  cy.sensor   = B_Read_Int(instructs, 9);
  cy.dsend    = B_Read_Int(instructs, 11);
  cy.t_delta  = B_Read_Int(instructs, 13);
  if (instructs[15]==1) allowLights = true; else allowLights = false;
}
