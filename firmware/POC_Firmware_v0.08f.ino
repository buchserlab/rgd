#include <EEPROM.h>
#include "POC_Defines.h"
#define FIRMWARE_VERSION 806
#define PACKETBYTETOTAL 16

//TODO: Heating should have to be gotten a signal every so often, otherwise will turn off
//If TempV goes above the OptimalValue + Range * 2, then shut off the heating . . 


long deviceID;
Temp_Params t;
Tube_Params tubes[4];
Timing_Params cy;
Status_Params stat;
byte tubeCount;
byte chCount;

byte LED_OnStatus;
bool allowHeater;
bool allowLights;
bool allowStatusLights;
byte hStatus;

long t_i;
long t_pe;
byte lastOn = -1;
char delim = ' ';
int PacketWrongCount = 0;

void Setup_Params() {
  // - - - - - - - - General - - - -
  LED_OnStatus = LOW;
  allowHeater = false;
  allowLights = true;
  allowStatusLights = true;

  // - - - - - - - - Temp Setup - - -
  t.temp_Ideal = 60;
  t.temp_Tolerance = 0.3;
  t.pinThermo = A3;
  t.pinThermo2 = A2;
  t.pinHeater = 12;
  t.Millis_Before_HeaterOff = 43200000; //12 hours (10*60*60*1000)
  t.Millis_HeaterOn = 0; //Set when heater is turned on
  Temp_ResetAvg();

  // - - - - - - - - Status Setup - - - - -
  stat.pinBuzzer = 5;
  stat.pinR = 7;
  stat.pinG = 8;
  stat.pinB = 9;
  stat.modOn = 1000;
  stat.fractionOn = 0.5;
  stat.lengthCounter = 0;

  // - - - - - - - - Timing Setup - - -
  cy.cycle   = 2500;  // The total cycle time (must be 4x longer than cy.light at least)
  cy.light   = 500;   // The time that the light is on
  cy.sdelay  = 300;   // How long to wait before starting to get sensor readings
  cy.sensor  = 100;   // How frequently to get sensor readings
  cy.dsend   = 250;   // Frequency to send serial output (temp and sensor data)
  cy.t_delta = 50;    // Delay interval, everything happens as slow as this (50 ms ideal?)

  // - - - - - - - - Tube Setup - - -
  tubeCount = 3; chCount = 1;

  //Tube 1 (left side, opposite battery)
  tubes[1].active = true;
  tubes[1].Ch[0].Sensor_Pin = A1;
  tubes[1].Ch[0].LED_Pin = 2;

  //Tube 2 (middle, inactive so thermister probe can be used)
  tubes[2].active = true;
  tubes[2].Ch[0].Sensor_Pin = A4;
  tubes[2].Ch[0].LED_Pin = 2;

  //Tube 3 (right or battery side)
  tubes[3].active = true;
  tubes[3].Ch[0].Sensor_Pin = A6;
  tubes[3].Ch[0].LED_Pin = 3;
}

void Setup_Correct_PinMode() {
  pinMode(t.pinHeater, OUTPUT);

  for (byte tube = 1; tube <= tubeCount; tube++) {
    if (tubes[tube].active) {
      for (byte ch = 0; ch < chCount; ch++) {
        pinMode(tubes[tube].Ch[ch].LED_Pin, OUTPUT);
      }
    }
  }

  pinMode(stat.pinBuzzer, OUTPUT);
  pinMode(stat.pinR, OUTPUT);
  pinMode(stat.pinG, OUTPUT);
  pinMode(stat.pinB, OUTPUT);
}

void Send_InitialInfo() {
  Serial.print(deviceID); Serial.print(delim);         //0
  Serial.print(100*t.calSlope); Serial.print(delim);       //1 (the multiply increases the decimal places)
  Serial.print(t.calOffset); Serial.print(delim);      //2
  Serial.print(FIRMWARE_VERSION); Serial.print(delim); //3

  Serial.print(tubes[1].Ch[0].Sensor_Pin); Serial.print(delim); //4
  Serial.print(tubes[2].Ch[0].Sensor_Pin); Serial.print(delim); //5
  Serial.print(tubes[3].Ch[0].Sensor_Pin); Serial.print(delim); //6

  Serial.print(tubes[1].Ch[0].LED_Pin); Serial.print(delim); //7
  Serial.print(tubes[2].Ch[0].LED_Pin); Serial.print(delim); //8
  Serial.print(tubes[3].Ch[0].LED_Pin); Serial.print(delim); //9

  Serial.print(t.pinThermo); Serial.print(delim); //10
  Serial.print(t.pinHeater); Serial.print(delim); //11

  Serial.print(t.pinThermo2); Serial.print(delim); //12
  Serial.print(t.temp2Raw_OptimalValue); Serial.print(delim); //13
  Serial.print(t.temp2Raw_OptimalRange); Serial.print(delim); //14

  Serial.print(stat.pinBuzzer); Serial.print(delim); //15
  Serial.print(stat.pinR); Serial.print(delim); //16
  Serial.print(stat.pinG); Serial.print(delim); //17
  Serial.print(stat.pinB); Serial.print(delim); //18

  Serial.println();
}

void setup() {
  //https://learn.adafruit.com/memories-of-an-arduino/optimizing-sram
  //https://forum.arduino.cc/index.php?topic=45267.0
  Serial.begin(9600);

  Setup_Params();
  Load_Settings(); //Some of the params may get over-written
  Setup_Correct_PinMode();

  delay(10);
  Send_InitialInfo();
  //Check_EEPROM_Positions();
}

//--- Send / Receive ------------------------------------------------------------------------------------------

void Send_Data() {
  //In the new version I am avoiding all string concatenation . .
  if (t_i % cy.dsend < cy.t_delta) {
    for (short i = 1; i <= tubeCount; i++) {
      Serial.print(tubes[i].Ch[0].avgVal); Serial.print(delim);
    }
    Serial.print(t.thermoSensorCal); Serial.print(delim);
    Serial.print(t.tempRaw_Avg); Serial.print(delim);
    Serial.print(t.temp2Raw_Avg); Serial.print(delim);
    Serial.println(hStatus);

    Temp_ResetAvg();
  }
}

void Check_Instruct() {
  int avail = Serial.available();
  if (avail > 0) {
    if (avail == PACKETBYTETOTAL) {
      PacketWrongCount = 0;
      t_pe = -1;
      byte Arr[PACKETBYTETOTAL];
      for (byte i = 0; i < PACKETBYTETOTAL; i++) {
        Arr[i] = Serial.read();
      }
      switch (Arr[0]) {
        case 27 : //StartCode_Heat
          Instruct_Heat(Arr);
          break;
        case 45 : //StartCode_Timing
          Instruct_Timing(Arr);
          break;
        case 73 : //StartCode_EEPROM
          Instruct_EEPROM(Arr);
          break;
        case 79 : //StartCode_EEPROM_Ext1
          Instruct_EEPROM_Ext1(Arr);
          break;
        case 83 : //StartCode_EEPROM_Ext2
          Instruct_EEPROM_Ext2(Arr);
          break;
        case 91 : //StartCode_PreQuery
          //Just re-send the inital info
          Send_InitialInfo(); delay(20);
          Send_InitialInfo();
          break;
        case 107 : //StartCode_StatusBuzzer
          Instruct_Status_Buzzer(Arr);
          break;
        case 123 : //StartCode_StatusLights
          Instruct_Status_Lights(Arr);
          break;
      }
    } else { //Wrong # of Bytes received
      //Serial.println(F("BOO "));
      PacketWrongCount++;
      if (t_pe == -1) t_pe = millis(); //Keep track of how long we got the wrong thing
      if ((millis() - t_pe) > 2000) {
        ClearSerial(); return;
      }
      if (PacketWrongCount > 1) { //Wait one cycle to check
        ClearSerial(); return;
      }
    }
  }
}

void ClearSerial() {
  while (Serial.available()) Serial.read();
  t_pe = -1;
  PacketWrongCount = 0;
  //Serial.println(F("CLEAR"));
}

//---- LOOP  -----------------------------------------------------------------------------------------

void loop() {
  t_i = millis();
  Check_Instruct();
  Check_Optical();
  Check_Temperature();
  Status_Lights_Check();
  Send_Data();
  delay(cy.t_delta);
}
