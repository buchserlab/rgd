//-------------  Status Instructs -------------------------

void Instruct_Status_Buzzer(byte Arr[]) {
  //0 is start code
  // A = msec delay in cycle, B = length in msec of that cycle * 32 maybe
  
  for (byte t = 1; t <= PACKETBYTETOTAL; t+=3) {
    if (Arr[t+1]==0) break;
    PlaySound(Arr[t],Arr[t+1],Arr[t+2]);
  }
}

void PlaySound(byte sFreq, byte sLength, byte qLength) {
  tone(stat.pinBuzzer, 200+(sFreq*4));
  delay(sLength*20);
  noTone(stat.pinBuzzer);
  delay(qLength*20);
}

void Instruct_Status_Lights(byte Arr[]) {
  //0 is start Code
  stat.sR = Arr[1];      //Red
  stat.sG = Arr[2];      //Green
  stat.sB = Arr[3];      //Blue
  stat.modOn = (Arr[4]*20);        //which modulus switches on in milliseconds - Max 5 seconds, min 20 msec
  stat.fractionOn = ((float)Arr[5]/256);   //what fraction of the same modulus do the lights stay on for (so 0.5 means on for half and off for half . . 0.9 means on 90% duty cycle)
  //Not implemented yet
  stat.lengthCounter = Arr[6]; //means indefinite - Max 1.5 minutes, min 1/3 of second
  stat.Counter = 0;
}

void Status_Lights_Check() {
  if (allowStatusLights) {
    if (t_i % stat.modOn <= cy.t_delta) {
      //Turn on the lights
      digitalWrite(stat.pinR,stat.sR);
      digitalWrite(stat.pinG,stat.sG);
      digitalWrite(stat.pinB,stat.sB);
      
      //Check the counter
      if (stat.lengthCounter>0) stat.Counter++;
      if (stat.Counter>stat.lengthCounter) { stat.sR = stat.sG = stat.sB = 0; }
    }
    if (t_i % stat.modOn >= (stat.fractionOn * stat.modOn)) {
      //Blink off the lights
      digitalWrite(stat.pinR,0);
      digitalWrite(stat.pinG,0);
      digitalWrite(stat.pinB,0);
    }
  }
}
