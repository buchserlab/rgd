struct Temp_Params {
  float temp_Ideal;
  float temp_Tolerance;
  float calSlope;
  float calOffset;
  float thermoSensorCal;
  byte pinHeater;
  
  byte pinThermo; //In-Tube Thermister
  float tempRaw_Sum;
  float tempRaw_Avg;
  float tempRaw_Count;
  
  byte pinThermo2; //In-Holder Thermister
  float temp2Raw_Sum;
  float temp2Raw_Avg;
  float temp2Raw_OptimalValue; //This is set by the software and used to monitor that the system is working properly
  float temp2Raw_OptimalRange;

  unsigned long Millis_Before_HeaterOff;
  unsigned long Millis_HeaterOn;
};

struct Channel_Params {
  short Sensor_Pin;
  short LED_Pin;

  float avgVal;
  float sumVal;
  short countVal;
};

struct Tube_Params {
  struct Channel_Params Ch[1];
  bool active;
};

struct Timing_Params {   // - - All in MSEC
  long cycle;      // The total cycle time (must be 4x longer than cy.light at least)
  int  light;      // The time that the light is on
  int  sdelay;     // How long to wait before starting to get sensor readings
  int  sensor;     // How frequently to get sensor readings
  int  dsend;      // How frequently to send the data
  int  t_delta;    // Delay interval, everything happens as slow as this (50 ms ideal?)
};

struct Status_Params {
  int pinBuzzer;
  int pinR;
  int pinG;
  int pinB;
  
  //variables that control the lighting
  byte sR, sG, sB;
  int modOn;
  float fractionOn;
  int lengthCounter;

  //counter
  int Counter;
};
