﻿using System.Drawing;

namespace RGD_Control
{
    partial class RGD_FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RGD_FormMain));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lbl_Instructions = new System.Windows.Forms.Label();
            this.panel_RunSchema = new RGD_Control.RunSchemaPanel();
            this.label14 = new System.Windows.Forms.Label();
            this.label_Running = new System.Windows.Forms.Label();
            this.btn_PreHeat = new System.Windows.Forms.Button();
            this.btn_Run_30min = new System.Windows.Forms.Button();
            this.User = new System.Windows.Forms.TextBox();
            this.txBx_Update = new System.Windows.Forms.TextBox();
            this.btn_Stop = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.Subject = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Metadata_Tube2 = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txt_AddInfoTube3 = new System.Windows.Forms.TextBox();
            this.txt_AddInfoTube1 = new System.Windows.Forms.TextBox();
            this.SendNotification = new System.Windows.Forms.CheckBox();
            this.DisplayLogarithmic = new System.Windows.Forms.CheckBox();
            this.btn_Diagnostics = new System.Windows.Forms.Button();
            this.btn_Clear = new System.Windows.Forms.Button();
            this.btn_Connect = new System.Windows.Forms.Button();
            this.panel_TempHeating = new System.Windows.Forms.Panel();
            this.chkBox_Heater = new System.Windows.Forms.CheckBox();
            this.Temperature_Tolerance = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.Temperature_Ideal = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label_T_Instruct = new System.Windows.Forms.Label();
            this.TemperatureCal_Low = new System.Windows.Forms.TextBox();
            this.TemperatureCal_High = new System.Windows.Forms.TextBox();
            this.txBx_TemperatureCurrent = new System.Windows.Forms.TextBox();
            this.btn_TCal_Hi = new System.Windows.Forms.Button();
            this.btn_TCal_Low = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_SvImage = new System.Windows.Forms.Label();
            this.lbl_SelectDevice = new System.Windows.Forms.Label();
            this.btn_Query = new System.Windows.Forms.Button();
            this.comboBox_Devices = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btn_LoadSchema = new System.Windows.Forms.Button();
            this.btn_OpenAppData = new System.Windows.Forms.Button();
            this.BaseFolder = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.panel_Params = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.RunTime_Minutes = new System.Windows.Forms.TextBox();
            this.label_More_EEPROM = new System.Windows.Forms.Label();
            this.lbl_Timing_Quick = new System.Windows.Forms.Label();
            this.Timing_Defaults = new System.Windows.Forms.Label();
            this.AllowLights = new System.Windows.Forms.CheckBox();
            this.Timing_Delta = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.Timing_Send = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btn_Timing = new System.Windows.Forms.Button();
            this.Timing_Sensor = new System.Windows.Forms.TextBox();
            this.txBx_calSlope = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txBx_DeviceID = new System.Windows.Forms.TextBox();
            this.txBx_Firmware = new System.Windows.Forms.TextBox();
            this.txBx_CalIntercept = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.btn_SendEEPROM = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Timing_Delay = new System.Windows.Forms.TextBox();
            this.Timing_Light = new System.Windows.Forms.TextBox();
            this.Timing_Cycle = new System.Windows.Forms.TextBox();
            this.btn_SaveSettings = new System.Windows.Forms.Button();
            this.lbl_AdvancedEasy = new System.Windows.Forms.Label();
            this.timer_Update = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel_TempHeating.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel_Params.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Panel2MinSize = 300;
            this.splitContainer1.Size = new System.Drawing.Size(879, 702);
            this.splitContainer1.SplitterDistance = 350;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.Text = "splitContainer1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(873, 324);
            this.tabControl1.TabIndex = 48;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lbl_Instructions);
            this.tabPage1.Controls.Add(this.panel_RunSchema);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label_Running);
            this.tabPage1.Controls.Add(this.btn_PreHeat);
            this.tabPage1.Controls.Add(this.btn_Run_30min);
            this.tabPage1.Controls.Add(this.User);
            this.tabPage1.Controls.Add(this.txBx_Update);
            this.tabPage1.Controls.Add(this.btn_Stop);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.Subject);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.Metadata_Tube2);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(865, 296);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Basic Run";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lbl_Instructions
            // 
            this.lbl_Instructions.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbl_Instructions.AutoSize = true;
            this.lbl_Instructions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbl_Instructions.Font = new System.Drawing.Font("Segoe UI", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_Instructions.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lbl_Instructions.Location = new System.Drawing.Point(88, 13);
            this.lbl_Instructions.Name = "lbl_Instructions";
            this.lbl_Instructions.Padding = new System.Windows.Forms.Padding(3);
            this.lbl_Instructions.Size = new System.Drawing.Size(475, 39);
            this.lbl_Instructions.TabIndex = 52;
            this.lbl_Instructions.Text = "Place a red + in slot 1 and a blue \"-\" in slot 3";
            // 
            // panel_RunSchema
            // 
            this.panel_RunSchema.Location = new System.Drawing.Point(5, 4);
            this.panel_RunSchema.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_RunSchema.Name = "panel_RunSchema";
            this.panel_RunSchema.Size = new System.Drawing.Size(404, 266);
            this.panel_RunSchema.TabIndex = 50;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label14.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label14.Location = new System.Drawing.Point(550, 186);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 48;
            this.label14.Text = "Tube 2";
            this.label14.Visible = false;
            // 
            // label_Running
            // 
            this.label_Running.AutoSize = true;
            this.label_Running.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label_Running.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label_Running.Location = new System.Drawing.Point(672, 33);
            this.label_Running.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Running.Name = "label_Running";
            this.label_Running.Size = new System.Drawing.Size(99, 30);
            this.label_Running.TabIndex = 40;
            this.label_Running.Text = "Running";
            this.label_Running.Visible = false;
            // 
            // btn_PreHeat
            // 
            this.btn_PreHeat.Location = new System.Drawing.Point(615, 85);
            this.btn_PreHeat.Margin = new System.Windows.Forms.Padding(2);
            this.btn_PreHeat.Name = "btn_PreHeat";
            this.btn_PreHeat.Size = new System.Drawing.Size(77, 60);
            this.btn_PreHeat.TabIndex = 42;
            this.btn_PreHeat.Text = "Pre\r\nHeat";
            this.btn_PreHeat.UseVisualStyleBackColor = true;
            this.btn_PreHeat.Click += new System.EventHandler(this.btn_PreHeat_Click);
            // 
            // btn_Run_30min
            // 
            this.btn_Run_30min.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btn_Run_30min.Location = new System.Drawing.Point(696, 85);
            this.btn_Run_30min.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Run_30min.Name = "btn_Run_30min";
            this.btn_Run_30min.Size = new System.Drawing.Size(75, 60);
            this.btn_Run_30min.TabIndex = 34;
            this.btn_Run_30min.Text = "Run";
            this.btn_Run_30min.UseVisualStyleBackColor = true;
            this.btn_Run_30min.Click += new System.EventHandler(this.Run_Long_Start);
            // 
            // User
            // 
            this.User.Location = new System.Drawing.Point(423, 93);
            this.User.Margin = new System.Windows.Forms.Padding(2);
            this.User.Name = "User";
            this.User.Size = new System.Drawing.Size(152, 23);
            this.User.TabIndex = 1;
            this.toolTip1.SetToolTip(this.User, "Enter the information about the technician running this test (and other informati" +
        "on)");
            // 
            // txBx_Update
            // 
            this.txBx_Update.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txBx_Update.Location = new System.Drawing.Point(615, 156);
            this.txBx_Update.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_Update.Multiline = true;
            this.txBx_Update.Name = "txBx_Update";
            this.txBx_Update.ReadOnly = true;
            this.txBx_Update.Size = new System.Drawing.Size(236, 87);
            this.txBx_Update.TabIndex = 24;
            // 
            // btn_Stop
            // 
            this.btn_Stop.Location = new System.Drawing.Point(775, 85);
            this.btn_Stop.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Stop.Name = "btn_Stop";
            this.btn_Stop.Size = new System.Drawing.Size(76, 60);
            this.btn_Stop.TabIndex = 27;
            this.btn_Stop.Text = "Stop";
            this.btn_Stop.UseVisualStyleBackColor = true;
            this.btn_Stop.Click += new System.EventHandler(this.btn_Stop_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label11.Location = new System.Drawing.Point(449, 126);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 19);
            this.label11.TabIndex = 10;
            this.label11.Text = "Note (optional)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Subject
            // 
            this.Subject.Location = new System.Drawing.Point(423, 147);
            this.Subject.Margin = new System.Windows.Forms.Padding(2);
            this.Subject.Name = "Subject";
            this.Subject.Size = new System.Drawing.Size(152, 23);
            this.Subject.TabIndex = 2;
            this.toolTip1.SetToolTip(this.Subject, "Enter a de-identified subject identifier");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label10.Location = new System.Drawing.Point(482, 72);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 19);
            this.label10.TabIndex = 9;
            this.label10.Text = "User";
            // 
            // Metadata_Tube2
            // 
            this.Metadata_Tube2.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.Metadata_Tube2.Enabled = false;
            this.Metadata_Tube2.Location = new System.Drawing.Point(556, 208);
            this.Metadata_Tube2.Margin = new System.Windows.Forms.Padding(2);
            this.Metadata_Tube2.Name = "Metadata_Tube2";
            this.Metadata_Tube2.Size = new System.Drawing.Size(23, 23);
            this.Metadata_Tube2.TabIndex = 4;
            this.Metadata_Tube2.Text = "-";
            this.toolTip1.SetToolTip(this.Metadata_Tube2, "Tube 2 Contents");
            this.Metadata_Tube2.Visible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label22);
            this.tabPage2.Controls.Add(this.label21);
            this.tabPage2.Controls.Add(this.txt_AddInfoTube3);
            this.tabPage2.Controls.Add(this.txt_AddInfoTube1);
            this.tabPage2.Controls.Add(this.SendNotification);
            this.tabPage2.Controls.Add(this.DisplayLogarithmic);
            this.tabPage2.Controls.Add(this.btn_Diagnostics);
            this.tabPage2.Controls.Add(this.btn_Clear);
            this.tabPage2.Controls.Add(this.btn_Connect);
            this.tabPage2.Controls.Add(this.panel_TempHeating);
            this.tabPage2.Controls.Add(this.lbl_SvImage);
            this.tabPage2.Controls.Add(this.lbl_SelectDevice);
            this.tabPage2.Controls.Add(this.btn_Query);
            this.tabPage2.Controls.Add(this.comboBox_Devices);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(865, 296);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Advanced";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(608, 118);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(124, 15);
            this.label22.TabIndex = 43;
            this.label22.Text = "Tube 3 Additional Info";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(608, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(124, 15);
            this.label21.TabIndex = 42;
            this.label21.Text = "Tube 1 Additional Info";
            // 
            // txt_AddInfoTube3
            // 
            this.txt_AddInfoTube3.Location = new System.Drawing.Point(573, 137);
            this.txt_AddInfoTube3.Multiline = true;
            this.txt_AddInfoTube3.Name = "txt_AddInfoTube3";
            this.txt_AddInfoTube3.Size = new System.Drawing.Size(188, 72);
            this.txt_AddInfoTube3.TabIndex = 41;
            // 
            // txt_AddInfoTube1
            // 
            this.txt_AddInfoTube1.Location = new System.Drawing.Point(573, 37);
            this.txt_AddInfoTube1.Multiline = true;
            this.txt_AddInfoTube1.Name = "txt_AddInfoTube1";
            this.txt_AddInfoTube1.Size = new System.Drawing.Size(188, 72);
            this.txt_AddInfoTube1.TabIndex = 40;
            // 
            // SendNotification
            // 
            this.SendNotification.AutoSize = true;
            this.SendNotification.Location = new System.Drawing.Point(290, 205);
            this.SendNotification.Name = "SendNotification";
            this.SendNotification.Size = new System.Drawing.Size(146, 19);
            this.SendNotification.TabIndex = 0;
            this.SendNotification.Text = "Send Error Notification";
            this.SendNotification.UseVisualStyleBackColor = true;
            // 
            // DisplayLogarithmic
            // 
            this.DisplayLogarithmic.AutoSize = true;
            this.DisplayLogarithmic.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.DisplayLogarithmic.Location = new System.Drawing.Point(208, 206);
            this.DisplayLogarithmic.Margin = new System.Windows.Forms.Padding(2);
            this.DisplayLogarithmic.Name = "DisplayLogarithmic";
            this.DisplayLogarithmic.Size = new System.Drawing.Size(45, 17);
            this.DisplayLogarithmic.TabIndex = 14;
            this.DisplayLogarithmic.Text = "Log";
            this.DisplayLogarithmic.UseVisualStyleBackColor = true;
            this.DisplayLogarithmic.CheckedChanged += new System.EventHandler(this.DisplayLogarithmic_CheckedChanged);
            // 
            // btn_Diagnostics
            // 
            this.btn_Diagnostics.Location = new System.Drawing.Point(34, 34);
            this.btn_Diagnostics.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Diagnostics.Name = "btn_Diagnostics";
            this.btn_Diagnostics.Size = new System.Drawing.Size(131, 35);
            this.btn_Diagnostics.TabIndex = 36;
            this.btn_Diagnostics.Text = "PreCheck";
            this.toolTip1.SetToolTip(this.btn_Diagnostics, "Insert at least Empty tubes if in an enclosure");
            this.btn_Diagnostics.UseVisualStyleBackColor = true;
            this.btn_Diagnostics.Click += new System.EventHandler(this.btn_Diagnostics_Click);
            // 
            // btn_Clear
            // 
            this.btn_Clear.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_Clear.Location = new System.Drawing.Point(110, 84);
            this.btn_Clear.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Clear.Name = "btn_Clear";
            this.btn_Clear.Size = new System.Drawing.Size(68, 30);
            this.btn_Clear.TabIndex = 25;
            this.btn_Clear.Text = "Clear";
            this.btn_Clear.UseVisualStyleBackColor = true;
            this.btn_Clear.Click += new System.EventHandler(this.btn_Clear_Click);
            // 
            // btn_Connect
            // 
            this.btn_Connect.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_Connect.Location = new System.Drawing.Point(92, 125);
            this.btn_Connect.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Connect.Name = "btn_Connect";
            this.btn_Connect.Size = new System.Drawing.Size(85, 29);
            this.btn_Connect.TabIndex = 26;
            this.btn_Connect.Text = "Connect";
            this.btn_Connect.UseVisualStyleBackColor = true;
            this.btn_Connect.Click += new System.EventHandler(this.btn_Connect_Click);
            // 
            // panel_TempHeating
            // 
            this.panel_TempHeating.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_TempHeating.Controls.Add(this.chkBox_Heater);
            this.panel_TempHeating.Controls.Add(this.Temperature_Tolerance);
            this.panel_TempHeating.Controls.Add(this.label16);
            this.panel_TempHeating.Controls.Add(this.Temperature_Ideal);
            this.panel_TempHeating.Controls.Add(this.label15);
            this.panel_TempHeating.Controls.Add(this.label_T_Instruct);
            this.panel_TempHeating.Controls.Add(this.TemperatureCal_Low);
            this.panel_TempHeating.Controls.Add(this.TemperatureCal_High);
            this.panel_TempHeating.Controls.Add(this.txBx_TemperatureCurrent);
            this.panel_TempHeating.Controls.Add(this.btn_TCal_Hi);
            this.panel_TempHeating.Controls.Add(this.btn_TCal_Low);
            this.panel_TempHeating.Controls.Add(this.label1);
            this.panel_TempHeating.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.panel_TempHeating.Location = new System.Drawing.Point(208, 34);
            this.panel_TempHeating.Margin = new System.Windows.Forms.Padding(2);
            this.panel_TempHeating.Name = "panel_TempHeating";
            this.panel_TempHeating.Size = new System.Drawing.Size(227, 157);
            this.panel_TempHeating.TabIndex = 33;
            // 
            // chkBox_Heater
            // 
            this.chkBox_Heater.AutoSize = true;
            this.chkBox_Heater.Location = new System.Drawing.Point(6, 69);
            this.chkBox_Heater.Margin = new System.Windows.Forms.Padding(2);
            this.chkBox_Heater.Name = "chkBox_Heater";
            this.chkBox_Heater.Size = new System.Drawing.Size(60, 17);
            this.chkBox_Heater.TabIndex = 17;
            this.chkBox_Heater.Text = "Heater";
            this.chkBox_Heater.UseVisualStyleBackColor = true;
            this.chkBox_Heater.CheckedChanged += new System.EventHandler(this.chkBox_Heater_CheckedChanged);
            // 
            // Temperature_Tolerance
            // 
            this.Temperature_Tolerance.Location = new System.Drawing.Point(166, 120);
            this.Temperature_Tolerance.Margin = new System.Windows.Forms.Padding(2);
            this.Temperature_Tolerance.Name = "Temperature_Tolerance";
            this.Temperature_Tolerance.Size = new System.Drawing.Size(50, 22);
            this.Temperature_Tolerance.TabIndex = 3;
            this.Temperature_Tolerance.TextChanged += new System.EventHandler(this.Temperature_Tolerance_TextChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(116, 123);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(35, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Toler.";
            this.toolTip1.SetToolTip(this.label16, ".");
            // 
            // Temperature_Ideal
            // 
            this.Temperature_Ideal.Location = new System.Drawing.Point(56, 120);
            this.Temperature_Ideal.Margin = new System.Windows.Forms.Padding(2);
            this.Temperature_Ideal.Name = "Temperature_Ideal";
            this.Temperature_Ideal.Size = new System.Drawing.Size(52, 22);
            this.Temperature_Ideal.TabIndex = 3;
            this.Temperature_Ideal.TextChanged += new System.EventHandler(this.Temperature_Ideal_TextChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 123);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(35, 13);
            this.label15.TabIndex = 5;
            this.label15.Text = "Set T.";
            // 
            // label_T_Instruct
            // 
            this.label_T_Instruct.AutoSize = true;
            this.label_T_Instruct.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label_T_Instruct.Location = new System.Drawing.Point(101, 7);
            this.label_T_Instruct.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_T_Instruct.Name = "label_T_Instruct";
            this.label_T_Instruct.Size = new System.Drawing.Size(81, 24);
            this.label_T_Instruct.TabIndex = 15;
            this.label_T_Instruct.Text = "Press Cal when \r\ntemp (c) is stable.\r\n";
            // 
            // TemperatureCal_Low
            // 
            this.TemperatureCal_Low.Location = new System.Drawing.Point(171, 49);
            this.TemperatureCal_Low.Margin = new System.Windows.Forms.Padding(2);
            this.TemperatureCal_Low.Name = "TemperatureCal_Low";
            this.TemperatureCal_Low.Size = new System.Drawing.Size(44, 22);
            this.TemperatureCal_Low.TabIndex = 3;
            // 
            // TemperatureCal_High
            // 
            this.TemperatureCal_High.Location = new System.Drawing.Point(171, 83);
            this.TemperatureCal_High.Margin = new System.Windows.Forms.Padding(2);
            this.TemperatureCal_High.Name = "TemperatureCal_High";
            this.TemperatureCal_High.Size = new System.Drawing.Size(44, 22);
            this.TemperatureCal_High.TabIndex = 3;
            // 
            // txBx_TemperatureCurrent
            // 
            this.txBx_TemperatureCurrent.Location = new System.Drawing.Point(11, 27);
            this.txBx_TemperatureCurrent.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_TemperatureCurrent.Name = "txBx_TemperatureCurrent";
            this.txBx_TemperatureCurrent.Size = new System.Drawing.Size(57, 22);
            this.txBx_TemperatureCurrent.TabIndex = 3;
            // 
            // btn_TCal_Hi
            // 
            this.btn_TCal_Hi.Location = new System.Drawing.Point(92, 83);
            this.btn_TCal_Hi.Margin = new System.Windows.Forms.Padding(2);
            this.btn_TCal_Hi.Name = "btn_TCal_Hi";
            this.btn_TCal_Hi.Size = new System.Drawing.Size(72, 33);
            this.btn_TCal_Hi.TabIndex = 1;
            this.btn_TCal_Hi.Text = "Cal Hi";
            this.btn_TCal_Hi.UseVisualStyleBackColor = true;
            this.btn_TCal_Hi.Click += new System.EventHandler(this.btn_TCal_Hi_Click);
            // 
            // btn_TCal_Low
            // 
            this.btn_TCal_Low.Location = new System.Drawing.Point(92, 49);
            this.btn_TCal_Low.Margin = new System.Windows.Forms.Padding(2);
            this.btn_TCal_Low.Name = "btn_TCal_Low";
            this.btn_TCal_Low.Size = new System.Drawing.Size(72, 33);
            this.btn_TCal_Low.TabIndex = 1;
            this.btn_TCal_Low.Text = "Cal Lo";
            this.btn_TCal_Low.UseVisualStyleBackColor = true;
            this.btn_TCal_Low.Click += new System.EventHandler(this.txBx_TCal_Low_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 6);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Temperature";
            // 
            // lbl_SvImage
            // 
            this.lbl_SvImage.AutoSize = true;
            this.lbl_SvImage.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_SvImage.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lbl_SvImage.Location = new System.Drawing.Point(30, 130);
            this.lbl_SvImage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_SvImage.Name = "lbl_SvImage";
            this.lbl_SvImage.Size = new System.Drawing.Size(40, 13);
            this.lbl_SvImage.TabIndex = 37;
            this.lbl_SvImage.Text = "Sv Img";
            this.lbl_SvImage.Click += new System.EventHandler(this.Save_Form_Image_Click);
            // 
            // lbl_SelectDevice
            // 
            this.lbl_SelectDevice.AutoSize = true;
            this.lbl_SelectDevice.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lbl_SelectDevice.Location = new System.Drawing.Point(33, 157);
            this.lbl_SelectDevice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_SelectDevice.Name = "lbl_SelectDevice";
            this.lbl_SelectDevice.Size = new System.Drawing.Size(87, 15);
            this.lbl_SelectDevice.TabIndex = 39;
            this.lbl_SelectDevice.Text = "Select Device >";
            this.lbl_SelectDevice.Click += new System.EventHandler(this.label_ReScanDevices);
            // 
            // btn_Query
            // 
            this.btn_Query.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_Query.Location = new System.Drawing.Point(32, 84);
            this.btn_Query.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Query.Name = "btn_Query";
            this.btn_Query.Size = new System.Drawing.Size(67, 30);
            this.btn_Query.TabIndex = 25;
            this.btn_Query.Text = "Query";
            this.btn_Query.UseVisualStyleBackColor = true;
            this.btn_Query.Click += new System.EventHandler(this.btn_Query_Click);
            // 
            // comboBox_Devices
            // 
            this.comboBox_Devices.FormattingEnabled = true;
            this.comboBox_Devices.Location = new System.Drawing.Point(33, 186);
            this.comboBox_Devices.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox_Devices.Name = "comboBox_Devices";
            this.comboBox_Devices.Size = new System.Drawing.Size(147, 23);
            this.comboBox_Devices.TabIndex = 38;
            this.comboBox_Devices.Text = "Choose your device";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Transparent;
            this.tabPage3.Controls.Add(this.btn_LoadSchema);
            this.tabPage3.Controls.Add(this.btn_OpenAppData);
            this.tabPage3.Controls.Add(this.BaseFolder);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.panel_Params);
            this.tabPage3.Controls.Add(this.btn_SaveSettings);
            this.tabPage3.Controls.Add(this.lbl_AdvancedEasy);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(865, 296);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Settings";
            // 
            // btn_LoadSchema
            // 
            this.btn_LoadSchema.Location = new System.Drawing.Point(723, 184);
            this.btn_LoadSchema.Name = "btn_LoadSchema";
            this.btn_LoadSchema.Size = new System.Drawing.Size(127, 23);
            this.btn_LoadSchema.TabIndex = 44;
            this.btn_LoadSchema.Text = "Reload Schema";
            this.btn_LoadSchema.UseVisualStyleBackColor = true;
            this.btn_LoadSchema.Click += new System.EventHandler(this.btn_ReloadSchema_Click);
            // 
            // btn_OpenAppData
            // 
            this.btn_OpenAppData.Location = new System.Drawing.Point(723, 216);
            this.btn_OpenAppData.Name = "btn_OpenAppData";
            this.btn_OpenAppData.Size = new System.Drawing.Size(127, 23);
            this.btn_OpenAppData.TabIndex = 0;
            this.btn_OpenAppData.Text = "Open Program Files";
            this.btn_OpenAppData.UseVisualStyleBackColor = true;
            this.btn_OpenAppData.Click += new System.EventHandler(this.btn_OpenAppData_Click);
            // 
            // BaseFolder
            // 
            this.BaseFolder.Location = new System.Drawing.Point(76, 205);
            this.BaseFolder.Margin = new System.Windows.Forms.Padding(2);
            this.BaseFolder.Name = "BaseFolder";
            this.BaseFolder.PlaceholderText = "c:\\temp\\record\\";
            this.BaseFolder.Size = new System.Drawing.Size(167, 23);
            this.BaseFolder.TabIndex = 6;
            this.toolTip1.SetToolTip(this.BaseFolder, "Location where raw data and results are saved.");
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label12.Location = new System.Drawing.Point(19, 210);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Folder";
            // 
            // panel_Params
            // 
            this.panel_Params.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Params.Controls.Add(this.label13);
            this.panel_Params.Controls.Add(this.RunTime_Minutes);
            this.panel_Params.Controls.Add(this.label_More_EEPROM);
            this.panel_Params.Controls.Add(this.lbl_Timing_Quick);
            this.panel_Params.Controls.Add(this.Timing_Defaults);
            this.panel_Params.Controls.Add(this.AllowLights);
            this.panel_Params.Controls.Add(this.Timing_Delta);
            this.panel_Params.Controls.Add(this.label9);
            this.panel_Params.Controls.Add(this.Timing_Send);
            this.panel_Params.Controls.Add(this.label19);
            this.panel_Params.Controls.Add(this.btn_Timing);
            this.panel_Params.Controls.Add(this.Timing_Sensor);
            this.panel_Params.Controls.Add(this.txBx_calSlope);
            this.panel_Params.Controls.Add(this.label18);
            this.panel_Params.Controls.Add(this.txBx_DeviceID);
            this.panel_Params.Controls.Add(this.txBx_Firmware);
            this.panel_Params.Controls.Add(this.txBx_CalIntercept);
            this.panel_Params.Controls.Add(this.label17);
            this.panel_Params.Controls.Add(this.btn_SendEEPROM);
            this.panel_Params.Controls.Add(this.label3);
            this.panel_Params.Controls.Add(this.label4);
            this.panel_Params.Controls.Add(this.label5);
            this.panel_Params.Controls.Add(this.label6);
            this.panel_Params.Controls.Add(this.label7);
            this.panel_Params.Controls.Add(this.label8);
            this.panel_Params.Controls.Add(this.Timing_Delay);
            this.panel_Params.Controls.Add(this.Timing_Light);
            this.panel_Params.Controls.Add(this.Timing_Cycle);
            this.panel_Params.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.panel_Params.Location = new System.Drawing.Point(290, 2);
            this.panel_Params.Margin = new System.Windows.Forms.Padding(2);
            this.panel_Params.Name = "panel_Params";
            this.panel_Params.Size = new System.Drawing.Size(246, 237);
            this.panel_Params.TabIndex = 32;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(110, 184);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 13);
            this.label13.TabIndex = 41;
            this.label13.Text = "Run (m)";
            // 
            // RunTime_Minutes
            // 
            this.RunTime_Minutes.Location = new System.Drawing.Point(181, 181);
            this.RunTime_Minutes.Margin = new System.Windows.Forms.Padding(2);
            this.RunTime_Minutes.Name = "RunTime_Minutes";
            this.RunTime_Minutes.Size = new System.Drawing.Size(52, 22);
            this.RunTime_Minutes.TabIndex = 40;
            // 
            // label_More_EEPROM
            // 
            this.label_More_EEPROM.AutoSize = true;
            this.label_More_EEPROM.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label_More_EEPROM.Location = new System.Drawing.Point(183, 212);
            this.label_More_EEPROM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_More_EEPROM.Name = "label_More_EEPROM";
            this.label_More_EEPROM.Size = new System.Drawing.Size(42, 13);
            this.label_More_EEPROM.TabIndex = 39;
            this.label_More_EEPROM.Text = "more ..";
            this.label_More_EEPROM.Click += new System.EventHandler(this.label_More_EEPROM_Click);
            // 
            // lbl_Timing_Quick
            // 
            this.lbl_Timing_Quick.AutoSize = true;
            this.lbl_Timing_Quick.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lbl_Timing_Quick.Location = new System.Drawing.Point(125, 213);
            this.lbl_Timing_Quick.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Timing_Quick.Name = "lbl_Timing_Quick";
            this.lbl_Timing_Quick.Size = new System.Drawing.Size(36, 13);
            this.lbl_Timing_Quick.TabIndex = 38;
            this.lbl_Timing_Quick.Text = "Quick";
            this.lbl_Timing_Quick.Click += new System.EventHandler(this.Timing_Defaults_Quick);
            // 
            // Timing_Defaults
            // 
            this.Timing_Defaults.AutoSize = true;
            this.Timing_Defaults.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.Timing_Defaults.Location = new System.Drawing.Point(81, 213);
            this.Timing_Defaults.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Timing_Defaults.Name = "Timing_Defaults";
            this.Timing_Defaults.Size = new System.Drawing.Size(32, 13);
            this.Timing_Defaults.TabIndex = 37;
            this.Timing_Defaults.Text = "Slow";
            this.Timing_Defaults.Click += new System.EventHandler(this.Timing_Defaults_Std);
            // 
            // AllowLights
            // 
            this.AllowLights.AutoSize = true;
            this.AllowLights.Location = new System.Drawing.Point(9, 208);
            this.AllowLights.Margin = new System.Windows.Forms.Padding(2);
            this.AllowLights.Name = "AllowLights";
            this.AllowLights.Size = new System.Drawing.Size(57, 17);
            this.AllowLights.TabIndex = 36;
            this.AllowLights.Text = "Lights";
            this.AllowLights.UseVisualStyleBackColor = true;
            // 
            // Timing_Delta
            // 
            this.Timing_Delta.Location = new System.Drawing.Point(57, 177);
            this.Timing_Delta.Margin = new System.Windows.Forms.Padding(2);
            this.Timing_Delta.Name = "Timing_Delta";
            this.Timing_Delta.Size = new System.Drawing.Size(52, 22);
            this.Timing_Delta.TabIndex = 8;
            this.Timing_Delta.TextChanged += new System.EventHandler(this.ChangeTimingText);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 181);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 34;
            this.label9.Text = "Delta";
            this.label9.Click += new System.EventHandler(this.Show_TimingExplain);
            // 
            // Timing_Send
            // 
            this.Timing_Send.Location = new System.Drawing.Point(57, 151);
            this.Timing_Send.Margin = new System.Windows.Forms.Padding(2);
            this.Timing_Send.Name = "Timing_Send";
            this.Timing_Send.Size = new System.Drawing.Size(52, 22);
            this.Timing_Send.TabIndex = 7;
            this.Timing_Send.TextChanged += new System.EventHandler(this.ChangeTimingText);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 155);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(33, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "Send";
            this.label19.Click += new System.EventHandler(this.Show_TimingExplain);
            // 
            // btn_Timing
            // 
            this.btn_Timing.Location = new System.Drawing.Point(30, 8);
            this.btn_Timing.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Timing.Name = "btn_Timing";
            this.btn_Timing.Size = new System.Drawing.Size(72, 31);
            this.btn_Timing.TabIndex = 4;
            this.btn_Timing.Text = "Update";
            this.btn_Timing.UseVisualStyleBackColor = true;
            this.btn_Timing.Click += new System.EventHandler(this.UpdateLightTiming_Click);
            // 
            // Timing_Sensor
            // 
            this.Timing_Sensor.Location = new System.Drawing.Point(57, 124);
            this.Timing_Sensor.Margin = new System.Windows.Forms.Padding(2);
            this.Timing_Sensor.Name = "Timing_Sensor";
            this.Timing_Sensor.Size = new System.Drawing.Size(52, 22);
            this.Timing_Sensor.TabIndex = 6;
            this.Timing_Sensor.TextChanged += new System.EventHandler(this.ChangeTimingText);
            // 
            // txBx_calSlope
            // 
            this.txBx_calSlope.Location = new System.Drawing.Point(181, 75);
            this.txBx_calSlope.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_calSlope.Name = "txBx_calSlope";
            this.txBx_calSlope.Size = new System.Drawing.Size(52, 22);
            this.txBx_calSlope.TabIndex = 3;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label18.Location = new System.Drawing.Point(7, 128);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(34, 12);
            this.label18.TabIndex = 30;
            this.label18.Text = "Sensor";
            this.label18.Click += new System.EventHandler(this.Show_TimingExplain);
            // 
            // txBx_DeviceID
            // 
            this.txBx_DeviceID.Location = new System.Drawing.Point(181, 43);
            this.txBx_DeviceID.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_DeviceID.Name = "txBx_DeviceID";
            this.txBx_DeviceID.Size = new System.Drawing.Size(52, 22);
            this.txBx_DeviceID.TabIndex = 3;
            // 
            // txBx_Firmware
            // 
            this.txBx_Firmware.Location = new System.Drawing.Point(181, 138);
            this.txBx_Firmware.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_Firmware.Name = "txBx_Firmware";
            this.txBx_Firmware.ReadOnly = true;
            this.txBx_Firmware.Size = new System.Drawing.Size(52, 22);
            this.txBx_Firmware.TabIndex = 29;
            // 
            // txBx_CalIntercept
            // 
            this.txBx_CalIntercept.Location = new System.Drawing.Point(181, 108);
            this.txBx_CalIntercept.Margin = new System.Windows.Forms.Padding(2);
            this.txBx_CalIntercept.Name = "txBx_CalIntercept";
            this.txBx_CalIntercept.Size = new System.Drawing.Size(52, 22);
            this.txBx_CalIntercept.TabIndex = 3;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label17.Location = new System.Drawing.Point(112, 141);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 12);
            this.label17.TabIndex = 28;
            this.label17.Text = "Firmware";
            // 
            // btn_SendEEPROM
            // 
            this.btn_SendEEPROM.Location = new System.Drawing.Point(131, 8);
            this.btn_SendEEPROM.Margin = new System.Windows.Forms.Padding(2);
            this.btn_SendEEPROM.Name = "btn_SendEEPROM";
            this.btn_SendEEPROM.Size = new System.Drawing.Size(84, 31);
            this.btn_SendEEPROM.TabIndex = 4;
            this.btn_SendEEPROM.Text = "Push *";
            this.toolTip1.SetToolTip(this.btn_SendEEPROM, "Remember to limit the use on this if possible");
            this.btn_SendEEPROM.UseVisualStyleBackColor = true;
            this.btn_SendEEPROM.Click += new System.EventHandler(this.btn_SendEEPROM_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(112, 47);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "deviceID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(112, 78);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "cal Slope";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(112, 110);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "cal icept";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 101);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Delay";
            this.label6.Click += new System.EventHandler(this.Show_TimingExplain);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 47);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Cycle";
            this.toolTip1.SetToolTip(this.label7, "Click on this for more information");
            this.label7.Click += new System.EventHandler(this.Show_TimingExplain);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 74);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Light";
            this.label8.Click += new System.EventHandler(this.Show_TimingExplain);
            // 
            // Timing_Delay
            // 
            this.Timing_Delay.Location = new System.Drawing.Point(57, 97);
            this.Timing_Delay.Margin = new System.Windows.Forms.Padding(2);
            this.Timing_Delay.Name = "Timing_Delay";
            this.Timing_Delay.Size = new System.Drawing.Size(52, 22);
            this.Timing_Delay.TabIndex = 5;
            this.Timing_Delay.TextChanged += new System.EventHandler(this.ChangeTimingText);
            // 
            // Timing_Light
            // 
            this.Timing_Light.Location = new System.Drawing.Point(57, 69);
            this.Timing_Light.Margin = new System.Windows.Forms.Padding(2);
            this.Timing_Light.Name = "Timing_Light";
            this.Timing_Light.Size = new System.Drawing.Size(52, 22);
            this.Timing_Light.TabIndex = 4;
            this.Timing_Light.TextChanged += new System.EventHandler(this.ChangeTimingText);
            // 
            // Timing_Cycle
            // 
            this.Timing_Cycle.Location = new System.Drawing.Point(57, 43);
            this.Timing_Cycle.Margin = new System.Windows.Forms.Padding(2);
            this.Timing_Cycle.Name = "Timing_Cycle";
            this.Timing_Cycle.Size = new System.Drawing.Size(52, 22);
            this.Timing_Cycle.TabIndex = 3;
            this.Timing_Cycle.TextChanged += new System.EventHandler(this.ChangeTimingText);
            // 
            // btn_SaveSettings
            // 
            this.btn_SaveSettings.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_SaveSettings.Location = new System.Drawing.Point(566, 49);
            this.btn_SaveSettings.Margin = new System.Windows.Forms.Padding(2);
            this.btn_SaveSettings.Name = "btn_SaveSettings";
            this.btn_SaveSettings.Size = new System.Drawing.Size(92, 29);
            this.btn_SaveSettings.TabIndex = 8;
            this.btn_SaveSettings.Text = "Save Settings";
            this.toolTip1.SetToolTip(this.btn_SaveSettings, "Not necessary, usually saves automatically . . ");
            this.btn_SaveSettings.UseVisualStyleBackColor = true;
            this.btn_SaveSettings.Click += new System.EventHandler(this.btn_SaveSettings_Click);
            // 
            // lbl_AdvancedEasy
            // 
            this.lbl_AdvancedEasy.AutoSize = true;
            this.lbl_AdvancedEasy.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lbl_AdvancedEasy.Location = new System.Drawing.Point(585, 149);
            this.lbl_AdvancedEasy.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_AdvancedEasy.Name = "lbl_AdvancedEasy";
            this.lbl_AdvancedEasy.Size = new System.Drawing.Size(31, 15);
            this.lbl_AdvancedEasy.TabIndex = 43;
            this.lbl_AdvancedEasy.Text = "Adv.";
            this.lbl_AdvancedEasy.Click += new System.EventHandler(this.lbl_AdvancedEasy_Click);
            // 
            // timer_Update
            // 
            this.timer_Update.Tick += new System.EventHandler(this.timer_Update_Tick);
            // 
            // RGD_FormMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(879, 702);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "RGD_FormMain";
            this.Text = "RapidGeneticDetection Device Control";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RGD_FormMain_FormClosing);
            this.ResizeEnd += new System.EventHandler(this.RGD_FormMain_ResizeEnd);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel_TempHeating.ResumeLayout(false);
            this.panel_TempHeating.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel_Params.ResumeLayout(false);
            this.panel_Params.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer_Update;
        private System.Windows.Forms.Button btn_TCal_Low;
        private System.Windows.Forms.Button btn_TCal_Hi;
        private System.Windows.Forms.TextBox txBx_TemperatureCurrent;
        private System.Windows.Forms.TextBox TemperatureCal_High;
        private System.Windows.Forms.TextBox TemperatureCal_Low;
        private System.Windows.Forms.TextBox txBx_CalIntercept;
        private System.Windows.Forms.TextBox txBx_DeviceID;
        private System.Windows.Forms.TextBox txBx_calSlope;
        private System.Windows.Forms.Button btn_SendEEPROM;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_SaveSettings;
        private System.Windows.Forms.TextBox User;
        private System.Windows.Forms.TextBox BaseFolder;
        private System.Windows.Forms.TextBox Subject;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox DisplayLogarithmic;
        private System.Windows.Forms.TextBox Temperature_Tolerance;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox Temperature_Ideal;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label_T_Instruct;
        private System.Windows.Forms.Button btn_Timing;
        private System.Windows.Forms.TextBox Timing_Cycle;
        private System.Windows.Forms.TextBox Timing_Light;
        private System.Windows.Forms.TextBox Timing_Delay;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txBx_Update;
        private System.Windows.Forms.Button btn_Connect;
        private System.Windows.Forms.Button btn_Query;
        private System.Windows.Forms.Button btn_Stop;
        private System.Windows.Forms.TextBox txBx_Firmware;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox Timing_Sensor;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel_Params;
        private System.Windows.Forms.TextBox Timing_Send;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Timing_Delta;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel_TempHeating;
        private System.Windows.Forms.CheckBox AllowLights;
        private System.Windows.Forms.Button btn_Clear;
        private System.Windows.Forms.Label Timing_Defaults;
        private System.Windows.Forms.Label lbl_Timing_Quick;
        private System.Windows.Forms.Label label_More_EEPROM;
        private System.Windows.Forms.Button btn_Run_30min;
        private System.Windows.Forms.CheckBox chkBox_Heater;
        private System.Windows.Forms.Button btn_Diagnostics;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lbl_SvImage;
        private System.Windows.Forms.ComboBox comboBox_Devices;
        private System.Windows.Forms.Label lbl_SelectDevice;
        private System.Windows.Forms.Label label_Running;
        private System.Windows.Forms.Button btn_PreHeat;
        private System.Windows.Forms.Label lbl_AdvancedEasy;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox RunTime_Minutes;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox Metadata_Tube2;
        private RunSchemaPanel panel_RunSchema;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.CheckBox SendNotification;
        private System.Windows.Forms.Button btn_OpenAppData;
        private System.Windows.Forms.TextBox txt_AddInfoTube3;
        private System.Windows.Forms.TextBox txt_AddInfoTube1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btn_LoadSchema;
        public System.Windows.Forms.Label lbl_Instructions;
    }
}