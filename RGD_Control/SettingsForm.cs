﻿using RGD_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RGD_Control
{
    public partial class SettingsForm : Form
    {
        RGD_FormMain RGDForm;
        MainController Controller;
        public string Prefix = "";
        public int shiftRight = 180;
        public int NextTop = 40;

        public List<string> TB_Names = new List<string>() { "SensorTube1",      "SensorTube2",     "SensorTube3",     "LEDTube1",     "LEDTube3",     "Thermo1",             "Thermo2",               "Heater",     "Buzzer",     "LEDrgbR",      "LEDrgbG",      "LEDrgbB",     "Temp2Optimal",  "Temp2Range" };
        public List<string> TB_Labels = new List<string>() { "SensorTube1 (A)", "SensorTube2 (A)", "SensorTube3 (A)", "LEDTube1 (D)", "LEDTube3 (D)", "Thermo1 in-Tube (A)", "Thermo2 in-Holder (A)", "Heater (D)", "Buzzer (D)", "LEDrgb R (D)", "LEDrgb G (D)", "LEDrgb B (D)","TempV Optimal", "TempV Range" };
        public List<string> TB_Defaults = new List<string>() { "1",             "4",               "6",               "2",            "3",            "3",                   "2",                      "12",         "5",         "7",            "8",            "9",           "760",           "10" };

        public CheckBox Allow_Sound;

        public SettingsForm(RGD_FormMain rgdform)
        {
            RGDForm = rgdform; 
            Controller = RGDForm.Controller;
            InitializeComponent();

            for (int i = 0; i < TB_Names.Count; i++) AddPair(TB_Names[i], TB_Labels[i], TB_Defaults[i]);

            Label lMsg = new Label();
            lMsg.Text = "Warning, make sure you know what you are doing.";
            lMsg.Top = 5; lMsg.Left = 10;
            lMsg.Width = shiftRight * 2; lMsg.Height = 50;
            Controls.Add(lMsg);

            Button btn_Send = new Button();
            btn_Send.Top = 30; btn_Send.Left = shiftRight * 3;
            btn_Send.Text = "Push Pins (up to Heater)";
            btn_Send.Click += Btn_Send1_Click;
            btn_Send.Width = shiftRight;
            btn_Send.Height = 35;
            Controls.Add(btn_Send);

            Button btn_Send2 = AddButton(btn_Send, "Push Pins (Status)");
            btn_Send2.Click += Btn_Send2_Click;

            Button btn_Defaults = AddButton(btn_Send2, "Defaults");
            btn_Defaults.Click += Btn_Defaults_Click;

            Button btn_Test_Buzzer = AddButton(btn_Defaults,"Try Buzzer");
            btn_Test_Buzzer.Click += Btn_BuzzerTest_Click;

            Button btn_Test_StatusLights = AddButton(btn_Test_Buzzer, "Status Lights");
            btn_Test_StatusLights.Click += Btn_Test_StatusLights_Click;

            Button btn_Toggle_Tube2 = AddButton(btn_Test_StatusLights, "Toggle Tube 2");
            btn_Toggle_Tube2.Click += Btn_Enable_Tube2_Click;

            Button btn_Load_Raw = AddButton(btn_Toggle_Tube2, "Load Analyze Raw");
            btn_Load_Raw.Click += Btn_Load_Raw_Click;

            Button btn_Check_Run = AddButton(btn_Load_Raw, "Check Run");
            btn_Check_Run.Click += btn_Check_Run_Click;

            Allow_Sound = new CheckBox();
            Allow_Sound.Top = btn_Load_Raw.Top + 38; Allow_Sound.Left = shiftRight * 3;
            Allow_Sound.Width = shiftRight;
            Allow_Sound.Height = 35;
            Allow_Sound.Text = "Allow Sound";
            Allow_Sound.CheckedChanged += Allow_Sound_CheckedChanged;
            Controls.Add(Allow_Sound);

            this.FormClosing += SettingsForm_FormClosing;
        }

        private void btn_Check_Run_Click(object sender, EventArgs e)
        {
            var form = new ModelCheck(Controller);
            form.Show();
        }

        private void Btn_Load_Raw_Click(object sender, EventArgs e)
        {
            Debugger.Break();
            string RawFolder = @"R:\five\exp\FIV335\";
            Controller.LoadRawData(RawFolder);
            Debug.Print("Done Re-Analyzing");
        }

        private Button AddButton(Button Previous, string Text)
        {
            Button newButton = new Button();
            newButton.Top = Previous.Top + 38; newButton.Left = Previous.Left;
            newButton.Text = Text;
            newButton.Width = Previous.Width;
            newButton.Height = Previous.Height;
            Controls.Add(newButton);
            return newButton;
        }

        private void Btn_Defaults_Click(object sender, EventArgs e)
        {
            WriteToBoxes(TB_Defaults);
        }

        private void WriteToBoxes(List<string> Values)
        {
            for (int i = 0; i < TB_Names.Count; i++)
            {
                TextBox TB = (TextBox)Controls.Find(TB_Names[i], true).First();
                TB.Text = Values[i];
            }
        }

        private void Btn_Send1_Click(object sender, EventArgs e)
        {
            List<byte> bArr = new List<byte>();
            foreach (string TS in TB_Names) { bArr.Add((byte)GetNum(TS)); if (bArr.Count > 7) break; }
            RGDForm.Send_EEPROM_Ext1(bArr[0], bArr[1], bArr[2], bArr[3], bArr[4], bArr[5], bArr[6], bArr[7]);
        }

        private void Btn_Send2_Click(object sender, EventArgs e)
        {
            List<object> bArr = new List<object>();
            foreach (string TS in TB_Names) bArr.Add(GetNum(TS));
            if (bArr[13].GetType().Name == "Byte") bArr[13] = (float)(byte)bArr[13];
            RGDForm.Send_EEPROM_Ext2((byte) bArr[8], (byte)bArr[9], (byte)bArr[10], (byte)bArr[11], (float)bArr[12], (float)(bArr[13]));
        }

        private void Btn_BuzzerTest_Click(object sender, EventArgs e)
        {
            RGDForm.Send_Status_Buzzer_Test();
        }

        private void Btn_Test_StatusLights_Click(object sender, EventArgs e)
        {
            RGDForm.Send_Status_Lights_Toggle();
        }

        private void Btn_Enable_Tube2_Click(object sender, EventArgs e)
        {
            RGDForm.Enable_Tube2();
        }

        private void Allow_Sound_CheckedChanged(object sender, EventArgs e)
        {
            RGDForm.UpdateStore();
        }

        //-----------------------------------------------------------------------------------------------------------------------------------------

        public object GetNum(string TextSetName)
        {
            TextBox TB = (TextBox)Controls.Find(TextSetName, true).First();
            byte t1 =0 ;
            float t2 = 0;
            if (!byte.TryParse(TB.Text, out t1))
            {
                float.TryParse(TB.Text, out t2);
                return t2;
            }
            return t1;
        }

        public void AddPair(string Names, string sLabel, string Val)
        {
            Label lbl = new Label();
            lbl.Text = sLabel;
            lbl.Width = shiftRight - 10;
            lbl.Top = NextTop += 38; lbl.Left = 4;

            TextBox txb = new TextBox();
            txb.Name = Prefix + Names;
            txb.Top = lbl.Top; txb.Left = lbl.Left + shiftRight;
            txb.Width = shiftRight - 10;
            txb.Text = Val;

            this.Controls.Add(lbl);
            this.Controls.Add(txb);
        }

        private void SettingsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        internal void UpdateSettingsFromDevice(MainController controller)
        {
            var device = controller.Device;
            WriteToBoxes(new List<string>(13) {
                device.Pins.SENgT1.ToString(), device.Pins.SENgT2.ToString(), device.Pins.SENgT3.ToString(), device.Pins.LEDgT1.ToString(), device.Pins.LEDgT3.ToString(),
                device.Pins.Thermo1.ToString(), device.Pins.Thermo2.ToString(), device.Pins.Heater.ToString(),
                device.Pins.Buzzer.ToString(), device.Pins.LEDrgbR.ToString(), device.Pins.LEDrgbG.ToString(), device.Pins.LEDrgbB.ToString(), controller.tempHeater2.OptimalValue.ToString(), controller.tempHeater2.OptimalRange.ToString()
            });
        }
    }
}
