﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using RGD_Library;

namespace RGD_Control
{
    public partial class CurveViewer : PictureBox
    {
        public CurveViewer()
        {
            BackColor = Color.White;
            //https://www.codeproject.com/Questions/864097/Need-help-with-paint-program-Winform-csharp
            //https://stackoverflow.com/questions/12813752/avoid-flickering-in-windows-forms
            //https://docs.microsoft.com/en-us/dotnet/framework/winforms/advanced/how-to-reduce-graphics-flicker-with-double-buffering-for-forms-and-controls
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            Channels = new List<PlottableChannel>();
            Channels_LabelOnly = new List<PlottableChannel>();
            CurvePens = Curve_PensSetup.Channelx3_Default;
        }

        public List<PlottableChannel> Channels { get; set; }
        public List<PlottableChannel> Channels_LabelOnly { get; set; }
        private Control Canvas => this;
        private Graphics g; //Sets just-in-time
        public float FontSize_Points => LabelFont.SizeInPoints; // 1 point is 1.3333 pixels
        public float FontSize_Pixels => (float)(FontSize_Points * 1.333);
        public int FontPadding = 5;
        public int Border_x = 30;
        public int Border_y = 10;

        public bool UseLogarithmicY = false;
        public Pen GridPen = new Pen(Color.Gray, 1.0f);
        public Pen BackPen = new Pen(Color.FromArgb(40, 40, 40), 2.0f);
        public Brush TextBrush = new SolidBrush(Color.FromArgb(40, 40, 40));
        private StringFormat _xlabelformat = null;
        public StringFormat xlabelformat
        {
            get
            {
                if (_xlabelformat != null) return _xlabelformat;
                else
                {
                    _xlabelformat = new StringFormat();
                    _xlabelformat.LineAlignment = StringAlignment.Center;
                    _xlabelformat.Alignment = StringAlignment.Center;
                    return _xlabelformat;
                }
            }
        }
        private StringFormat _ylabelformat = null;
        public StringFormat ylabelformat
        {
            get
            {
                if (_ylabelformat != null) return _ylabelformat;
                else
                {
                    _ylabelformat = new StringFormat();
                    _ylabelformat.LineAlignment = StringAlignment.Center;
                    _ylabelformat.Alignment = StringAlignment.Far;
                    return _ylabelformat;
                }
            }
        }
        public List<Brush> LabelBrush;
        public List<Pen> ChPen;
        public float ChPen_Width = 2.0f;
        public Font LabelFont = new Font("Calibri", 13f, FontStyle.Bold);
        private int FontSpace => (int)(FontSize_Pixels) + (2 * FontPadding);
        private int CharsRes = 3; //Reserve this many characters for the y-axis labels
        private bool ClearSet = false; //Causes the next draw to be blank
        public Rectangle CenterRect => new Rectangle((FontSpace * CharsRes) + Border_x, Border_y, Canvas.Width - (FontSpace * CharsRes) - (Border_x * 2), Canvas.Height - FontSpace - (Border_y * 2));

        public enum Curve_PensSetup
        {
            Channelx3_Default,
            Channelx1_Temperature,
            Other
        }

        private Curve_PensSetup _CurvePens;
        public Curve_PensSetup CurvePens
        {
            get { return _CurvePens; }
            set
            {
                _CurvePens = value;
                switch (value)
                {
                    case Curve_PensSetup.Channelx3_Default:
                        ChPen = new List<Pen>(5) { 
                            new Pen(Color.FromArgb(6, 58, 250), ChPen_Width), 
                            new Pen(Color.FromArgb(20, 175, 0), ChPen_Width), 
                            new Pen(Color.FromArgb(255, 30, 30), ChPen_Width),
                            new Pen(Color.FromArgb(210, 170 ,170), ChPen_Width),
                            new Pen(Color.FromArgb(254, 6, 88), ChPen_Width) 
                        };
                        break;
                    case Curve_PensSetup.Channelx1_Temperature:
                        ChPen[0] = new Pen(Color.Red, 3.0f);
                        break;
                    case Curve_PensSetup.Other:
                        break;
                    default:
                        break;
                }
                LabelBrush = new List<Brush>();
                for (int i = 0; i < ChPen.Count; i++) LabelBrush.Add(new SolidBrush(ChPen[i].Color));
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Draw_Full(this, e);
            //base.OnPaint(e);
        }
        
        /// <summary>
        /// Redraws just the BG grid (kind of erases things)
        /// </summary>
        public void Clear()
        {
            ClearSet = true;
            this.Refresh();
        }

        public void Draw_Full(object sender, PaintEventArgs e)
        {
            g = e.Graphics;
            int ch;

            //Refresh the Background Grid
            Refresh_Background(Channels);
            if (ClearSet) {
                //Allows this next stuff to be skipped so it can be cleared out . . 
                g.DrawString("Preparing . .", LabelFont, LabelBrush[0], CenterRect.Left + 12, CenterRect.Top + 12 + (0 * FontSpace));
                ClearSet = false; return; 
            } 

            //Channels means a "PlottableChannel"
            //Optical Sensors from the Tubes
            for (ch = 0; ch < Channels.Count; ch++)
                DrawChannel(Channels[ch], ch, true);
            //Temperature Information
            for (ch = 0; ch < Channels_LabelOnly.Count; ch++)
                DrawChannel(Channels_LabelOnly[ch], ch + Channels.Count, true, true);
        }

        public void DrawChannel(PlottableChannel channel, int idx, bool FullRedraw = true, bool OnlyLabel = false)
        {
            float[] tRecent = null;
            try
            {
                tRecent = channel.Recent.ToArray();
            }
            catch { 
                return; 
            } //Sometimes things get hairy . . just skip out

            int x; int y; int xp = 0; int yp = 0; double fraction_y;
            int xmax = Math.Max(tRecent.Length, (int)(channel.BufferSize / channel.InitialZoom));
            int start = FullRedraw ? 0 : tRecent.Length;

            //Label this line
            g.DrawString(channel.ChannelName + (OnlyLabel ? " " + channel.LatestValue : ""), LabelFont, LabelBrush[idx], CenterRect.Left + 12, CenterRect.Top + 12 + (idx * FontSpace));

            if (!OnlyLabel)
            {
                for (int i = start; i < tRecent.Length; i++)
                {
                    x = Math.Max(0, CenterRect.Left + CenterRect.Width * i / xmax);
                    fraction_y = Calc_Fraction_Y(channel, tRecent[i]);
                    y = Math.Max(0, (int)(CenterRect.Height - (CenterRect.Height * fraction_y)));
                    if (i > 0) g.DrawLine(ChPen[idx], xp, yp, x, y); //Calculate the first time, but don't draw
                    xp = x; yp = y;
                }
            }
        }

        private double Calc_Fraction_Y(PlottableChannel channel, float y_internal)
        {
            return Logify_Fraction((y_internal - channel.rectValue.Y) / (channel.rectValue.Height - channel.rectValue.Y));
        }

        Dictionary<double, string> Fraction_Label_Log = new Dictionary<double, string>(3) { { 0, "0" }, { 0.025, "2.5" }, { 0.05, "5.0" }, { 0.1, "10" }, { 0.2, "25" }, { 0.5, "50" }, { 0.75, "75" }, { 1, "100" } };
        internal void Refresh_Background(List<PlottableChannel> Chx)
        {
            g.FillRectangle(new SolidBrush(Color.FromArgb(220, 220, 220)), CenterRect);
            DrawGridLines(g, GridPen, 10, true, Chx);
            DrawGridLines(g, GridPen, 10, false, Chx, UseLogarithmicY ? Fraction_Label_Log : null);
            g.DrawRectangle(BackPen, CenterRect);
        }

        private void DrawGridLines(Graphics g, Pen Pen, int NumLines, bool DrawAlongX, List<PlottableChannel> Chx, Dictionary<double, string> Fraction_Label = null)
        {
            float DisplayWidth = Chx == null ? 100 : (Chx.Count < 1 ? 100 : Chx[0].rectDisplay.Width);
            float DisplayHeight = Chx == null ? 100 : (Chx.Count < 1 ? 100 : Chx[0].rectDisplay.Height);
            int dim; double fraction; string fract_lbl;
            if (Fraction_Label == null)
            {
                Fraction_Label = new Dictionary<double, string>(NumLines);
                for (int i = 0; i <= NumLines; i++)
                {
                    fraction = (i * (DrawAlongX ? DisplayWidth : DisplayHeight) / NumLines);
                    fract_lbl = DoFormat(fraction);
                    Fraction_Label.Add((double)i / NumLines, fract_lbl);
                }
            }
            foreach (KeyValuePair<double, string> kvp in Fraction_Label)
            {
                fraction = kvp.Key;
                if (DrawAlongX)
                {
                    dim = CenterRect.Left + (int)(CenterRect.Width * fraction);
                    g.DrawLine(Pen, dim, CenterRect.Top, dim, CenterRect.Bottom);
                    dim -= (int)(FontSize_Pixels / 2) - 2;
                    g.DrawString(kvp.Value, LabelFont, TextBrush, dim, CenterRect.Bottom + 15, xlabelformat);
                }
                else
                {
                    fraction = Logify_Fraction(fraction);
                    dim = CenterRect.Height - (CenterRect.Top + (int)(CenterRect.Height * fraction));
                    g.DrawLine(Pen, CenterRect.Left, dim, CenterRect.Right, dim);
                    dim -= (int)(FontSize_Pixels / 2) - 5;
                    g.DrawString(kvp.Value, LabelFont, TextBrush, CenterRect.Left - 10, dim, ylabelformat);
                }
            }
        }

        private static string DoFormat(double fraction)
        {
            string fract_lbl;
            if (fraction < 1)
                fract_lbl = fraction.ToString("0.00");
            else if (fraction < 10)
                fract_lbl = fraction.ToString("0");
            else
                fract_lbl = fraction.ToString("0");

            fract_lbl = fract_lbl.Replace(".00", "");
            if (fract_lbl.Contains("."))
            {
                if (fract_lbl.EndsWith("0")) fract_lbl = fract_lbl.Substring(0, fract_lbl.Length - 1);
            }
            return fract_lbl;
        }

        double log_min = 0.01; //If the value is expected to be lower than this, then this can get adjusted down
        double log_min_l10 => Math.Log10(log_min);
        public double Logify_Fraction(double fraction)
        {
            if (UseLogarithmicY)
            {
                fraction = (Math.Log10(fraction + log_min) - log_min_l10) / (Math.Log10(1 + log_min) - log_min_l10);
            }
            return fraction;
        }
    }
}
