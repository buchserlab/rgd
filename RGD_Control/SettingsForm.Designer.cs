﻿namespace RGD_Control
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.btn_FluorTest_T3Blank = new System.Windows.Forms.Button();
            this.btn_FluorTest_T1Blank = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.btn_HeatTest_1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.btn_FluorTest_T3Blank);
            this.panel1.Controls.Add(this.btn_FluorTest_T1Blank);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.btn_HeatTest_1);
            this.panel1.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.panel1.Location = new System.Drawing.Point(774, 492);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(105, 135);
            this.panel1.TabIndex = 41;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(9, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 21);
            this.label14.TabIndex = 23;
            this.label14.Text = "Fluor Test:";
            // 
            // btn_FluorTest_T3Blank
            // 
            this.btn_FluorTest_T3Blank.Location = new System.Drawing.Point(9, 101);
            this.btn_FluorTest_T3Blank.Name = "btn_FluorTest_T3Blank";
            this.btn_FluorTest_T3Blank.Size = new System.Drawing.Size(88, 28);
            this.btn_FluorTest_T3Blank.TabIndex = 22;
            this.btn_FluorTest_T3Blank.Text = "T3=Blank";
            this.btn_FluorTest_T3Blank.UseVisualStyleBackColor = true;
            this.btn_FluorTest_T3Blank.Click += new System.EventHandler(RGDForm.btn_FluorTest_T3Blank_Click);
            // 
            // btn_FluorTest_T1Blank
            // 
            this.btn_FluorTest_T1Blank.Location = new System.Drawing.Point(9, 66);
            this.btn_FluorTest_T1Blank.Name = "btn_FluorTest_T1Blank";
            this.btn_FluorTest_T1Blank.Size = new System.Drawing.Size(88, 28);
            this.btn_FluorTest_T1Blank.TabIndex = 21;
            this.btn_FluorTest_T1Blank.Text = "T1=Blank";
            this.btn_FluorTest_T1Blank.UseVisualStyleBackColor = true;
            this.btn_FluorTest_T1Blank.Click += new System.EventHandler(RGDForm.btn_FluorTest_T1Blank_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 21);
            this.label13.TabIndex = 20;
            this.label13.Text = "Testing:";
            // 
            // btn_HeatTest_1
            // 
            this.btn_HeatTest_1.Location = new System.Drawing.Point(9, 19);
            this.btn_HeatTest_1.Name = "btn_HeatTest_1";
            this.btn_HeatTest_1.Size = new System.Drawing.Size(88, 28);
            this.btn_HeatTest_1.TabIndex = 19;
            this.btn_HeatTest_1.Text = "Heat Check";
            this.btn_HeatTest_1.UseVisualStyleBackColor = true;
            this.btn_HeatTest_1.Click += new System.EventHandler(RGDForm.btn_HeatTest_1_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 716);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "SettingsForm";
            this.Text = "Settings Form";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.Button btn_FluorTest_T3Blank;
        public System.Windows.Forms.Button btn_FluorTest_T1Blank;
        public System.Windows.Forms.Button btn_HeatTest_1;
    }
}