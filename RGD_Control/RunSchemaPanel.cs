﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using RGD_Library;
using System.Linq;
using System.Security.Policy;

namespace RGD_Control
{

    public partial class RunSchemaPanel : UserControl
    {
        private RunSchemaList _rawSchemaList = new RunSchemaList();
        private RunSchemaList _schemaList = new RunSchemaList();
        public RunSchemaState SchemaState;
        public int SelectedIndex = 0;
        public RunSchema CurrentSchema { get { return _schemaList[SelectedIndex]; } }
        public int BufferSize = 10;
        public int RectHeight = 30;
        public int RectSemiHeight => RectHeight / 2;
        public int RectWidth => panelPainter.Width - 2 * BufferSize - NumLabelWidth;
        public int BetweenRectHeight = 10;
        public int CircleDiameter => RectHeight - 2 * CircleBuffer;
        public int CircleBuffer = 2;

        public int NumLabelWidth = 20;

        public StringFormat drawCenter;


        public static Pen BasicPen = new Pen(Color.Black, 1);
        public static SolidBrush FontBrush = new SolidBrush(Color.Black);
        public static Font BasicFont = new Font("Arial", 10);
        public static Font LabelFont = new Font("Arial", 12, FontStyle.Bold);
        public static SolidBrush SelectedBrush = new SolidBrush(Color.Yellow);

        public int TotalDrawHeight { get => 2 * BufferSize + _schemaList.Count * RectHeight + (_schemaList.Count - 1) * BetweenRectHeight; }
        public RunSchemaPanel()
        {
            drawCenter = new StringFormat();
            drawCenter.Alignment = StringAlignment.Center;
            drawCenter.LineAlignment = StringAlignment.Center;
            InitializeComponent();
            txt_Index.Text = (SelectedIndex + 1).ToString();
            UpdateParent(false);
        }

        private void panelPaint_Draw(object sender, PaintEventArgs e)
        {
            using (Graphics g = panelPainter.CreateGraphics())
            {
                RunSchema schema;
                Point ulCorner;
                for (int i = 0; i < _schemaList.Count; i++)
                {
                    schema = _schemaList[i];
                    ulCorner = new Point(BufferSize + NumLabelWidth, BufferSize + i * (RectHeight + BetweenRectHeight));
                    g.DrawString($"{i + 1}", BasicFont, FontBrush,
                        (BufferSize + NumLabelWidth) / 2, ulCorner.Y + RectSemiHeight, drawCenter);
                    if (i == SelectedIndex)
                    {
                        g.FillRectangle(SelectedBrush, ulCorner.X, ulCorner.Y, RectWidth, RectHeight);
                    }
                    g.DrawRectangle(BasicPen, ulCorner.X, ulCorner.Y , RectWidth, RectHeight);
                    g.DrawEllipse(BasicPen, ulCorner.X + CircleBuffer, ulCorner.Y + CircleBuffer, 
                        CircleDiameter, CircleDiameter);
                    g.DrawEllipse(BasicPen, ulCorner.X + RectWidth - CircleDiameter - CircleBuffer, ulCorner.Y + CircleBuffer, 
                        CircleDiameter, CircleDiameter);
                    g.DrawString(RunSchema.SampleasChar(schema.Tube1Type) /*+ (schema.BothSamples ? "1" : "")*/, 
                        LabelFont, new SolidBrush(schemaColor(schema.Tube1Type)), 
                        ulCorner.X + RectSemiHeight, ulCorner.Y + RectSemiHeight, drawCenter);
                    g.DrawString(RunSchema.SampleasChar(schema.Tube3Type) /*+ (schema.BothSamples ? (CurrentSchema.Flags.HasFlag(SchemaFlags.SamplesAreDifferent) ? "2" : "1" ) : "")*/, 
                        LabelFont, new SolidBrush(schemaColor(schema.Tube3Type)), 
                        ulCorner.X + RectWidth - RectSemiHeight, ulCorner.Y + RectSemiHeight, drawCenter);
                }
            }
        }

        public Color schemaColor(SampleType sampleType)
        {
            switch (sampleType)
            {
                case SampleType.PosControl:
                    return Color.Red;
                case SampleType.NegControl:
                    return Color.Blue;
                case SampleType.Sample:
                    return Color.Gray;
                default:
                    return Color.Black;
            }
        }

        public string schemaLabelText(SampleType sampleType)
        {
            switch (sampleType)
            {
                case SampleType.PosControl:
                    return "red \"+\"";
                case SampleType.NegControl:
                    return "blue \"\u2022\"";
                case SampleType.Sample:
                    return "blank tube";
                default:
                    return "";
            }
        }

        public void EnableControls(RGDControlState State)
        {
            if (State == RGDControlState.RunLong)
            {
                txt_Index.Enabled = false;
                txt_Tube1ID.Enabled = false;
                txt_Tube3ID.Enabled = false;
                btn_EditIndex.Enabled = false;
                btn_SaveID.Enabled = false;
            } 
            else
            {
                btn_EditIndex.Enabled = true;
                ResetIDTexts();
            }
        }

        internal static SchemaFlags FlagsFromState(RunSchemaState runSchemaState)
        {
            switch (runSchemaState)
            {
                case RunSchemaState.StartUp:
                    return SchemaFlags.StartUpSchema;
                case RunSchemaState.Regular:
                    return ~(SchemaFlags.StartUpSchema | SchemaFlags.EndofDaySchema);
                case RunSchemaState.ShutDown:
                    return SchemaFlags.EndofDaySchema;
                default:
                    return SchemaFlags.None;
            }
        }
        public bool IsSchemaStatePresent(RunSchemaState runSchemaState)
        {
            SchemaFlags flags = FlagsFromState(runSchemaState);
            if (!_rawSchemaList.ContainsSchemaFlags(flags)) return false;
            return true;
        }

        /// <summary>
        /// Attempts to set the Run Schema State. If no schema contains the state, then returns false
        /// </summary>
        /// <param name="runSchemaState">The wanted state</param>
        /// <returns>true if the operations works, false if it doesn't</returns>
        public bool SetSchemaState(RunSchemaState runSchemaState)
        {
            SchemaFlags flags = FlagsFromState(runSchemaState);
            if (!_rawSchemaList.ContainsSchemaFlags(flags)) return false;
            SchemaState = runSchemaState;
            SetRunSchema(_rawSchemaList.FilterBySchemaFlag(flags));
            return true;
        }

        public void LoadRawSchema(RunSchemaList rsl)
        {
            _rawSchemaList = rsl;
        }
        public void SetRunSchema(RunSchemaList rsl)
        {
            _schemaList = rsl;
            SetPaintingPanelDims();
            SetIndex(0);
        }

        public void SetPaintingPanelDims()
        {
            int bufferContainer = 5;
            if (TotalDrawHeight > panelPainter.Height + bufferContainer) // If scroll bar appears
            {
                panelPainter.Height = TotalDrawHeight;
                panelPainter.Width = panelContainer.Width - 2 * bufferContainer - SystemInformation.VerticalScrollBarWidth;
            }
        }

        public void IncrementSchema()
        {
            int nextIndex = SelectedIndex + 1;
            if (nextIndex >= _schemaList.Count && SchemaState == RunSchemaState.StartUp)
            {
                SetSchemaState(RunSchemaState.Regular);
                return;
            }
            SetIndex(nextIndex % _schemaList.Count);
        }

        private void btn_EditIndex_Click(object sender, EventArgs e)
        {
            btn_EditIndex.Enabled = false;
            btn_SaveID.Enabled = true;
            txt_Index.Enabled = true;
        }

        private void btn_SaveID_Click(object sender, EventArgs e)
        {
            int result;
            bool success = int.TryParse(txt_Index.Text, out result);
            if (!success || result > _schemaList.Count || result < 1)
            {
                MessageBox.Show($"Please only enter a number 1-{_schemaList.Count}");
                return;
            }
            btn_SaveID.Enabled = false;
            btn_EditIndex.Enabled = true;
            txt_Index.Enabled = false;
            ChangeIndex(result - 1);
        }
        public void ChangeIndex(int index)
        {
            SelectedIndex = index;
            ResetIDTexts();
            panelPainter.Invalidate();
            UpdateParent(true);
        }
        public void SetIndex(int index)
        {
            txt_Index.Text = (index + 1).ToString();
            ChangeIndex(index);
        }

        private void ResetIDTexts()
        {
            if (CurrentSchema.Tube1Type == SampleType.Sample)
            {
                txt_Tube1ID.Enabled = true;
                txt_Tube1ID.Text = (txt_Tube1ID.Text == "N/A" ? "" : txt_Tube1ID.Text);
            }
            else
            {
                txt_Tube1ID.Enabled = false;
                txt_Tube1ID.Text = "N/A";
            }
            if (CurrentSchema.Tube3Type == SampleType.Sample)
            {
                txt_Tube3ID.Enabled = true;
                txt_Tube3ID.Text = (txt_Tube3ID.Text == "N/A" ? "" : txt_Tube3ID.Text);
            }
            else
            {
                txt_Tube3ID.Enabled = false;
                txt_Tube3ID.Text = "N/A";
            }
        }

        public void UpdateParent(bool setStore)
        {
            var parent = (RGD_FormMain)this.ParentForm;
            if (parent == null) return;
            txt_MetadataTube1.Text = Tube1Text();
            txt_MetadataTube3.Text = Tube3Text();
            if (setStore) parent.Controller.Store.SchemaIndex = SelectedIndex;
            parent.Controller.Tubes[1].TubeSampleType = CurrentSchema.Tube1Type;
            parent.Controller.Tubes[3].TubeSampleType = CurrentSchema.Tube3Type;
            parent.lbl_Instructions.Text = $"Place a {schemaLabelText(CurrentSchema.Tube1Type)} in slot 1 and a {schemaLabelText(CurrentSchema.Tube3Type)} in slot 3";
            // Yes this was a sloppy fix because of the way that the store is structured (reflections are evil)
            parent.Controller.Store.Metadata_Tube1 = txt_MetadataTube1.Text;
            parent.Controller.Store.Metadata_Tube3 = txt_MetadataTube3.Text;
        }

        public string Tube1Text()
        {
            return RunSchema.SampleAsString(CurrentSchema.Tube1Type, txt_Tube1ID.Text);
        }

        public string Tube3Text()
        {
            return RunSchema.SampleAsString(CurrentSchema.Tube3Type, txt_Tube3ID.Text);
        }

        public bool IsFilledOut()
        {
            if (CurrentSchema.Tube1Type == SampleType.Sample && txt_Tube1ID.Text.Trim() == "")
            {
                return false;
            }
            if (CurrentSchema.Tube3Type == SampleType.Sample && txt_Tube3ID.Text.Trim() == "")
            {
                return false;
            }
            return true;
        }

        private void txt_TubeID_TextChanged(object sender, EventArgs e)
        {
            UpdateParent(false);
        }
    }
}
