﻿using RGD_Library;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RGD_Control
{
    public partial class ModelCheck : Form
    {
        public Dictionary<string, ResultsClass> results;
        public MainController Controller;

        internal Dictionary<string, TextBox> InfoTexts { 
            get
            {
                return new Dictionary<string, TextBox>()
                {
                    {"X50", txt_X50 },
                    {"RiseTime", txt_RiseTime },
                    {"Plateau", txt_Plateau },
                    {"Baseline", txt_Baseline },
                    {"Interpretation", txt_Interpretation }
                };
            } 
        }
        public ModelCheck(MainController controller)
        {
            Controller = controller;
            InitializeComponent();
        }

        private void btn_ChooseFile_Click(object sender, EventArgs e)
        {
            if (openRunFile.ShowDialog() == DialogResult.OK && ( openRunFile.FileName != null))
            {
                string fileName = openRunFile.FileName;
                results = Controller.LoadFromRaw(fileName, OutputVersion.V1).ToDictionary(r => r.InformationDict["TubeID"].ToString() + ":" + r.InformationDict["ChannelID"].ToString(), r => r);
            }
            lb_Curves.Items.Clear();
            foreach (string key in results.Keys) lb_Curves.Items.Add(key);
            foreach (TextBox box in InfoTexts.Values) box.Text = "";
        }

        private void lb_Curves_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (string key in InfoTexts.Keys)
            {
                InfoTexts[key].Text = results[lb_Curves.SelectedItem.ToString()].InformationDict[key].ToString();
            }
        }
    }
}
