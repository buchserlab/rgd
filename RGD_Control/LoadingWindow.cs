﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RGD_Control
{
    public partial class LoadingWindow : Form
    {
        public LoadingWindow()
        {
            InitializeComponent();
        }

        public void ClearMessages()
        {
            detailsbox.Text = "";
        }

        public void AddMessage(string message, bool newline = true)
        {
            detailsbox.Text += message + (newline ? Environment.NewLine: "");
        }
    }
}
