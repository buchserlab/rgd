﻿namespace RGD_Control
{
    partial class ModelCheck
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openRunFile = new System.Windows.Forms.OpenFileDialog();
            this.btn_ChooseFile = new System.Windows.Forms.Button();
            this.lb_Curves = new System.Windows.Forms.ListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbl_Interpretation = new System.Windows.Forms.Label();
            this.lbl_RiseTime = new System.Windows.Forms.Label();
            this.lbl_Baseline = new System.Windows.Forms.Label();
            this.lbl_Plateau = new System.Windows.Forms.Label();
            this.lbl_X50 = new System.Windows.Forms.Label();
            this.txt_Interpretation = new System.Windows.Forms.TextBox();
            this.txt_RiseTime = new System.Windows.Forms.TextBox();
            this.txt_Baseline = new System.Windows.Forms.TextBox();
            this.txt_Plateau = new System.Windows.Forms.TextBox();
            this.txt_X50 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openRunFile
            // 
            this.openRunFile.FileName = "openRunFile";
            // 
            // btn_ChooseFile
            // 
            this.btn_ChooseFile.Location = new System.Drawing.Point(65, 61);
            this.btn_ChooseFile.Name = "btn_ChooseFile";
            this.btn_ChooseFile.Size = new System.Drawing.Size(75, 23);
            this.btn_ChooseFile.TabIndex = 0;
            this.btn_ChooseFile.Text = "Select File";
            this.btn_ChooseFile.UseVisualStyleBackColor = true;
            this.btn_ChooseFile.Click += new System.EventHandler(this.btn_ChooseFile_Click);
            // 
            // lb_Curves
            // 
            this.lb_Curves.FormattingEnabled = true;
            this.lb_Curves.ItemHeight = 15;
            this.lb_Curves.Location = new System.Drawing.Point(65, 99);
            this.lb_Curves.Name = "lb_Curves";
            this.lb_Curves.Size = new System.Drawing.Size(226, 169);
            this.lb_Curves.TabIndex = 1;
            this.lb_Curves.SelectedIndexChanged += new System.EventHandler(this.lb_Curves_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbl_Interpretation);
            this.panel1.Controls.Add(this.lbl_RiseTime);
            this.panel1.Controls.Add(this.lbl_Baseline);
            this.panel1.Controls.Add(this.lbl_Plateau);
            this.panel1.Controls.Add(this.lbl_X50);
            this.panel1.Controls.Add(this.txt_Interpretation);
            this.panel1.Controls.Add(this.txt_RiseTime);
            this.panel1.Controls.Add(this.txt_Baseline);
            this.panel1.Controls.Add(this.txt_Plateau);
            this.panel1.Controls.Add(this.txt_X50);
            this.panel1.Location = new System.Drawing.Point(369, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(353, 376);
            this.panel1.TabIndex = 2;
            // 
            // lbl_Interpretation
            // 
            this.lbl_Interpretation.AutoSize = true;
            this.lbl_Interpretation.Location = new System.Drawing.Point(43, 267);
            this.lbl_Interpretation.Name = "lbl_Interpretation";
            this.lbl_Interpretation.Size = new System.Drawing.Size(79, 15);
            this.lbl_Interpretation.TabIndex = 1;
            this.lbl_Interpretation.Text = "Interpretation";
            // 
            // lbl_RiseTime
            // 
            this.lbl_RiseTime.AutoSize = true;
            this.lbl_RiseTime.Location = new System.Drawing.Point(43, 196);
            this.lbl_RiseTime.Name = "lbl_RiseTime";
            this.lbl_RiseTime.Size = new System.Drawing.Size(52, 15);
            this.lbl_RiseTime.TabIndex = 1;
            this.lbl_RiseTime.Text = "Risetime";
            // 
            // lbl_Baseline
            // 
            this.lbl_Baseline.AutoSize = true;
            this.lbl_Baseline.Location = new System.Drawing.Point(43, 131);
            this.lbl_Baseline.Name = "lbl_Baseline";
            this.lbl_Baseline.Size = new System.Drawing.Size(50, 15);
            this.lbl_Baseline.TabIndex = 1;
            this.lbl_Baseline.Text = "Baseline";
            // 
            // lbl_Plateau
            // 
            this.lbl_Plateau.AutoSize = true;
            this.lbl_Plateau.Location = new System.Drawing.Point(43, 64);
            this.lbl_Plateau.Name = "lbl_Plateau";
            this.lbl_Plateau.Size = new System.Drawing.Size(46, 15);
            this.lbl_Plateau.TabIndex = 1;
            this.lbl_Plateau.Text = "Plateau";
            // 
            // lbl_X50
            // 
            this.lbl_X50.AutoSize = true;
            this.lbl_X50.Location = new System.Drawing.Point(43, 8);
            this.lbl_X50.Name = "lbl_X50";
            this.lbl_X50.Size = new System.Drawing.Size(26, 15);
            this.lbl_X50.TabIndex = 1;
            this.lbl_X50.Text = "X50";
            // 
            // txt_Interpretation
            // 
            this.txt_Interpretation.Location = new System.Drawing.Point(43, 294);
            this.txt_Interpretation.Name = "txt_Interpretation";
            this.txt_Interpretation.ReadOnly = true;
            this.txt_Interpretation.Size = new System.Drawing.Size(100, 23);
            this.txt_Interpretation.TabIndex = 0;
            // 
            // txt_RiseTime
            // 
            this.txt_RiseTime.Location = new System.Drawing.Point(43, 226);
            this.txt_RiseTime.Name = "txt_RiseTime";
            this.txt_RiseTime.ReadOnly = true;
            this.txt_RiseTime.Size = new System.Drawing.Size(100, 23);
            this.txt_RiseTime.TabIndex = 0;
            // 
            // txt_Baseline
            // 
            this.txt_Baseline.Location = new System.Drawing.Point(43, 158);
            this.txt_Baseline.Name = "txt_Baseline";
            this.txt_Baseline.ReadOnly = true;
            this.txt_Baseline.Size = new System.Drawing.Size(100, 23);
            this.txt_Baseline.TabIndex = 0;
            // 
            // txt_Plateau
            // 
            this.txt_Plateau.Location = new System.Drawing.Point(43, 89);
            this.txt_Plateau.Name = "txt_Plateau";
            this.txt_Plateau.ReadOnly = true;
            this.txt_Plateau.Size = new System.Drawing.Size(100, 23);
            this.txt_Plateau.TabIndex = 0;
            // 
            // txt_X50
            // 
            this.txt_X50.Location = new System.Drawing.Point(43, 26);
            this.txt_X50.Name = "txt_X50";
            this.txt_X50.ReadOnly = true;
            this.txt_X50.Size = new System.Drawing.Size(100, 23);
            this.txt_X50.TabIndex = 0;
            // 
            // ModelCheck
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lb_Curves);
            this.Controls.Add(this.btn_ChooseFile);
            this.Name = "ModelCheck";
            this.Text = "ModelCheck";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openRunFile;
        private System.Windows.Forms.Button btn_ChooseFile;
        private System.Windows.Forms.ListBox lb_Curves;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbl_Interpretation;
        private System.Windows.Forms.Label lbl_RiseTime;
        private System.Windows.Forms.Label lbl_Baseline;
        private System.Windows.Forms.Label lbl_Plateau;
        private System.Windows.Forms.Label lbl_X50;
        private System.Windows.Forms.TextBox txt_Interpretation;
        private System.Windows.Forms.TextBox txt_RiseTime;
        private System.Windows.Forms.TextBox txt_Baseline;
        private System.Windows.Forms.TextBox txt_Plateau;
        private System.Windows.Forms.TextBox txt_X50;
    }
}