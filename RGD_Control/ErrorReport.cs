﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;

namespace RGD_Control
{
    class ErrorReport
    {
        public static bool SendErrorEmail(string subject, string message)
        {
            return false;
            // If you want to send an email error, remove the above return comment and fill in the stuff below
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(""); // Fill in with client

                mail.From = new MailAddress(""); // From Address
                mail.To.Add(""); // To Address
                mail.Subject = subject;
                mail.Body = message;

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential(mail.From.Address, "password"); // Replace with email password
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
