﻿namespace RGD_Control
{
    partial class RunSchemaPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_Index = new System.Windows.Forms.TextBox();
            this.panelContainer = new System.Windows.Forms.Panel();
            this.panelPainter = new System.Windows.Forms.Panel();
            this.txt_Tube1ID = new System.Windows.Forms.TextBox();
            this.txt_Tube3ID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_EditIndex = new System.Windows.Forms.Button();
            this.btn_SaveID = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_MetadataTube3 = new System.Windows.Forms.TextBox();
            this.txt_MetadataTube1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panelContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // txt_Index
            // 
            this.txt_Index.Enabled = false;
            this.txt_Index.Location = new System.Drawing.Point(174, 232);
            this.txt_Index.Name = "txt_Index";
            this.txt_Index.Size = new System.Drawing.Size(33, 23);
            this.txt_Index.TabIndex = 0;
            // 
            // panelContainer
            // 
            this.panelContainer.AutoScroll = true;
            this.panelContainer.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panelContainer.Controls.Add(this.panelPainter);
            this.panelContainer.Location = new System.Drawing.Point(129, 59);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(126, 146);
            this.panelContainer.TabIndex = 1;
            // 
            // panelPainter
            // 
            this.panelPainter.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelPainter.Location = new System.Drawing.Point(5, 5);
            this.panelPainter.Name = "panelPainter";
            this.panelPainter.Size = new System.Drawing.Size(116, 136);
            this.panelPainter.TabIndex = 0;
            this.panelPainter.Paint += new System.Windows.Forms.PaintEventHandler(this.panelPaint_Draw);
            // 
            // txt_Tube1ID
            // 
            this.txt_Tube1ID.Enabled = false;
            this.txt_Tube1ID.Location = new System.Drawing.Point(35, 98);
            this.txt_Tube1ID.Name = "txt_Tube1ID";
            this.txt_Tube1ID.Size = new System.Drawing.Size(46, 23);
            this.txt_Tube1ID.TabIndex = 2;
            this.txt_Tube1ID.TextChanged += new System.EventHandler(this.txt_TubeID_TextChanged);
            // 
            // txt_Tube3ID
            // 
            this.txt_Tube3ID.Enabled = false;
            this.txt_Tube3ID.Location = new System.Drawing.Point(300, 98);
            this.txt_Tube3ID.Name = "txt_Tube3ID";
            this.txt_Tube3ID.Size = new System.Drawing.Size(46, 23);
            this.txt_Tube3ID.TabIndex = 3;
            this.txt_Tube3ID.TextChanged += new System.EventHandler(this.txt_TubeID_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label1.Location = new System.Drawing.Point(150, 214);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "Current Index";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Tube 1 Sample ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(273, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tube 3 Sample ID";
            // 
            // btn_EditIndex
            // 
            this.btn_EditIndex.Location = new System.Drawing.Point(145, 257);
            this.btn_EditIndex.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_EditIndex.Name = "btn_EditIndex";
            this.btn_EditIndex.Size = new System.Drawing.Size(41, 22);
            this.btn_EditIndex.TabIndex = 7;
            this.btn_EditIndex.Text = "Edit";
            this.btn_EditIndex.UseVisualStyleBackColor = true;
            this.btn_EditIndex.Visible = false;
            this.btn_EditIndex.Click += new System.EventHandler(this.btn_EditIndex_Click);
            // 
            // btn_SaveID
            // 
            this.btn_SaveID.Enabled = false;
            this.btn_SaveID.Location = new System.Drawing.Point(192, 257);
            this.btn_SaveID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_SaveID.Name = "btn_SaveID";
            this.btn_SaveID.Size = new System.Drawing.Size(49, 22);
            this.btn_SaveID.TabIndex = 8;
            this.btn_SaveID.Text = "Save";
            this.btn_SaveID.UseVisualStyleBackColor = true;
            this.btn_SaveID.Visible = false;
            this.btn_SaveID.Click += new System.EventHandler(this.btn_SaveID_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.label4.Location = new System.Drawing.Point(240, 230);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "The index increments";
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label5.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.label5.Location = new System.Drawing.Point(240, 242);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(122, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "after each run, so only";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.label6.Location = new System.Drawing.Point(251, 255);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "edit if necessary";
            this.label6.Visible = false;
            // 
            // txt_MetadataTube3
            // 
            this.txt_MetadataTube3.Enabled = false;
            this.txt_MetadataTube3.Location = new System.Drawing.Point(273, 182);
            this.txt_MetadataTube3.Name = "txt_MetadataTube3";
            this.txt_MetadataTube3.Size = new System.Drawing.Size(98, 23);
            this.txt_MetadataTube3.TabIndex = 12;
            // 
            // txt_MetadataTube1
            // 
            this.txt_MetadataTube1.Enabled = false;
            this.txt_MetadataTube1.Location = new System.Drawing.Point(11, 182);
            this.txt_MetadataTube1.Name = "txt_MetadataTube1";
            this.txt_MetadataTube1.Size = new System.Drawing.Size(98, 23);
            this.txt_MetadataTube1.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label7.Location = new System.Drawing.Point(39, 166);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 48;
            this.label7.Text = "Tube 1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label8.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label8.Location = new System.Drawing.Point(304, 166);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 50;
            this.label8.Text = "Tube 3";
            // 
            // RunSchemaPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txt_MetadataTube1);
            this.Controls.Add(this.txt_MetadataTube3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btn_SaveID);
            this.Controls.Add(this.btn_EditIndex);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_Tube3ID);
            this.Controls.Add(this.txt_Tube1ID);
            this.Controls.Add(this.panelContainer);
            this.Controls.Add(this.txt_Index);
            this.Name = "RunSchemaPanel";
            this.Size = new System.Drawing.Size(406, 304);
            this.panelContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_Index;
        private System.Windows.Forms.Panel panelContainer;
        private System.Windows.Forms.Panel panelPainter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_EditIndex;
        private System.Windows.Forms.Button btn_SaveID;
        public System.Windows.Forms.TextBox txt_Tube1ID;
        public System.Windows.Forms.TextBox txt_Tube3ID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txt_MetadataTube3;
        public System.Windows.Forms.TextBox txt_MetadataTube1;
    }
}
