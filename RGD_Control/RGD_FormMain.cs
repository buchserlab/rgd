﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.DirectoryServices.ActiveDirectory;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using RGD_Control.Properties;
using RGD_Library;

namespace RGD_Control
{
    public partial class RGD_FormMain : Form
    {
        public CurveViewer Curves;
        public Metadata_Table MetadataTable;
        public ResultsSet_Class ResultsSet;
        public MainController Controller;
        public RGDFlags Flags;
        public List<ResultsClass> Result_Latest => ResultsSet.Result_Latest;
        public Form Timing_Explain_Form;
        public Form OnScreen_Light_Test;
        public SettingsForm Settings_Form;
        public LoadingWindow Loading_Window;
        public Form_Finished_Run FinishedRun_Form;
        public System.Windows.Forms.Timer Timer_Tests;
        string[] PortsPresent;
        string PortinUse;
        public System.Windows.Forms.Timer Port_Listener;
        private const double QuickSpin_Fract_Default = 0.6;
        public FileVersionInfo FileVerInfo;
        public TextBox Metadata_Tube1 => panel_RunSchema.txt_MetadataTube1;
        public TextBox Metadata_Tube3 => panel_RunSchema.txt_MetadataTube3;
        public string FormTitle => "RGD Reader Control " + FileVerInfo.FileVersion + Controller.ExportTitle;

        public RunSchemaState SchemaState => panel_RunSchema.SchemaState;
        private IEnumerable<Control> _AdvancedElements
        {
            get
            {
                return new List<Control>()
                {
                    panel_Params, panel_TempHeating,
                    btn_Diagnostics, btn_Clear, btn_SaveSettings,
                    lbl_SelectDevice, lbl_SvImage,
                    DisplayLogarithmic
                };
            }
        }

        public RGD_FormMain()
        {
            //this.Font = new Font(Font.Name, 8.25f * 96f / CreateGraphics().DpiX, Font.Style, Font.Unit, Font.GdiCharSet, Font.GdiVerticalFont);
            //this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            InitializeComponent();

            // The passing of flags here allows access to some signals to the main form
            // otherwise you have to do a circular reference
            Flags = new RGDFlags();
            Controller = new MainController(Flags);
            panel_RunSchema.LoadRawSchema(Controller.Schema);
            //panel_RunSchema.SetIndex(Controller.Store.SchemaIndex);
            bool doStartRuns = panel_RunSchema.SetSchemaState(RunSchemaState.StartUp);
            if (!doStartRuns) panel_RunSchema.SetSchemaState(RunSchemaState.Regular);

            Loading_Window = new LoadingWindow();
            Loading_Window.Show();
            Loading_Window.Refresh();

            FileVerInfo = FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location);
            this.Text = FormTitle;

            Loading_Window.AddMessage("Searching for settings file...", false);
            Settings_Form = new SettingsForm(this);
            if (!Flags.GoodPath) MessageBox.Show("Could not connect to remote folder. Redirecting to documents folder");
            Loading_Window.AddMessage("Success!");

            Loading_Window.AddMessage("Setting up visuals...", false);
            //Fix the splitter distance
            this.Height = Controller.Store.Form_Height;
            splitContainer1.SplitterDistance = Controller.Store.SplitterDistance;
            FinishedRun_Form = new Form_Finished_Run(this); //Needs the store loaded first

            //Setup the Curves
            //Curves = new CurveMaster();
            Curves = new CurveViewer();
            splitContainer1.Panel1.Controls.Add(Curves);
            Curves.Dock = System.Windows.Forms.DockStyle.Fill;
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);

            //Fill out the settings
            LoadFromStore();
            SwitchMode(false);

            Timer_Tests = new System.Windows.Forms.Timer();
            timer_Update.Interval = 250;
            Enable_Controls(RGDControlState.NotQueried);

            //Setup the Metadata Table
            MetadataTable = new Metadata_Table(Controller.Store);
            Loading_Window.AddMessage("Success!");

            Loading_Window.AddMessage("Searching for devices...", false);
            //Look for devices and connect one
            PortsPresent = SerialPort.GetPortNames();
            PortinUse = null;
            Scan_then_Connect();

            //Setup the port listener
            Port_Listener = new System.Windows.Forms.Timer();
            Port_Listener.Interval = 500;
            Port_Listener.Tick += CheckPorts_Tick;
            Port_Listener.Start();

            Loading_Window.AddMessage("Success!");
            Loading_Window.Hide();
            Loading_Window.ClearMessages();
            //Put the cursor to focus on metadata
            User.Focus();
            ResultsSet = new ResultsSet_Class();
            if (doStartRuns) txBx_Update.Text = "Please do the necessary start-up run(s) as shown.";
        }


        #region Port Scanning -------------------------------------------------------------------------------------------------------------------------------------------------------
        private void Scan_then_Connect()
        {
            Scan_Devices();
            if (comboBox_Devices.Items.Count == 1)
            {
                comboBox_Devices.SelectedIndex = 0;
                Query();
                QuickSpin();
                Connect();
            }
        }

        private void CheckPorts_Tick(object sender, EventArgs e)
        {
            string[] Ports = SerialPort.GetPortNames();
            if (PortsPresent.SequenceEqual(Ports)) return;
            Port_Listener.Stop();
            if (Ports.Length > PortsPresent.Length)
            {
                if (CurrentState == RGDControlState.RunLong) return;
                PortsPresent = Ports;
                if (CurrentState != RGDControlState.NotQueried && PortinUse != null)
                {
                    Scan_Devices();
                }
                else
                {
                    Scan_then_Connect();
                }
            }
            else
            {
                if (PortinUse == null || Ports.Contains(PortinUse))
                {
                    if (CurrentState == RGDControlState.RunLong) return;
                    PortsPresent = Ports;
                    Scan_Devices();
                }
                else
                {
                    if (Timer_Tests.Enabled)
                    {
                        Timer_Tests.Stop();
                        DialogResult deg = MessageBox.Show("Please plug back in soon, or the test will fail\r\nPress OK if you have plugged it back in, or press cancel to stop the run", "Unplugged", MessageBoxButtons.OKCancel);
                        if (deg == DialogResult.Cancel)
                        {
                            Stop();
                            Clear();
                            PortsPresent = Ports;
                            Scan_Devices();
                            txBx_Update.Text += "Device was disconnected";
                        }
                        else
                        {
                            Timer_Tests.Start();
                        }
                    }
                    else
                    {
                        Stop();
                        Clear();
                        PortsPresent = Ports;
                        Scan_Devices();
                        txBx_Update.Text += "Device was disconnected";

                    }
                }
            }
            Port_Listener.Start();
            return;
        }

        public void Scan_Devices()
        {
            //Iterate through the available com ports and try and do a pre-query to get the ID from it
            List<string> Devices = new List<string>();
            string tRet;
            comboBox_Devices.Items.Clear();
            foreach (string Port in PortsPresent)
            {
                tRet = Controller.GetDeviceInfo(Port);
                if (tRet != "-1")
                {
                    Devices.Add(tRet);
                    comboBox_Devices.Items.Add(tRet);
                }
            }
        }

        private void label_ReScanDevices(object sender, EventArgs e)
        {
            Scan_Devices();
        }

        #endregion

        #region Connect and Start ------------------------------------------------------------------------------------------------------------------------------------------------------

        private void btn_Query_Click(object sender = null, EventArgs e = null)
        {
            Query();
        }

        private void Query()
        {
            //Setup the device connection, etc
            txBx_Update.Text = "Querying . . "; Application.DoEvents();
            SaveSettings();
            if (Controller.Store.COMPort.Trim() == "") return;
            PortinUse = Controller.Store.COMPort.Trim();
            Controller.ConnectToDevice(Controller.Store.COMPort);
            Controller.Channels_SetupForBasic();
            panel_RunSchema.UpdateParent(false);
            Curves.Channels = Controller.GetChannels();
            Curves.Channels_LabelOnly = new List<PlottableChannel>(2) { Controller.tempHeater, Controller.tempHeater2 };

            //Pull some settings from the device
            string msg = Controller.QueryDevice();
            if (Controller.GetDeviceID >= 0)
            {
                //Worked!
                Enable_Controls(RGDControlState.Queried);
                txBx_Update.Text = "Found. Press CONNECT when ready." + msg;
                FillOutSettings_FromDevice();
            }
            else
            {
                Enable_Controls(RGDControlState.NotQueried);
                txBx_Update.Text = "Trouble Querying on this Port.\r\n" + msg;
                return;
            }
        }

        private void FillOutSettings_FromDevice()
        {
            txBx_DeviceID.Text = Controller.Device.deviceID.ToString();
            txBx_calSlope.Text = Controller.tempHeater.calSlope.ToString();
            txBx_CalIntercept.Text = Controller.tempHeater.calIntercept.ToString();
            txBx_Firmware.Text = Controller.Device.Firmware_Version.ToString();

            Settings_Form.UpdateSettingsFromDevice(Controller);
        }

        private void btn_Clear_Click(object sender = null, EventArgs e = null)
        {
            Clear();
        }

        private void Clear()
        {
            Curves.Channels = new List<PlottableChannel>();
            Controller.Device = null;
            Thread.Sleep(250);
            Enable_Controls(RGDControlState.NotQueried);
            PortinUse = null;
        }

        public RGDControlState CurrentState;


        public enum RGDBuzzerSounds
        {
            Connected,
            RunLong_Start,
            RunLong_Halfway,
            RunLong_Finished
        }

        public enum RGDLightStates
        {
            Default,
            Connected,
            RunLong_Start,
            RunLong_Halfway,
            RunLong_Finished
        }

        public void Enable_Controls(RGDControlState State)
        {
            CurrentState = State;
            label_Running.Visible = false;

            btn_Query.Enabled = (State == RGDControlState.NotQueried);
            btn_Clear.Enabled = btn_Connect.Enabled = (State == RGDControlState.Queried);
            btn_Stop.Enabled = (State == RGDControlState.RunLong);
            lbl_SelectDevice.Enabled = (State == RGDControlState.Queried || State == RGDControlState.NotQueried);

            btn_SendEEPROM.Enabled = btn_Timing.Enabled = chkBox_Heater.Enabled = btn_Run_30min.Enabled = btn_Diagnostics.Enabled =
                Settings_Form.btn_HeatTest_1.Enabled = Settings_Form.btn_FluorTest_T1Blank.Enabled = Settings_Form.btn_FluorTest_T3Blank.Enabled =
                lbl_Timing_Quick.Enabled = label_More_EEPROM.Enabled = Timing_Defaults.Enabled =
                btn_TCal_Hi.Enabled = btn_TCal_Low.Enabled = btn_PreHeat.Enabled =
                (State == RGDControlState.Standby);

            User.Enabled = Subject.Enabled = /*Metadata_Tube1.Enabled = Metadata_Tube2.Enabled = Metadata_Tube3.Enabled = */BaseFolder.Enabled = (State != RGDControlState.RunLong);

            comboBox_Devices.Enabled = (State == RGDControlState.Queried || State == RGDControlState.NotQueried);

            if (State == RGDControlState.NotQueried)
            {
                txBx_calSlope.Text = txBx_DeviceID.Text = txBx_Firmware.Text = txBx_CalIntercept.Text = "";
            }

            panel_RunSchema.EnableControls(State);
        }

        private void btn_Connect_Click(object sender = null, EventArgs e = null)
        {
            Connect();
        }

        private void Connect(bool clearUpdates = true)
        {
            //Fire it up to "Standby"
            Enable_Controls(RGDControlState.Standby);

            if (clearUpdates) txBx_Update.Text = "";
            Controller.Channels_AdjustScaling(true);

            Controller.StartDevice();
            QuickSpin(1.1);
            Timing_Defaults_Quick();
            QuickSpin(2.1);
            Status_Buzzer(RGDBuzzerSounds.Connected);
            QuickSpin();
            Status_Lights(RGDLightStates.Connected);

            timer_Update.Start();
            this.Text = FormTitle; //Put the Device ID name up top
        }

        private void btn_Stop_Click(object sender = null, EventArgs e = null)
        {
            Stop();
        }
        private void Stop()
        {
            if (!Confirm_Stop()) { return; }
            if (CurrentState == RGDControlState.RunLong)
            {
                Run_Long_EndRun();
            }
            Controller.Device.Instruct(Packet_Instruct.SendStatusLights(0, 0, 0));
            Controller.StopDevice();
            Connect(false);
        }

        #endregion

        #region Easy Advanced ------------------------------------------------------------

        private void lbl_AdvancedEasy_Click(object sender, EventArgs e)
        {
            if (panel_Params.Visible)
            {
                SwitchMode(true);
            }
            else
            {
                SwitchMode(false);
            }
        }

        public void SwitchMode(bool EasyMode)
        {
            foreach (Control c in _AdvancedElements) c.Visible = !EasyMode;
        }


        #endregion

        #region Heating and Temperature -----------------------------------------------------------------------------------------------------------------------------------------------------------

        private void btn_PreHeat_Click(object sender, EventArgs e)
        {
            //When the calibrated center position is 62, the side tubes are usually 60. 60 is the temp we want for the side tubes, so starting around 7/13 we will calibrate the in-tube thermister to report on the side tube reading
            Temperature_Ideal.Text = "60";
            Heater_ON();
            txBx_Update.Text = "Heating . . ";
        }
        public void Heater_ON() { chkBox_Heater.Checked = true; }

        public void Heater_Off() { chkBox_Heater.Checked = false; }

        private void chkBox_Heater_CheckedChanged(object sender, EventArgs e)
        {
            UpdateHeater();
        }

        private void txBx_TCal_Low_Click(object sender, EventArgs e)
        {
            CalTempSet(true);
        }

        private void btn_TCal_Hi_Click(object sender, EventArgs e)
        {
            CalTempSet(false);
        }
        public double TCal_Low_Raw = -1;
        public double TCal_High_Raw = -1;
        public void CalTempSet(bool Low)
        {
            double tRaw = Controller.tempHeater.Temperature_Raw;
            Heater_Off(); //Keep the heater off for the tests
            if (Low)
                TCal_Low_Raw = tRaw;
            else
                TCal_High_Raw = tRaw;
            if (TCal_Low_Raw >= 0 && TCal_High_Raw >= 0)
            {
                Controller.tempHeater.Calibrate(TCal_Low_Raw, TCal_High_Raw);
                txBx_calSlope.Text = Controller.tempHeater.calSlope.ToString();
                txBx_CalIntercept.Text = Controller.tempHeater.calIntercept.ToString();
                txBx_calSlope.ForeColor = txBx_CalIntercept.ForeColor = Color.Red;
                txBx_Update.Text = "Click PUSH to store this in the device.";
            }
            txBx_Update.Text += (Low ? "Low=" : "Hi=") + tRaw + " ";
        }

        public void UpdateHeater()
        {
            SaveSettings();
            SendInstructions(Packet_Instruct.SendTemp(chkBox_Heater.Checked, Controller.Store));
        }

        private void Temperature_Ideal_TextChanged(object sender, EventArgs e)
        {
            if (CurrentlyPopulating_DontUpdate) return;
            double t = 0;
            double.TryParse(Temperature_Ideal.Text, out t);
            if (t > 10 && t < 80)
            {
                UpdateHeater();
            }
        }

        private void Temperature_Tolerance_TextChanged(object sender, EventArgs e)
        {
            if (CurrentlyPopulating_DontUpdate) return;
            double t = 0;
            double.TryParse(Temperature_Tolerance.Text, out t);
            if (t > 0.05 && t < 10)
            {
                UpdateHeater();
            }
        }


        #endregion

        #region Timing and Lighting  ------------------------------------------------------------------------------------------------------------------------------------------------------
        private void UpdateLightTiming_Click(object sender = null, EventArgs e = null)
        {
            SaveSettings();
            SendInstructions(Packet_Instruct.SendTiming(Controller.Store));
            Timing_Cycle.ForeColor = Timing_Light.ForeColor = Timing_Delay.ForeColor = Timing_Sensor.ForeColor = Timing_Send.ForeColor = Timing_Delta.ForeColor = Color.Black;

            //Change the update time to match what is recorded here . . 
            timer_Update.Stop();
            Controller.Channels_SetupForBasic();
            timer_Update.Interval = Math.Min(Controller.Store.Timing_Send, (short)500); //Don't let the time go above 500
            timer_Update.Start();
        }

        public void Timing_Defaults_LongRun_DontPush()
        {
            Timing_Cycle.Text = "2500";
            Timing_Light.Text = "500";
            Timing_Delay.Text = "200";
            Timing_Sensor.Text = "50";
            Timing_Send.Text = Timing_Cycle.Text; //Same as std except this
            Timing_Delta.Text = "50";
            AllowLights.Checked = true;

            Application.DoEvents();
        }

        public void Timing_Defaults_Std(object sender = null, EventArgs e = null)
        {
            Timing_Cycle.Text = "2500";
            Timing_Light.Text = "500";
            Timing_Delay.Text = "200";
            Timing_Sensor.Text = "50";
            Timing_Send.Text = "250";
            Timing_Delta.Text = "50";
            AllowLights.Checked = true;

            UpdateLightTiming_Click();
        }

        public void Timing_Defaults_Quick(object sender = null, EventArgs e = null)
        {
            Timing_Cycle.Text = "250";
            Timing_Light.Text = "50";
            Timing_Delay.Text = "0";
            Timing_Sensor.Text = "50";
            Timing_Send.Text = "50";
            Timing_Delta.Text = "50";
            AllowLights.Checked = false;

            UpdateLightTiming_Click();
        }

        private void Timing_Defaults_Diagnostic(bool LightsOn)
        {
            Timing_Cycle.Text = "800";
            Timing_Light.Text = "200";
            Timing_Delay.Text = "100";
            Timing_Sensor.Text = "50";
            Timing_Send.Text = "800";
            Timing_Delta.Text = "50";
            AllowLights.Checked = LightsOn;

            UpdateLightTiming_Click();
        }

        private void ChangeTimingText(object sender, EventArgs e)
        {
            TextBox TB = (TextBox)sender;
            TB.ForeColor = Color.Red;
        }

        #endregion

        #region Settings ---------------------------------------------------------------------------------------------------------------------------------------------------------------

        private void btn_SaveSettings_Click(object sender = null, EventArgs e = null)
        {
            SaveSettings();
        }
        private void SaveSettings()
        {
            UpdateStore();
            Controller.Store.Save();
        }

        public bool MetadataFilled
        {
            get
            {
                bool Check(TextBox TB)
                {
                    bool Empty = (TB.Text.Trim() == "");
                    if (Empty)
                    {
                        TB.BackColor = Color.LightPink;
                        return true;
                    }
                    else
                    {
                        TB.BackColor = Color.White;
                        return false;
                    }
                }
                foreach (var box in new List<TextBox>() { User, Metadata_Tube1, Metadata_Tube2, Metadata_Tube3, panel_RunSchema.txt_Tube1ID, panel_RunSchema.txt_Tube3ID })
                {
                    if (Check(box)) return false;
                }
                return true;
            }
        }

        Dictionary<string, TextBox> GetRelTextBoxes()
        {
            Dictionary<string, TextBox> TxBxs = splitContainer1.Panel2.Controls.OfType<TextBox>().ToDictionary(k => k.Name.ToUpper(), v => v);
            Dictionary<string, TextBox> TxBxs2 = panel_Params.Controls.OfType<TextBox>().ToDictionary(k => k.Name.ToUpper(), v => v);
            Dictionary<string, TextBox> TxBxs3 = panel_TempHeating.Controls.OfType<TextBox>().ToDictionary(k => k.Name.ToUpper(), v => v);

            Dictionary<string, TextBox> TxBxs4 = tabPage1.Controls.OfType<TextBox>().ToDictionary(k => k.Name.ToUpper(), v => v);
            Dictionary<string, TextBox> TxBxs5 = tabPage2.Controls.OfType<TextBox>().ToDictionary(k => k.Name.ToUpper(), v => v);
            Dictionary<string, TextBox> TxBxs6 = tabPage3.Controls.OfType<TextBox>().ToDictionary(k => k.Name.ToUpper(), v => v);
            TxBxs = TxBxs.Concat(TxBxs2).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            TxBxs = TxBxs.Concat(TxBxs3).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            TxBxs = TxBxs.Concat(TxBxs4).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            TxBxs = TxBxs.Concat(TxBxs5).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            TxBxs = TxBxs.Concat(TxBxs6).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            return TxBxs;
        }
        private Dictionary<string, CheckBox> GetRelCheckBoxes()
        {
            Dictionary<string, CheckBox> c1 = splitContainer1.Panel2.Controls.OfType<CheckBox>().ToDictionary(k => k.Name.ToUpper(), v => v);
            Dictionary<string, CheckBox> c2 = panel_Params.Controls.OfType<CheckBox>().ToDictionary(k => k.Name.ToUpper(), v => v);
            Dictionary<string, CheckBox> c3 = tabPage1.Controls.OfType<CheckBox>().ToDictionary(k => k.Name.ToUpper(), v => v);
            Dictionary<string, CheckBox> c4 = tabPage2.Controls.OfType<CheckBox>().ToDictionary(k => k.Name.ToUpper(), v => v);
            Dictionary<string, CheckBox> c5 = tabPage3.Controls.OfType<CheckBox>().ToDictionary(k => k.Name.ToUpper(), v => v);
            c1 = c1.Concat(c2).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            c1 = c1.Concat(c3).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            c1 = c1.Concat(c4).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            c1 = c1.Concat(c5).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            return c1;
        }

        private bool CurrentlyPopulating_DontUpdate;
        public void LoadFromStore()
        {
            CurrentlyPopulating_DontUpdate = true;
            StoreClass SV = Controller.Store; string key;
            Dictionary<string, TextBox> TxBxs = GetRelTextBoxes();
            Dictionary<string, CheckBox> ChkBxs = GetRelCheckBoxes();
            foreach (PropertyInfo prop in SV.GetType().GetProperties())
            {
                key = prop.Name.ToUpper();
                if (TxBxs.ContainsKey(key)) TxBxs[key].Text = prop.GetValue(SV).ToString();
                if (ChkBxs.ContainsKey(key)) ChkBxs[key].Checked = (bool)prop.GetValue(SV);
            }

            //Manual
            Settings_Form.Allow_Sound.Checked = SV.AllowSound;
            CurrentlyPopulating_DontUpdate = false;
        }

        public void UpdateStore()
        {
            //Reverse of above, but need to do a little parsing
            StoreClass SV = Controller.Store; string key;
            Dictionary<string, TextBox> TxBxs = GetRelTextBoxes();
            Dictionary<string, CheckBox> ChkBxs = GetRelCheckBoxes();
            foreach (PropertyInfo prop in SV.GetType().GetProperties())
            {
                key = prop.Name.ToUpper();
                if (TxBxs.ContainsKey(key))
                {
                    switch (prop.PropertyType.Name)
                    {
                        case string a when a.Contains("Int16"):
                            if (TxBxs[key].Text == "") TxBxs[key].Text = "0";
                            prop.SetValue(SV, (Int16)int.Parse(TxBxs[key].Text));
                            break;
                        case string a when a.Contains("Int"):
                            if (TxBxs[key].Text == "") TxBxs[key].Text = "0";
                            prop.SetValue(SV, int.Parse(TxBxs[key].Text));
                            break;
                        case string a when a.Contains("Single"):
                            if (TxBxs[key].Text == "") TxBxs[key].Text = "0";
                            prop.SetValue(SV, float.Parse(TxBxs[key].Text));
                            break;
                        default:
                            prop.SetValue(SV, TxBxs[key].Text);
                            break;
                    }
                }
                if (ChkBxs.ContainsKey(key)) prop.SetValue(SV, ChkBxs[key].Checked);
            }
            //Manual Entries
            SV.SplitterDistance = splitContainer1.SplitterDistance;
            SV.Form_Height = this.Height;
            SV.AllowSound = Settings_Form.Allow_Sound.Checked;

            //Getting the Com Port
            SV.COMPort = comboBox_Devices.SelectedItem == null ? "" : comboBox_Devices.SelectedItem.ToString();
            if (SV.COMPort.Contains("(")) SV.COMPort = SV.COMPort.Substring(SV.COMPort.IndexOf("(") + 1);
            SV.COMPort = SV.COMPort.Replace(")", "");

            SV.InitializeDependents();
        }

        #endregion

        #region Test Cycles ---------------------------------------------------------------------------------------------------------------------------------------------------------------
        private void DelayCycle(int MSECDelay)
        {
            for (int i = 0; i < MSECDelay / 5; i++)
            {
                Application.DoEvents();
                Thread.Sleep(5);
            }
        }

        public void btn_HeatTest_1_Click(object sender, EventArgs e)
        {
            //Set Curves = Temperature
            Controller.tempHeater.Reset();
            Curves.Channels = new List<PlottableChannel>(1) { Controller.tempHeater };
            Curves.CurvePens = CurveViewer.Curve_PensSetup.Channelx1_Temperature;
            timer_Update.Stop();

            //Allow the probe to cool
            Heater_Off();
            chkBox_Heater.Enabled = false;

            int time_msec = 5000;
            int delay_msec = 250;
            for (int i = 0; i < time_msec / delay_msec; i++)
            {
                if ((i * delay_msec) % 1000 == 0)
                    txBx_Update.Text = "Cooling . . \r\n" + ((time_msec - i * delay_msec) / 1000).ToString();
                timer_Update_Tick();
                DelayCycle(delay_msec);

            }
            double temp_Start = Controller.tempHeater.Temperature_Current; //Save the starting temperature

            //Turn on the heat for 30 seconds
            Heater_ON();
            time_msec = 30000;
            for (int i = 0; i < time_msec / delay_msec; i++)
            {
                if ((i * delay_msec) % 1000 == 0)
                    txBx_Update.Text = "Heating . . \r\n" + ((time_msec - i * delay_msec) / 1000).ToString();
                timer_Update_Tick();
                DelayCycle(delay_msec);
            }
            txBx_Update.Text = "Finished Heating Test.";
            double temp_End = Controller.tempHeater.Temperature_Current; //Save the ending temperature
            txBx_Update.Text = "Heat Rise = " + (temp_End - temp_Start).ToString("0.00") + "C in " + (time_msec / 1000).ToString("0") + "s.";

            Heater_Off(); chkBox_Heater.Enabled = true;

            Controller.Device.Reset(); //Channels
            Curves.Channels = Controller.GetChannels();
            Curves.CurvePens = CurveViewer.Curve_PensSetup.Channelx3_Default;
            timer_Update.Start();
        }

        List<float[]> FluorT1, FluorT3;

        public void btn_FluorTest_T1Blank_Click(object sender, EventArgs e)
        {
            FluorT1 = SaveChannelData(10000);
        }

        private List<float[]> SaveChannelData(int milliseconds)
        {
            //We have just inserted the tubes, so get a steady-state measure of both
            //Clear the channels, then measure for a set time
            Controller.ChannelsReset();
            txBx_Update.Text = "Recording for 10 seconds . . ";
            for (int i = 0; i < milliseconds / 100; i++)
            {
                Application.DoEvents(); Thread.Sleep(100);
            }
            List<float[]> Saved = Controller.ChannelsRecentData(0);
            txBx_Update.Text = "Done";
            return Saved;
        }

        public void btn_FluorTest_T3Blank_Click(object sender, EventArgs e)
        {
            FluorT3 = SaveChannelData(10000);
            StringBuilder sb = new StringBuilder();
            char d = '\t';
            sb.Append("Tube" + d + "Blank" + d + "Signal" + "\r\n");
            for (int i = 0; i < FluorT1[1].Length; i++) sb.Append("T1" + d + FluorT1[1][i] + d + FluorT3[1][i] + "\r\n");
            for (int i = 0; i < FluorT1[1].Length; i++) sb.Append("T3" + d + FluorT3[3][i] + d + FluorT1[3][i] + "\r\n");
            System.IO.File.WriteAllText(@"c:\temp\fluor_test.txt", sb.ToString());
        }

        #endregion

        #region Diagnostics Button ----------------------------------------------------------------------------------------------------------------------------------------

        private int Diag_Replicates;
        private int Diag_Cycle;
        private List<float[]> Diag_Sensor_Dark;
        private List<float[]> Diag_Sensor_Lite;
        private double Diag_Temp_Start;
        private double Diag_Temp_End;

        private void btn_Diagnostics_Click(object sender, EventArgs e)
        {
            Diag_Cycle = 0;
            Diag_Replicates = 16;

            Diagnostics_Cycle();
        }

        internal void Diagnostics_Cycle()
        {
            switch (Diag_Cycle)
            {
                case 0:
                    //This is the very first call (first cycle), no timer yet
                    if (Controller.Store.Temperature_Ideal < 50)
                    {
                        Temperature_Ideal.Text = "58";
                        Temperature_Tolerance.Text = "0.5";
                        Application.DoEvents();
                    }
                    Heater_Off();
                    txBx_Update.Text = "Diagnostics . . Dark data . . ";

                    //Setup the timer for the first time
                    if (Timer_Tests != null) Timer_Tests.Stop();
                    Timer_Tests = new System.Windows.Forms.Timer();
                    Timer_Tests.Tick += Diagnostic_Tick;

                    //Turn the lights off and setup the
                    Timing_Defaults_Diagnostic(false);
                    Controller.ChannelsReset();
                    QuickSpin();

                    Timer_Tests.Interval = Controller.Store.Timing_Cycle * Diag_Replicates;

                    Timer_Tests.Start();
                    break;
                case 1:
                    //This is the Dark Data
                    Diag_Sensor_Dark = Controller.ChannelsRecentData(0);
                    txBx_Update.Text = "Now with Light . . ";

                    //Turn on the lights and continue
                    Timing_Defaults_Diagnostic(true);
                    Controller.ChannelsReset();
                    QuickSpin();

                    Timer_Tests.Start();
                    break;
                case 2:
                    //This is the Light Data
                    Diag_Sensor_Lite = Controller.ChannelsRecentData(0);

                    //Now go ahead with the heater test
                    txBx_Update.Text = "Heater testing . . ";
                    Diag_Temp_Start = Controller.tempHeater.Temperature_Current;

                    //Now get the heater working
                    Timing_Defaults_Diagnostic(false); QuickSpin();
                    Heater_ON(); QuickSpin();

                    Timer_Tests.Interval = Diag_Replicates * 1000; //7 seconds
                    Timer_Tests.Start();
                    break;
                case 3:
                    //Finished, make the report
                    Diag_Temp_End = Controller.tempHeater.Temperature_Current;
                    Heater_Off();

                    txBx_Update.Text = Controller.Device.deviceID + " Report: " + DateTime.Now.ToString("M/d H:mm") + "\r\n" +
                    Diag_Report(1) + "\r\n" +
                    Diag_Report(3) + "\r\n" +
                    "Heat Rise " + (Diag_Temp_End - Diag_Temp_Start).ToString("0.0") + " C" + " /" + Diag_Replicates;
                    break;
                default:
                    break;
            }
        }

        private string Diag_Report(int TubeNum)
        {
            int skip = 3;
            //Need to throw away the first data point
            double AvgDark = Diag_Sensor_Dark[TubeNum].Skip(skip).Sum() / (Diag_Sensor_Dark[TubeNum].Length - skip);
            double AvgLite = Diag_Sensor_Lite[TubeNum].Skip(skip).Sum() / (Diag_Sensor_Lite[TubeNum].Length - skip);
            double baseline = 1;
            double PCNT_Increase = ((AvgLite + baseline) / (AvgDark + baseline)) - 1;
            return "T" + TubeNum + " LightLvl=" + (AvgLite / 10.24).ToString("0.00") + " Rise=" + PCNT_Increase.ToString("0.0%");
        }

        /// <summary>
        /// Lets the application process events for a short period
        /// </summary>
        /// <param name="FractionOfSend">How long to spin, a fraction of the total send time. Make it one if you want to go through a whole send cycle, -1 will set to the default</param>
        private void QuickSpin(double FractionOfSend = -1)
        {
            if (FractionOfSend <= 0) FractionOfSend = QuickSpin_Fract_Default;
            DateTime StopTime = DateTime.Now.AddMilliseconds(Controller.Store.Timing_Send * FractionOfSend);
            while (DateTime.Now < StopTime)
            {
                Application.DoEvents();
                Thread.Sleep(2);
            }
        }

        private void Diagnostic_Tick(object sender, EventArgs e)
        {
            Timer_Tests.Stop();
            Diag_Cycle++;
            Diagnostics_Cycle();
        }

        #endregion

        #region Misc ---------------------------------------------------------------------------------------------------------------------------------------------------------------

        // If the form is resized, redraw the curves
        // If the timer's going, the curves are redrawn fairly frequently, so don't do it during then
        private void RGD_FormMain_ResizeEnd(object sender, EventArgs e)
        {
            if (!timer_Update.Enabled)
            {
                Curves.Invalidate();
            }
        }
        internal void Enable_Tube2()
        {
            Controller.Tubes.Tubes[2].Active = true;
            Curves.Channels = Controller.GetChannels();
        }

        private void btn_SendEEPROM_Click(object sender, EventArgs e)
        {
            SaveSettings();
            DialogResult Res = MessageBox.Show("You are about to push data to the EEPROM (10K writes total), so make sure you are really ready.", "Update Device?", MessageBoxButtons.OKCancel);
            if (Res == DialogResult.Cancel) return;
            try
            {
                Int32 deviceID = int.Parse(txBx_DeviceID.Text);
                float calSlope = float.Parse(txBx_calSlope.Text);
                float calInter = float.Parse(txBx_CalIntercept.Text);
                SendInstructions(Packet_Instruct.SendEEPROM(deviceID, calSlope, calInter));
                txBx_calSlope.ForeColor = txBx_CalIntercept.ForeColor = Color.Black;
            }
            catch
            {
                txBx_Update.Text = "Check your numbers, one of them may not be a number. (parsing error)";
            }
        }

        public void SendInstructions(Packet_Instruct Instruct)
        {
            Controller.Device.Instruct(Instruct);
        }

        private bool Confirm_Stop()
        {
            if (CurrentState == RGDControlState.RunLong)
            {
                DialogResult DR = MessageBox.Show("You are in the midst of a long detection run, aborting could negate any results. \r\nPress Yes to Abort and Close, \r\nPress No to Continue the run.", "", MessageBoxButtons.YesNo);
                if (DR == DialogResult.No)
                {
                    return false; //Don't stop
                }
            }
            return true; //Stop
        }

        private void RGD_FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Confirm_Stop())
            {
                e.Cancel = true; return;
            }
            if (GoToEndOfDayRuns())
            {
                BackToStandby(true);
                txBx_Update.Text = "Please put in the end-of-day controls as shown and hit \"Run\" to finish the day";
                panel_RunSchema.SetSchemaState(RunSchemaState.ShutDown);
                e.Cancel = true;
                return;
            }
            Heater_Off();
            SaveSettings();
            for (int i = 0; i < 100; i++) { Thread.Sleep(5); Application.DoEvents(); } //Give it time to actually shut down, etc
        }

        private bool GoToEndOfDayRuns()
        {
            if (!panel_RunSchema.IsSchemaStatePresent(RunSchemaState.ShutDown)) return false;
            if (SchemaState == RunSchemaState.ShutDown) return false;
            DialogResult result = MessageBox.Show("It is recommended to complete an end-of-day quality control run\r\n" +
                    "Would you like to complete this check?\r\n" +
                    "(Press YES to perform this check; Press NO to quit", "Please complete final QC", MessageBoxButtons.YesNo);
            if (result != DialogResult.Yes)
            {
                return false;
            }
            return true;
        }
        private void DisplayLogarithmic_CheckedChanged(object sender, EventArgs e)
        {
            if (Curves != null)
                Curves.UseLogarithmicY = DisplayLogarithmic.Checked;
        }

        private void Save_Form_Image_Click(object sender, EventArgs e)
        {
            Save_Form_Image("Q");
        }

        public void Save_Form_Image(string AppendName = "")
        {
            string SavePath = Path.Combine(Controller.Store.BaseFolder, Controller.Device.SubFolder, Controller.Device.FileHeader + AppendName + ".png");
            Bitmap bmpScreenshot = new Bitmap(this.Width, this.Height, PixelFormat.Format32bppArgb); // Set the bitmap object to the size of the screen
            Graphics gfxScreenshot = Graphics.FromImage(bmpScreenshot); // Create a graphics object from the bitmap
            gfxScreenshot.CopyFromScreen(this.Left, this.Top, 0, 0, this.Size, CopyPixelOperation.SourceCopy); // Take the screenshot from the upper left corner to the right bottom corner
            bmpScreenshot.Save(SavePath, ImageFormat.Png);
        }

        #endregion

        #region Status Lights and Sound - -------------------------------------------------------------------------------------------------------------------------------------

        public void Status_Lights(RGDLightStates LightState, bool QuickSpin_After = true)
        {
            switch (LightState)
            {
                case RGDLightStates.Default:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(false));
                    break;
                case RGDLightStates.Connected:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(1, 1, 1, 500, 0.3, 2));
                    break;
                case RGDLightStates.RunLong_Start:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(0, 0, 1, 1250, 0.4, 0));
                    break;
                case RGDLightStates.RunLong_Halfway:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(0, 0, 1, 1250, 0.4, 0));
                    break;
                case RGDLightStates.RunLong_Finished:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(0, 1, 0, 250, 0.4, 100));
                    break;
                default:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(false));
                    break;
            }
            if (QuickSpin_After) QuickSpin();
        }

        public void Status_Buzzer(RGDBuzzerSounds Sound, bool QuickSpin_After = true)
        {
            if (Controller.Store.AllowSound)
            {
                switch (Sound)
                {
                    case RGDBuzzerSounds.Connected:
                        Controller.Device.Instruct(Packet_Instruct.SendStatusBuzzer_Connected());
                        break;
                    case RGDBuzzerSounds.RunLong_Start:
                        Controller.Device.Instruct(Packet_Instruct.SendStatusBuzzer_StartingRun());
                        break;
                    case RGDBuzzerSounds.RunLong_Halfway:
                        Controller.Device.Instruct(Packet_Instruct.SendStatusBuzzer_Halfway());
                        break;
                    case RGDBuzzerSounds.RunLong_Finished:
                        Controller.Device.Instruct(Packet_Instruct.SendStatusBuzzer_RunEnd());
                        break;
                    default:
                        break;
                }
                if (QuickSpin_After) QuickSpin();
            }
        }

        #endregion

        #region Timing Explanation Popup ----------------------------------------------------------------------------------------------------------------------------------------

        private void Show_TimingExplain(object sender = null, EventArgs e = null)
        {
            if (Timing_Explain_Form == null) Timing_Explain_Init();
            Timing_Explain_Form.Show();
        }

        private void Timing_Explain_Init()
        {
            Timing_Explain_Form = new Form();
            Form T = Timing_Explain_Form;
            PictureBox PB = new PictureBox();
            T.Controls.Add(PB);
            PB.Dock = DockStyle.Fill;
            PB.Image = Resources.Timing_Explain;
            T.Size = new System.Drawing.Size(PB.Image.Width + 10, PB.Image.Height + 40);
            T.FormClosing += T_FormClosing;
            PB.Click += TEF_PB_Click;
        }

        private void TEF_PB_Click(object sender, EventArgs e)
        {
            Timing_Explain_Form.Hide();
        }

        private void T_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Timing_Explain_Form.Hide();
        }

        #endregion

        #region On Screen Light Test Popup ----------------------------------------------------------------------------------------------------------------------------------------

        private void Show_OnScreen_LightTest(object sender = null, EventArgs e = null)
        {
            if (OnScreen_Light_Test == null) OnScreen_LightTest_Init();
            OnScreen_Light_Test.Show();
        }

        private void OnScreen_LightTest_Init()
        {
            OnScreen_Light_Test = new Form();
            Form T = OnScreen_Light_Test;
            PictureBox PB = new PictureBox();
            T.Controls.Add(PB);
            PB.Dock = DockStyle.Fill;

            T.Size = new System.Drawing.Size(400, 300);
            T.FormClosing += OSLT_FormClosing;
        }

        private void OSLT_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            OnScreen_Light_Test.Hide();
        }

        #endregion

        #region More Settings Window --------------------------------------------------------------------------------------------------------------------------------------

        private void label_More_EEPROM_Click(object sender, EventArgs e)
        {
            Settings_Form.Show();
        }

        internal void Send_EEPROM_Ext1(byte T1, byte T2, byte T3, byte LED1, byte LED3, byte Therm, byte Therm2, byte Heater)
        {
            Controller.Device.Instruct(Packet_Instruct.SendEEPROM_Ext1(T1, T2, T3, LED1, LED3, Therm, Therm2, Heater));
        }

        internal void Send_EEPROM_Ext2(byte Buzzer, byte RGB_R, byte RGB_G, byte RGB_B, float Temp2Optimal, float Temp2Range)
        {
            Controller.Device.Instruct(Packet_Instruct.SendEEPROM_Ext2(Buzzer, RGB_R, RGB_G, RGB_B, Temp2Optimal, Temp2Range));
        }

        private int send_test_set = 0;
        internal void Send_Status_Buzzer_Test()
        {
            switch (send_test_set)
            {
                case 0: Controller.Device.Instruct(Packet_Instruct.SendStatusBuzzer_Connected()); break;
                case 1: Controller.Device.Instruct(Packet_Instruct.SendStatusBuzzer_StartingRun()); break;
                case 2: Controller.Device.Instruct(Packet_Instruct.SendStatusBuzzer_Halfway()); break;
                case 3: Controller.Device.Instruct(Packet_Instruct.SendStatusBuzzer_RunEnd()); break;
                case 4: Controller.Device.Instruct(Packet_Instruct.SendStatusBuzzer_Simple1()); break;
                case 5: Controller.Device.Instruct(Packet_Instruct.SendStatusBuzzer_Simple3()); break;
                default: Controller.Device.Instruct(Packet_Instruct.SendStatusBuzzer_StartingRun()); break;
            }
            send_test_set++; if (send_test_set > 5) send_test_set = 0;
        }

        internal void Send_Status_Lights_Toggle()
        {
            const int MSEC_Mod_Cycle = 1000;
            const double FractionOn = 0.4;
            switch (send_test_set)
            {
                case 1:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(1, 1, 1, MSEC_Mod_Cycle, FractionOn, 0)); //White
                    break;
                case 2:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(1, 0, 0, MSEC_Mod_Cycle, FractionOn, 0)); //Red
                    break;
                case 3:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(0, 1, 0, MSEC_Mod_Cycle, FractionOn, 0)); //Green
                    break;
                case 4:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(0, 0, 1, MSEC_Mod_Cycle, FractionOn, 0)); //Blue
                    break;
                case 5:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(1, 1, 0, MSEC_Mod_Cycle, FractionOn, 0)); //Yellow
                    break;
                case 6:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(0, 1, 1, MSEC_Mod_Cycle, FractionOn, 0)); //Cyan
                    break;
                case 7:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(1, 0, 1, MSEC_Mod_Cycle, FractionOn, 0)); //Magenta
                    break;
                default:
                    Controller.Device.Instruct(Packet_Instruct.SendStatusLights(false)); //Off
                    break;
            }
            send_test_set++; if (send_test_set > 7) send_test_set = 0;
        }

        #endregion

        #region Timer Update Tick -----------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Kind of a Key command, this is triggered by a timer and causes various updates to occur
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Update_Tick(object sender = null, EventArgs e = null)
        {
            //Draw the optical stuff on Panel1
            Curves.Invalidate();

            //Heater stuff
            UpdateTemperature();

            //If we are in the Midst of a run, check where we are in the run
            if (CurrentState == RGDControlState.RunLong)
                LongRun_Checks();

            //Check UnPlugged
        }

        private void CheckUnplugged()
        {
            //Check for unplugged device
            double milliSecondsLag = Math.Max(Controller.Store.Timing_Send * 20, 30000);
            if (Controller.Device.LastReceived_SP.AddMilliseconds(milliSecondsLag) < DateTime.Now)
            {
                txBx_Update.Text = "Device unplugged?";

                Stop();
                Thread.Sleep(100);
                Clear();
                Thread.Sleep(100);
                Scan_Devices();
            }
        }

        private void UpdateTemperature()
        {
            txBx_TemperatureCurrent.Text = Controller.tempHeater.Temperature_Current.ToString("0.0");
            switch (Controller.tempHeater.Status)
            {
                case -1:
                    txBx_TemperatureCurrent.ForeColor = Color.Blue;
                    break;
                case 1:
                    txBx_TemperatureCurrent.ForeColor = Color.Red;
                    break;
                default:
                    txBx_TemperatureCurrent.ForeColor = Color.Black;
                    break;
            }
            if (CurrentState == RGDControlState.Standby)
            {
                btn_Run_30min.Enabled = (Controller.tempHeater.Temperature_Current > Controller.Store.Temperature_Ideal * 0.93) && MetadataFilled;
                btn_PreHeat.Enabled = !chkBox_Heater.Checked;
            }
        }

        #endregion

        #region Long 30 minute run ------------------------------------------------------------------------------------------------------------------------------------

        private DateTime Long_Run_Start;
        bool Played_Halfway_Sound; bool InitialQC;
        //These next two could get moved in the the Device.SV
        public double Long_Run_FractionToStartNormalization = 0.585;
        public double Long_Run_FractionToBeep = 0.585;


        public float RunTime_Minutes_Value => panel_RunSchema.CurrentSchema.BothControls ? Controller.Store.RunTime_Minutes_Shortened : Controller.Store.RunTime_Minutes;
        private DateTime Long_Run_End => Long_Run_Start.AddMinutes(RunTime_Minutes_Value);

        public void Run_Long_Start(object sender = null, EventArgs e = null)
        {
            //Pause other threads messing with things
            timer_Update.Stop();
            timer_Update.Tick -= timer_Update_Tick;

            Curves.Clear();

            //Fire up the heater if it is not already on
            Heater_ON();
            QuickSpin();

            //Make sure heat is within 30% of the ideal
            if (Controller.IsHotEnough())
            {
                txBx_Update.Text = "Wait for Temperature to get closer to the set point (or adjust)...";
                timer_Update.Start();
                return;
            }

            //Ensure the lighting is correct
            Timing_Defaults_LongRun_DontPush();
            SaveSettings();

            //Change the lighting on the device
            QuickSpin();
            Status_Buzzer(RGDBuzzerSounds.RunLong_Start);
            QuickSpin();
            Status_Lights(RGDLightStates.RunLong_Start);
            QuickSpin();

            //Reset the channels, and adjust the buffer size
            UpdateLightTiming_Click();
            timer_Update.Stop(); //Pause other threads messing with things - duplicated since the above command restarts the timer
            //QuickSpin();

            //Change the channel settings for this long run, including starting a new save file
            Controller.Channels_SetupForLongRun(panel_RunSchema.CurrentSchema.BothControls);
            Controller.TempReset();

            //Start recording
            Enable_Controls(RGDControlState.RunLong);
            Long_Run_Start = DateTime.Now; Played_Halfway_Sound = false; InitialQC = false;
            txBx_Update.Text = "Start of " + RunTime_Minutes_Value.ToString("0") + " minute run . . ";

            timer_Update.Tick += timer_Update_Tick;
            timer_Update.Start(); //Also restart the main update timer
        }

        private void LongRun_Checks()
        {
            //Get the updated timing information
            TimeSpan TE = DateTime.Now - Long_Run_Start;
            TimeSpan TR = Long_Run_End - DateTime.Now;

            //Show the running label (blinks on and off)
            if (label_Running.Visible)
                label_Running.Visible = false;
            else
                label_Running.Visible = true;

            //Do the intial Quality check of this run
            if ((TE.TotalMinutes > 0.7) && !InitialQC)
            {
                string Status = "";
                Status += RGD_Library.Quality.RuntimeCheck.InitialCheck(Controller);
                Status += RGD_Library.Quality.RuntimeCheck.FrequentCheck(Controller);
                File.WriteAllText(Controller.FullFileName + "_st1.txt", Status);
                InitialQC = true;
            }

            //Halfway Lights
            if (TE.TotalMinutes > (RunTime_Minutes_Value * Long_Run_FractionToBeep))
            {
                if (!Played_Halfway_Sound)
                {
                    Played_Halfway_Sound = true; //If you don't do this first, it will try it again
                    Status_Buzzer(RGDBuzzerSounds.RunLong_Halfway);
                    Status_Lights(RGDLightStates.RunLong_Halfway);
                }
            }
            if (TE.TotalMinutes > (RunTime_Minutes_Value * Long_Run_FractionToStartNormalization)) //Auto Scale
            {
                Controller.Channels_AdjustScaling(false);
                txBx_Update.Text = "Display=NORMALIZED\r\n";
            }
            else
            {
                txBx_Update.Text = "Display=RAW\r\n";
            }

            //End point of the run
            if (TE.TotalMinutes > RunTime_Minutes_Value)
            {
                Run_Long_EndRun();
            }
            else
            {
                //Countdown display
                txBx_Update.Text += TE.ToString(@"mm\:ss") + " Elapsed\r\n" + TR.ToString(@"mm\:ss") + " Remaining";
            }
        }

        internal void Run_Long_EndRun()
        {
            timer_Update.Tick -= timer_Update_Tick;
            timer_Update.Stop();

            //End of Run!
            Status_Buzzer(RGDBuzzerSounds.RunLong_Finished, false);
            QuickSpin();
            Status_Lights(RGDLightStates.RunLong_Finished, false);
            txBx_Update.Text = "Finished! Saving image and results..";

            this.Focus(); this.BringToFront();
            Controller.ChannelsFlushBuffer();
            Save_Form_Image();
            var Res = Calculate_Results_Save();
            (bool isError, string message) = CheckForErrors();
            if (isError && SendNotification.Checked) SendErrorReport(message);
            Enable_Controls(RGDControlState.Standby); //Otherwise Stop will get unhappy
            if (SchemaState == RunSchemaState.ShutDown)
            {
                QuickSpin();
                Heater_Off();
                QuickSpin();
                Close();
            }
            if (isError) MessageBox.Show(message + "\r\nPlease service this device");
            panel_RunSchema.IncrementSchema();
            
            FinishedRun_Form = new Form_Finished_Run(this);
            DialogResult DR = FinishedRun_Form.ShowDialog();
            Save_FinalNotes(FinishedRun_Form.FinalNotes, Res); //Save any final notes that the users gave
            Controller.Channels_AdjustScaling(true); //Reset the scaling
            switch (DR)
            {
                case DialogResult.Yes: //User can get ready for another run
                case DialogResult.Cancel: 
                    BackToStandby(true);
                    return;
                case DialogResult.No: //User wants to go back into Standby mode with heater off
                    BackToStandby(false);
                    return;
                case DialogResult.Abort: //User wants to quit
                    try
                    {
                        Close();
                    }
                    catch
                    {
                        Environment.Exit(0);
                    }
                    break;
                default:
                    break;
            }
        }
        void BackToStandby(bool HeaterOn)
        {
            Connect();
            QuickSpin();
            if (HeaterOn) Heater_ON(); else Heater_Off();
            timer_Update.Tick += timer_Update_Tick;
            timer_Update.Start();
            txBx_Update.Text = "Standby";

            //Clear Note_2 with the subject ID
            Subject.PlaceholderText = Subject.Text;
            Subject.Text = "";
        }

        #endregion

        #region Results ------------------------------------------------------------------------------------------------------------------------------------------------

        private List<ResultsClass> Calculate_Results_Save()
        {
            //First calculate the results of this run
            List<ResultsClass> Results = Controller.GetResultsFromDevice();
            foreach (var result in Results)
            {
                switch (result.TubeID)
                {
                    case 1:
                        result.AdditionalInfo = txt_AddInfoTube1.Text;
                        break;
                    case 3:
                        result.AdditionalInfo = txt_AddInfoTube3.Text;
                        break;
                }

            }
            ResultsSet.AddRange(Results);
            foreach (ResultsClass result in Results) MetadataTable.Add(result);
            //Go through and delete that "data" part of the previous results so it doesn't occupy too much memory
            return Results;
        }

        private void Save_FinalNotes(string finalNotes, List<ResultsClass> Res)
        {
            foreach (ResultsClass result in Res)
            {
                result.FinalNote = finalNotes;
                MetadataTable.Add(result);
            }
        }

        private (bool iserror, string message) CheckForErrors()
        {
            if (ResultsSet == null) return (false, "");
            ResultsErrors errors = ResultsSet.LatestResultsErrors();
            if (errors.HasFlag(ResultsErrors.NegControlPositive) && errors.HasFlag(ResultsErrors.PosControlNegative))
            {
                return (true, "The positive control did not amplify and the negative control showed amplification");
            }
            else if (errors.HasFlag(ResultsErrors.PosControlNegative))
            {
                return (true, "The positive control did not amplify");
            }
            else if (errors.HasFlag(ResultsErrors.NegControlPositive))
            {
                return (true, "The negative control showed amplification");
            }
            return (false, "");
        }

        private void btn_OpenAppData_Click(object sender, EventArgs e)
        {
            string d = Path.Join(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "RGD");
            Directory.CreateDirectory(d);
            Process.Start("explorer.exe", d);
        }

        private void btn_ReloadSchema_Click(object sender, EventArgs e)
        {
            Controller.LoadSchema();

            panel_RunSchema.LoadRawSchema(Controller.Schema);
            bool doStartRuns = panel_RunSchema.SetSchemaState(RunSchemaState.StartUp);
            if (!doStartRuns) panel_RunSchema.SetSchemaState(RunSchemaState.Regular);
        }

        private void SendErrorReport(string message)
        {
            string time = DateTime.Now.ToString("HH:mm MM/dd/yyyy");
            ErrorReport.SendErrorEmail(
                $"Device {Controller.Device.deviceID} error",
                $"Device {Controller.Device.deviceID} has had the following error at {time}: {message}"
            );
            return;
        }
        #endregion
    }

    public class Form_Finished_Run : Form
    {
        public string FinalNotes => FinalNotesT.Text;

        public RGD_FormMain ParentRGD;
        public TextBox UpdateT;
        public TextBox FinalNotesT;
        public ResultsSet_Class ResultsSet => ParentRGD.ResultsSet;
        public static string MainUpdateTxt = "Your run has now finished.\r\nRefer to the graph above to determine a non-clinical result for presence of LAMP products.\r\nChoose and option below to continue.";
        public Form_Finished_Run(RGD_FormMain RGDParent)
        {
            ParentRGD = RGDParent;
            Form_Finished_Run_Shown(null, null);
            MakeButtons();
            Shown += Form_Finished_Run_Shown;
        }

        private void Form_Finished_Run_Shown(object sender, EventArgs e)
        {
            this.Top = ParentRGD.Top + ParentRGD.Controller.Store.SplitterDistance + 40;
            this.Height = ParentRGD.Bottom - this.Top - (40 * 2);
            this.Left = ParentRGD.Left + 40;
            this.Width = ParentRGD.Width - (40 * 2);
            if (UpdateT != null)
                UpdateT.Text = (ResultsSet == null ? "" : (ResultsSet.ReportLatest() + "\r\n")) + MainUpdateTxt;

        }

        private void MakeButtons()
        {
            int ButtonWidth = (this.Width - 10) / 3;

            UpdateT = new TextBox();
            UpdateT.Multiline = true;
            UpdateT.Top = 0; UpdateT.Left = 0; UpdateT.Width = 3*this.Width / 5;
            UpdateT.Height = 60;
            UpdateT.Text = MainUpdateTxt;
            UpdateT.TabStop = false;
            Controls.Add(UpdateT);

            FinalNotesT = new TextBox();
            FinalNotesT.Multiline = true;
            FinalNotesT.Top = 0; FinalNotesT.Left = UpdateT.Right + 1; FinalNotesT.Width = this.Width - FinalNotesT.Left;
            FinalNotesT.Height = 60;
            FinalNotesT.PlaceholderText = "T1=Clear, T3=Turbid, comments?";
            FinalNotesT.TabStop = false;
            Controls.Add(FinalNotesT);

            //> Swap out tubes, then start another Run
            //> Turn off heater, go back to pre - Run mode
            //> Exit program

            Button tB_Run = new Button();
            tB_Run.Text = "Keep Heater ON and go to Standby.";
            tB_Run.DialogResult = DialogResult.Yes;
            tB_Run.Location = new Point(2, UpdateT.Bottom + 3);
            tB_Run.Width = ButtonWidth - 2; tB_Run.Height = 80;
            Controls.Add(tB_Run);

            Button tB_Standby = new Button();
            tB_Standby.Text = "Turn off Heater and go to Standby.";
            tB_Standby.DialogResult = DialogResult.No;
            tB_Standby.Location = new Point(tB_Run.Left + ButtonWidth, tB_Run.Top);
            tB_Standby.Size = tB_Run.Size;
            Controls.Add(tB_Standby);

            Button tB_Exit = new Button();
            tB_Exit.Text = "Exit Program.";
            tB_Exit.DialogResult = DialogResult.Abort;
            tB_Exit.Location = new Point(tB_Standby.Left + ButtonWidth, tB_Run.Top);
            tB_Exit.Size = tB_Run.Size;
            Controls.Add(tB_Exit);

            this.AcceptButton = tB_Run;
            this.CancelButton = tB_Standby;

            tB_Run.Focus();
        }
    }
}
